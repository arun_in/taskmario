//
//  JSONParser.swift
//  SimpleJSONParse
//
//  Created by Appzoc on 22/01/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import Foundation
import Alamofire

class JSONParser{
    
    let delegate:JSONParserDelegate
    let url:String
    let httpMethod:HTTPMethod
    let parameters:[String:Any]
    let model:ModelType
    
    init(url:String,httpMethod:HTTPMethod,parameters:[String:Any]?,delegate:JSONParserDelegate, into model:ModelType) {
        self.url = url
        self.httpMethod = httpMethod
        self.parameters = parameters ?? [:]
        self.delegate = delegate
        self.model = model
    }
    
    func callWebService(){
        print("Webcall initiate")
        
        Alamofire.request(url, method: self.httpMethod, parameters: self.parameters, encoding: JSONEncoding.default , headers: nil).authenticate(user: "dev", password: "dev").responseJSON { (response) in
            //print(response.result.value)
            switch response.result{
            case .success(let validJSON):
                print("Call Success")
                print(validJSON)
                let tempJSON = validJSON as? [String:Any]
                print("TempJSON",tempJSON)
               // let jsonData = tempJSON
              //print(jsonData)
                let jsonDataFieldArray = tempJSON!["Data"] as? [[String:Any]]
                let jsonDataField = tempJSON!["Data"] as? [String:Any]
                do{
                    let jsonObject = try JSONSerialization.data(withJSONObject: jsonDataField ?? jsonDataFieldArray, options: .prettyPrinted)
                     self.extractModelFromData(modelData: jsonObject)
                }catch{
                    print(error)
                }
               
                //print(validJSON)
            case .failure(let failCause):
                print("Call Failure")
                self.delegate.didReturnWithError(error: failCause)
            }
        }
    }
    
    func extractModelFromData(modelData:Data){
        print("Extracting Model")
        let decoder = JSONDecoder()
        do {
            switch model {
            case .Questionaire:
                let populatedModel = try decoder.decode([QuestionsAPI].self, from: modelData)
                delegate.operateOnJSONResult(model: populatedModel)
            case .HomePage:
                let populatedModel = try decoder.decode(APIHomePage.self, from: modelData)
                delegate.operateOnJSONResult(model: populatedModel)
            case .MyProfile:
                let populatedModel = try decoder.decode(APIMyProfile.self, from: modelData)
                delegate.operateOnJSONResult(model: populatedModel)
            case .Login:
                let populatedModel = try decoder.decode(APILogin.self, from: modelData)
                delegate.operateOnJSONResult(model: populatedModel)
            case .PartnerProfile:
                let populatedModel = try decoder.decode(APIPartnerProfile.self, from: modelData)
                delegate.operateOnJSONResult(model: populatedModel)
            case .ClosedTasks:
                let populatedModel = try decoder.decode([APIClosedTasks].self, from: modelData)
                delegate.operateOnJSONResult(model: populatedModel)
            case .Notifications:
                let populatedModel = try decoder.decode([APINotifications].self, from: modelData)
                delegate.operateOnJSONResult(model: populatedModel)
            case .ChatList:
                let populatedModel = try decoder.decode([APIChatList].self, from: modelData)
                delegate.operateOnJSONResult(model: populatedModel)
            case .TasksList:
                let populatedModel = try decoder.decode([APITaskList].self, from: modelData)
                delegate.operateOnJSONResult(model: populatedModel)
            }
            
            
        } catch {
            print("error trying to convert data to JSON")
            delegate.didReturnWithError(error: error)
            
        }
    }
}

enum ModelType{
    case HomePage
    case Questionaire
    case MyProfile
    case Login
    case PartnerProfile
    case ClosedTasks
    case Notifications
    case ChatList
    case TasksList
}

protocol JSONParserDelegate {
    func operateOnJSONResult(model:Any)
    func didReturnWithError(error:Error)
}

