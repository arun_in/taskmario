//
//  Connectivity.swift
//  TaskMario
//
//  Created by Appzoc on 15/05/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import Foundation


class Connectivity{
    
    class func connectionAvailable() -> Bool{
        guard let status = Network.reachability?.status else { print("Guard Fail"); return false }
        switch status {
        case .unreachable:
        print("No Connection")
        //Banner.showBannerOn(VC: self, title: "Network Error", message: "Network Unreachable", style: .warning)
        //view.backgroundColor = .red
        return false
        default:
        print("Network Connected")
        return true
        //        case .wifi:
        //            Banner.showBannerOn(VC: self, title: "Wifi Connected", message: "", style: .success)
        //            //view.backgroundColor = .green
        //        case .wwan:
        //            Banner.showBannerOn(VC: self, title: "WAN Connected", message: "", style: .success)
        //            //view.backgroundColor = .yellow
        }
    }
}
