//
//  GlobalLoader.swift
//  TaskMario
//
//  Created by Appzoc on 26/02/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import Foundation
import UIKit

class LoadTabBar{
    static let shared = LoadTabBar()
    private init() {}
    
    func loadHomeTab(navigationController:UINavigationController){
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMHomeScreenController") as! TMHomeScreenController
        if (navigationController.topViewController?.isEqual(vc))!{
            print("Check Home on top")
        }else{
            navigationController.pushViewController(vc, animated: true)
        }
    }
    
    func loadNewTaskTab(navigationController:UINavigationController){
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMSearchController") as! TMSearchController
        navigationController.pushViewController(vc, animated: true)
    }
    
    
    func loadAllTasksTab(navigationController:UINavigationController){
        if AppState.isLoggedIn && AppState.userData?.isCompleted == 1 {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMAllTasksController") as! TMAllTasksController
        navigationController.pushViewController(vc, animated: true)
        }else{
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMLoginController") as! TMLoginController
            navigationController.pushViewController(vc, animated: true)
        }
    }
    
    func loadChatTab(navigationController:UINavigationController){
        if AppState.isLoggedIn && AppState.userData?.isCompleted == 1{
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMChatListController") as! TMChatListController
        navigationController.pushViewController(vc, animated: true)
        }else{
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMLoginController") as! TMLoginController
            navigationController.pushViewController(vc, animated: true)
        }
    }
    
    func loadProfileTab(navigationController:UINavigationController){
        //print(AppState.isLoggedIn,"appstatesss:",AppState.userData?.isCompleted)
        
        if AppState.isLoggedIn && AppState.userData?.isCompleted == 1{
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfileTabController") as! ProfileTabController
        navigationController.pushViewController(vc, animated: true)
        }else{
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMLoginController") as! TMLoginController
            navigationController.pushViewController(vc, animated: true)
        }
    }
}

