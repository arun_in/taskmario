//
//  BaseValidator.swift
//  Follow
//
//  Created by Appzoc on 10/01/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import Foundation
import UIKit

public class BaseValidator {
    
    fileprivate init(){}
    
//    class func unwrap<T>(element: T?) -> T {
//        guard let nonEmptyValue = element else {
//            print("TYpeee",element)
//            if element is String?{
//                return "" as! T
//            }else if element is Double?{
//                return 0.0 as! T
//            }else{
//                print("elsecase")
//                return 0.0 as! T
//            }
//
//        }
//
//        return nonEmptyValue
//    }
    
    class func unwrap(string: String?)-> String{
        guard let nonEmptyValue = string else {
            return ""
        }
        return nonEmptyValue
    }
    
    
    class func unwrap(integer: Int?)-> Int{
        guard let nonEmptyValue = integer else {
            return 0
        }
        return nonEmptyValue
    }
    
    
    class func unwrap(double: Double?)-> Double{
        guard let nonEmptyValue = double else {
            return 0.0
        }
        return nonEmptyValue
    }
    
    
    
    
    class func isValid(email: String?) -> Bool {
        guard let email = email, email != "" else { return false }
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTester = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTester.evaluate(with: email)
    }
    
    
    class func isValid(phone: String?)->Bool {
        guard let phone = phone, phone != "" else { return false }
        let charcterSet  = NSCharacterSet(charactersIn: "+0123456789").inverted
        let inputString = phone.components(separatedBy: charcterSet)
        let filtered = inputString.joined(separator: "")
        
        if phone == filtered {
            let phoneRegex = "[235689][0-9]{6}([0-9]{3})?"
            let phoneNumberTester = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
            return  phoneNumberTester.evaluate(with: phone)
        }else {
            return false
        }
    }
    
    class func isValid(digits: String)->Bool {
        let charcterSet  = NSCharacterSet(charactersIn: "+0123456789").inverted
        let inputString = digits.components(separatedBy: charcterSet)
        let filtered = inputString.joined(separator: "")
        return  digits == filtered
    }
    
    class func isValid(number: String) -> Bool {
        let characters = CharacterSet.decimalDigits.inverted
        return !number.isEmpty && number.rangeOfCharacter(from: characters) == nil
    }
    
    class func isNotEmpty(string: String?) -> Bool
    {
        guard !(string?.trimmingCharacters(in: .whitespaces).isEmpty)!, string != nil else {
            return false
        }
        return true
    }
    
    class func invalidStringCheck(withTextField: UITextField, assigningString: String){
        DispatchQueue.main.async {
            withTextField.textColor = UIColor.gray
            withTextField.text = nil
            if !assigningString.trimmingCharacters(in: .whitespaces).isEmpty{
                withTextField.text = assigningString.trimmingCharacters(in: .whitespaces)
            }
        }
        
    }
    
}

