//
//  ActivityIndicator.swift
//  AutoGradeVTS
//
//  Created by Appzoc on 03/02/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import Foundation
import UIKit
import NotificationBannerSwift

class ActivityIndicator
{
    
    static var activityView:UIView = UIView()
    static var activitySubview = UIView()
    
    
    static func setUpActivityIndicator(baseView:UIView){
        activityView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        activitySubview.frame = CGRect(x: (UIScreen.main.bounds.width/2 - 117), y: (UIScreen.main.bounds.height/2 - 43), width: 234, height: 86)
        activitySubview.backgroundColor = UIColor.white
        activitySubview.layer.cornerRadius = 20
        let indicator = UIActivityIndicatorView(frame: CGRect(x: 42, y: 33, width: 20, height: 20))
        indicator.startAnimating()
        indicator.color = UIColor.green
        let activityLabel = UILabel(frame: CGRect(x: 77, y: 33, width: 80, height: 21))
        activityLabel.text = "Loading..."
        activitySubview.addSubview(indicator)
        activitySubview.addSubview(activityLabel)
        activityView.addSubview(activitySubview)
        activityView.backgroundColor = UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 0.3)
        baseView.addSubview(activityView)
        
    }
    
    static func setUpActivityIndicatorWith(title:String, baseView:UIView){
        activityView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        activitySubview.frame = CGRect(x: (UIScreen.main.bounds.width/2 - 117), y: (UIScreen.main.bounds.height/2 - 43), width: 234, height: 86)
        activitySubview.backgroundColor = UIColor.white
        activitySubview.layer.cornerRadius = 20
        let indicator = UIActivityIndicatorView(frame: CGRect(x: 42, y: 33, width: 20, height: 20))
        indicator.startAnimating()
        indicator.color = UIColor.green
        let activityLabel = UILabel(frame: CGRect(x: 77, y: 33, width: 80, height: 21))
        activityLabel.text = "\(title)"
        activitySubview.addSubview(indicator)
        activitySubview.addSubview(activityLabel)
        activityView.addSubview(activitySubview)
        activityView.backgroundColor = UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 0.3)
        baseView.addSubview(activityView)
        
    }
    
    static func dismissActivityView(){
        DispatchQueue.main.async {
            //activityView.isHidden = true
           // activitySubview.removeFromSuperview()
            activityView.removeFromSuperview()
        }
        
    }
    
    
    
}

class Banner{
    static func showBanner(title: String = "", message: String, style: BannerStyle = .danger){

       // BaseThread.asyncMain {

            let banner = NotificationBanner(title: title, subtitle: message, style: style)

            banner.duration = 3

            banner.show(queuePosition: .front, bannerPosition: .bottom)

       // }

    }


    static func showBannerOn(VC: UIViewController, title: String = "", message: String, style: BannerStyle = .danger){

     //   BaseThread.asyncMain {

            let banner = NotificationBanner(title: title, subtitle: message, style: style)

            banner.duration = 3

            banner.show(bannerPosition: .top)

      //  }

    }
}

class ActivityIndicatorAlter
{
    var activityView:UIActivityIndicatorView!
    var view = UIView(frame: UIScreen.main.bounds)
    
    func start()
    {
        if activityView == nil
        { activityView = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
            activityView.frame = CGRect(x: (view.bounds.maxX/2)-20,
                                        y: (view.bounds.maxY/2)-20,
                                        width: 40, height: 40)
            self.view.addSubview(activityView)
            self.view.backgroundColor = UIColor(red:0.26, green:0.26, blue:0.26, alpha:0.8)
            UIApplication.shared.keyWindow?.addSubview(self.view)
            activityView.startAnimating()
        }
        else if activityView.isAnimating == true
        {
        }
        else
        { UIApplication.shared.keyWindow?.addSubview(self.view)
            activityView.startAnimating()
        }
    }
    
    func stop()
    {
        if activityView == nil || activityView.isAnimating == false
        {
        }
        else
        { activityView.stopAnimating()
            self.view.removeFromSuperview()
        }
    }
}

