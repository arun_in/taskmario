//
//  TMHomeScreenController.swift
//  TaskMario
//
//  Created by Appzoc on 06/02/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import Kingfisher
import Alamofire

class TMHomeScreenController: UIViewController, AccessNavigation {
    
    
    
    var displayData:APIHomePage = AppState.homePageData!
    var transferNameForCell2:[String] = []
    var transferImageForCell2:[String] = []
    var transferSubIDForCell2:[Int] = []
    
    @IBOutlet weak var locationLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Display Data",displayData)
        
        //testHeaderAuth()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        locationLabel.text = AppState.selectedLocation
    }
    
    override func didReceiveMemoryWarning() {
        print("Memory Issue")
    }
    
    func accessNavigation(viewCon: UIViewController) {
        self.navigationController?.pushViewController(viewCon, animated: true)
    }
    
    func presentThroughController(viewCon: UIViewController) {
        self.present(viewCon, animated: true, completion: nil)
    }
    
//    func testHeaderAuth(){
//        let username = "dev"
//        let password = "dev"
//
//        let credentialData = "\(username):\(password)".data(using: String.Encoding.utf8)!
//        let base64Credentials = credentialData.base64EncodedString()
//
//
//
//        Alamofire.request("http://taskmario.dev.webcastle.in/api/myProfile", method:.post, parameters: ["userid":92], encoding: JSONEncoding.default , headers: ["Authorization":"Basic \(base64Credentials)","Content-Type": "application/json"]).responseJSON {
//            (response) in
//            print("Error ",response.error)
//            print(response.result.value)
//            let jsonData = response.result.value as? [String:Any]
//            // print(jsonData)
//        }
//    }
    
    @IBAction func viewAllFromCategoryBTN(_ sender: UIButton) {
        if sender.tag == 0{
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMSearchController") as! TMSearchController
            vc.category = "Instant Services"
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMSearchController") as! TMSearchController
            vc.category = "Home & Repair"
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func searchServicesBTN(_ sender: UIButton) {
        //TMTabSearchController
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMSearchController") as! TMSearchController
        vc.category = "All"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
//    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMTabSearchController") as! TMTabSearchController
//    self.navigationController?.pushViewController(vc, animated: true)
    
    
    @IBAction func setLocationBTN(_ sender: UIButton) {
        //TMSetLocationController
       // ActivityIndicator.setUpActivityIndicator(baseView: self.view)
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMSetLocationController") as! TMSetLocationController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    //TabBarControls
    
    @IBAction func homeScreenBTN(_ sender: UIButton) {
       // LoadTabBar.shared.loadHomeTab(navigationController: self.navigationController!)
    }
    @IBAction func newTaskBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadNewTaskTab(navigationController: self.navigationController!)
    }
    @IBAction func tasksBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadAllTasksTab(navigationController: self.navigationController!)
    }
    
    @IBAction func chatBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadChatTab(navigationController: self.navigationController!)
    }
    
    @IBAction func profileScreenBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadProfileTab(navigationController: self.navigationController!)
    }
}

extension TMHomeScreenController: UITableViewDelegate, UITableViewDataSource{
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableView.allowsSelection = false
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell1 = tableView.dequeueReusableCell(withIdentifier: "TMHomeScreenCell1") as! TMHomeScreenCell1
        cell1.categoryData = displayData.Categorymain
        
        let cell2 = tableView.dequeueReusableCell(withIdentifier: "TMHomeScreenCell2") as! TMHomeScreenCell2
        cell2.navigationAccess = self
        
        let cell3 = tableView.dequeueReusableCell(withIdentifier: "TMHomeSceenCell3") as! TMHomeSceenCell3
        cell3.navigationControl = self
        
        cell1.navAccess = self
        switch indexPath.row {
        case 0:
            cell1.backgroundColor = UIColor.white
            cell1.separatorInset = UIEdgeInsetsMake(0, cell1.bounds.size.width, 0, 0);
            return cell1
        case 1:
            setUpTableCell2(key: "Instant Services")
            cell2.categoryName.text = "Instant Services"
            cell2.subcategoryData = self.transferNameForCell2
            cell2.isInstant = true
            cell2.subcategoryImage = self.transferImageForCell2
            cell2.subcategoryID = self.transferSubIDForCell2
            cell2.viewAllRef.tag = 0
            cell2.separatorInset = UIEdgeInsetsMake(0, 22, 1, 22);
            //cell2.separatorInset = UIEdgeInsetsMake(30, 2, cell2.bounds.size.width-60, 1);
           // cell2.backgroundColor = UIColor.green
            return cell2
        case 2:
            let key = displayData.CategoryList[8].catname
            setUpTableCell2(key: key)
            cell2.categoryName.text = key
            cell2.isInstant = false
            cell2.subcategoryData = self.transferNameForCell2
            cell2.subcategoryImage = self.transferImageForCell2
            cell2.subcategoryID = self.transferSubIDForCell2
            cell2.viewAllRef.tag = 1
            cell2.separatorInset = UIEdgeInsetsMake(0, cell2.bounds.size.width, 0, 0);
            return cell2
        case 3:
            cell3.servicesCountIndicator.text = "View all \(displayData.services_count) services"
            cell3.separatorInset = UIEdgeInsetsMake(0, cell3.bounds.size.width, 0, 0)
            return cell3
        default:
            //cell1.backgroundColor = UIColor.purple
            return cell1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        tableView.layer.shouldRasterize = true
        tableView.layer.rasterizationScale = UIScreen.main.scale
        switch indexPath.row {
        case 0:
            let numberOfItems: Double = 8
            var numberOfRows = Int(numberOfItems/4)
            let remainder = numberOfItems.truncatingRemainder(dividingBy: 4)
            print("Remainder",remainder)
            if remainder > 0{
                numberOfRows += 1
            }
            print("numberOFROws",numberOfRows)
            return CGFloat(numberOfRows * 102)
        case 1:
            return 210
        case 2:
            return 210
        case 3:
            return 90
        default:
            return 280
        }
    }
    
    func setUpTableCell2(key:String){
        transferImageForCell2.removeAll()
        transferNameForCell2.removeAll()
        transferSubIDForCell2.removeAll()
        var _:Subcategories? = nil
        for item in displayData.CategoryList{
            if item.catname == key{
                for subItem in item.Subcategories{
                    transferNameForCell2.append(subItem.subcat_name)
                    transferImageForCell2.append(subItem.thumb)
                    transferSubIDForCell2.append(subItem.subcatid)
                }
            }
        }
        
    }
    
}
