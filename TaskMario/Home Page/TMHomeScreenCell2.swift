//
//  TMHomeScreenCell2.swift
//  TaskMario
//
//  Created by Appzoc on 06/02/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import Kingfisher

class TMHomeScreenCell2: UITableViewCell {
    
    
    @IBOutlet weak var collectionRef: UICollectionView!
    
    @IBOutlet weak var categoryName: UILabel!
    
    @IBOutlet weak var viewAllRef: UIButton!
    
    var isInstant:Bool = false
    
    var navigationAccess:AccessNavigation!
    
    var subcategoryData:[String] = []
    var subcategoryImage:[String] = []
    var subcategoryID:[Int] = []
    
    let modifier = AnyModifier { request in
        let username = "dev"
        let password = "dev"
        let loginString = String(format: "%@:%@", username, password)
        let loginData = loginString.data(using: String.Encoding.utf8)!
        let base64LoginString = loginData.base64EncodedString()
        
        var r = request
        r.setValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")
        return r
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collectionRef.delegate = self
        collectionRef.dataSource = self
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension TMHomeScreenCell2: UICollectionViewDataSource, UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return subcategoryData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TMHomeSubCategoryCell", for: indexPath) as! TMHomeSubCategoryCell
        cell.subcategoryName.text = subcategoryData[indexPath.row]
        print("Image URL",subcategoryImage[indexPath.row])
        if isInstant{
            cell.boltIcon.isHidden = false
        }else{
            cell.boltIcon.isHidden = true
        }
        
        cell.subcategoryImage.kf.setImage(with: URL(string: "\(subcategoryImage[indexPath.row])")!, placeholder: UIImage(named: "painting-paint-roller-"), options: [.requestModifier(modifier)])
        //  cell.subcategoryImage.kf.setImage(with: URL(string: "http://taskmario.dev.webcastle.in/api/thumb/T2Ytz2tvf58taagDMQbbelLwib1KiMCt5JCdv2Ix.jpeg/100/100"), placeholder: UIImage(named: "painting-paint-roller-"), options: [.requestModifier(modifier)])
        //http://taskmario.dev.webcastle.in/api/thumb/T2Ytz2tvf58taagDMQbbelLwib1KiMCt5JCdv2Ix.jpeg/100/100
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let subCategoryID = subcategoryID[indexPath.row]
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMQuestionsVC") as! TMQuestionsVC
        AppState.answerForDisplay.removeAll()
        vc.titleName = subcategoryData[indexPath.row]
        vc.subQuestionID = subCategoryID
        navigationAccess.accessNavigation(viewCon: vc)
    }
}
