//
//  TMHomeSubCategoryCell.swift
//  TaskMario
//
//  Created by Appzoc on 06/02/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class TMHomeSubCategoryCell: UICollectionViewCell {
 
    @IBOutlet weak var boltIcon: UIImageView!
    
    @IBOutlet weak var subcategoryImage: UIImageView!
    
    @IBOutlet weak var subcategoryName: UILabel!
}
