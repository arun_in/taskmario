//
//  TMHomeSceenCell3.swift
//  TaskMario
//
//  Created by Appzoc on 26/02/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class TMHomeSceenCell3: UITableViewCell {
    
    var navigationControl:AccessNavigation?
    
    
    @IBOutlet weak var servicesCountIndicator: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func viewAllServicesBTN(_ sender: UIButton) {
        print("Presenting vc")
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMSearchController") as! TMSearchController
        vc.category = "All"
        self.navigationControl?.accessNavigation(viewCon: vc)
    }
    

}

protocol AccessNavigation{
    func accessNavigation(viewCon:UIViewController)
    func presentThroughController(viewCon:UIViewController)
}
