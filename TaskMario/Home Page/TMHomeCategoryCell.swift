//
//  TMHomeCategoryCell.swift
//  TaskMario
//
//  Created by Appzoc on 06/02/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class TMHomeCategoryCell: UICollectionViewCell {
    
    
    @IBOutlet weak var categoryImage: UIImageView!
    
    @IBOutlet weak var categoryLabel: UILabel!
    
}
