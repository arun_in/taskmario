//
//  TMHomeScreenCell1.swift
//  TaskMario
//
//  Created by Appzoc on 06/02/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import Kingfisher

class TMHomeScreenCell1: UITableViewCell {
    
    
    @IBOutlet weak var collectionRef: UICollectionView!
    
    var navAccess:AccessNavigation?
    var categoryData:[Categorymain] = []
    
    let modifier = AnyModifier { request in
        let username = "dev"
        let password = "dev"
        let loginString = String(format: "%@:%@", username, password)
        let loginData = loginString.data(using: String.Encoding.utf8)!
        let base64LoginString = loginData.base64EncodedString()
        
        var r = request
        r.setValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")
        return r
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collectionRef.delegate = self
        collectionRef.dataSource = self
        collectionRef.isScrollEnabled = false
        
        let numberOfItems: Double = 8
        var numberOfRows = Int(numberOfItems/4)
        let remainder = numberOfItems.truncatingRemainder(dividingBy: 4)
        print("Remainder",remainder)
        if remainder > 0{
            numberOfRows += 1
        }
        let height:CGFloat = CGFloat(numberOfRows * 100)
        
        collectionRef.frame = CGRect(x: collectionRef.frame.minX, y: collectionRef.frame.minY, width: collectionRef.frame.width, height: height)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        
    }
    
    

}

extension TMHomeScreenCell1: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TMHomeCategoryCell", for: indexPath) as! TMHomeCategoryCell
        cell.categoryImage.kf.setImage(with: URL(string: "\(categoryData[indexPath.row].categoryimage)")!, placeholder: UIImage(named: "painting-paint-roller-"), options: [.requestModifier(modifier)])
        cell.categoryImage.kf.setImage(with: URL(string: "\(categoryData[indexPath.row].categoryimage)")!, options: [.requestModifier(modifier)], completionHandler: {image,err,type,url in
            cell.categoryImage.image = cell.categoryImage.image!.withRenderingMode(.alwaysTemplate)
            cell.categoryImage.tintColor = AppState.blueColor
        })
        
        /*
         let templateImage = originalImage.imageWithRenderingMode(UIImageRenderingModeAlwaysTemplate)
         myImageView.image = templateImage
         myImageView.tintColor = UIColor.orangeColor()
 */
        
        cell.categoryLabel.text = categoryData[indexPath.row].categoryname
        cell.layer.shouldRasterize = true
        cell.layer.rasterizationScale = UIScreen.main.scale
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
            
            let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
            let cellsCount = 4
            let totalSpace = flowLayout.sectionInset.left
                + flowLayout.sectionInset.right
                + (flowLayout.minimumInteritemSpacing * CGFloat(cellsCount - 1))
            let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(cellsCount))
            
            let height = Int(Double(size) * 1) //0.825
            
            return CGSize(width: size, height: height)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMTabSearchController") as! TMTabSearchController
        
        vc.selectedIndex = indexPath.row
        vc.selectedCategory = categoryData[indexPath.row].catid
            self.navAccess?.accessNavigation(viewCon: vc)
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//
//    }
    
}
