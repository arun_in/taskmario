//
//  TMClosedTasksController.swift
//  TaskMario
//
//  Created by Appzoc on 28/02/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class TMClosedTasksController: UIViewController, JSONParserDelegate {
   
    let activityView = ActivityIndicatorAlter()
    
    var cellState:[Int] = []
    
    var mainData:[APIClosedTasks] = []
    
    var displayData:[APIClosedTasks] = []{
        didSet{
            setCellState()
        }
    }
    
    @IBOutlet weak var tabTitle: UILabel!
    
    @IBOutlet weak var tableRef: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setCellState()
        activityView.start()
        setUpWebCall()
        // Do any additional setup after loading the view.
    }
    
    private func setCellState(){
        for item in 0..<displayData.count{
            cellState.append(0)
        }
        print("CellState",cellState)
    }
    
    @IBAction func homeScreenBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadHomeTab(navigationController: self.navigationController!)
    }
    
    @IBAction func newTaskBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadNewTaskTab(navigationController: self.navigationController!)
    }
    
    @IBAction func tasksListBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadAllTasksTab(navigationController: self.navigationController!)
    }
    
    @IBAction func chatListBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadChatTab(navigationController: self.navigationController!)
    }
    
    @IBAction func profileBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadProfileTab(navigationController: self.navigationController!)
    }
    
    
    @IBAction func backBTN(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func viewProfilePartner(_ sender: IPButton) {
            //TMPartnerProfileController
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMPartnerProfileController") as! TMPartnerProfileController
        if let index = sender.indexPath{
            vc.partnerJob = displayData[index.row].subcat_name
            vc.partnerID = displayData[index.row].HiredPartnerDetails.partner_id
        }
            self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func setUpWebCall(){
        let jsonParser = JSONParser(url: "http://taskmario.dev.webcastle.in/api/closedTasksList", httpMethod: .post, parameters: ["userid":AppState.userData!.userid], delegate: self, into: .ClosedTasks)
        //AppState.userData!.userid
        //To test - 92
        jsonParser.callWebService()
    }
    
    func operateOnJSONResult(model: Any) {
        if let dataModel = model as? [APIClosedTasks]{
            print(dataModel)
            self.mainData = dataModel
            self.displayData = dataModel
            tableRef.reloadData()
            activityView.stop()
        }
    }
    
    func didReturnWithError(error: Error) {
        activityView.stop()
        Banner.showBannerOn(VC: self, title: "Network Error", message: "", style: .warning)
        print(error)
    }
    
    
    @IBAction func cellDetailsCollapse(_ sender: IPButton) {
        cellState[sender.indexPath!.row] = 0
        displayData = mainData
        tabTitle.text = "Closed Tasks"
        tableRef.reloadData()
        //tableRef.reloadRows(at: [sender.indexPath!], with: .right)
    }
    

    @IBAction func cellDetailExpand(_ sender: IPButton) {
        
        displayData = [mainData[sender.indexPath!.row]]
        cellState[0] = 1
        tabTitle.text = mainData[sender.indexPath!.row].subcat_name
        //tableRef.reloadRows(at: [sender.indexPath!], with: .left)
        tableRef.reloadData()
    }
    

}

extension TMClosedTasksController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return displayData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell1 = tableView.dequeueReusableCell(withIdentifier: "TMClosedTaskCellOne") as! TMClosedTaskCellOne
        let cell2 = tableView.dequeueReusableCell(withIdentifier: "TMClosedTaskCellTwo") as! TMClosedTaskCellTwo
        cell1.taskDetailExpandButton.indexPath = indexPath
        cell2.detailCollapseButton.indexPath = indexPath
        cell2.viewProfileBTNRef.indexPath = indexPath
        
        cell1.setUpCellWith(model: displayData[indexPath.row])
        cell2.setUpCellWith(model: displayData[indexPath.row])
        switch cellState[indexPath.row] {
        case 0:
            
            return cell1
        case 1:
            return cell2
        default:
            return cell1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch cellState[indexPath.row]{
        case 0:
            return 87
        case 1:
            return 288 //270
        default:
            return 87
        }
    }
}
