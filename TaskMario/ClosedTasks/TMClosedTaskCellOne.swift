//
//  TMClosedTaskCellOne.swift
//  TaskMario
//
//  Created by Appzoc on 28/02/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import Kingfisher

class TMClosedTaskCellOne: UITableViewCell {
    
    
    @IBOutlet weak var closedTaskImage: UIImageView!
    
    @IBOutlet weak var closedTaskName: UILabel!
    
    @IBOutlet weak var closedTaskLocation: UILabel!
    
    @IBOutlet weak var closedTaskDate: UILabel!
    
    @IBOutlet weak var taskDetailExpandButton: IPButton!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUpCellWith(model:APIClosedTasks){
        self.closedTaskName.text = model.subcat_name
        self.closedTaskLocation.text = model.location
        self.closedTaskImage.kf.setImage(with: URL(string: "\(model.subcat_image)")!, placeholder: UIImage(named: "painting-paint-roller-"), options: [.requestModifier(AppState.modifier)])
        self.closedTaskDate.text = model.closeddate
    }

}


