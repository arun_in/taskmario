//
//  TMClosedTaskCellTwo.swift
//  TaskMario
//
//  Created by Appzoc on 28/02/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import Cosmos

class TMClosedTaskCellTwo: UITableViewCell {
    
    
    @IBOutlet weak var taskImage: StandardImageView!
    
    @IBOutlet weak var taskName: UILabel!
    
    @IBOutlet weak var taskLocation: UILabel!
    
    @IBOutlet weak var taskClosedDate: UILabel!
    
    
    @IBOutlet weak var partnerImage: StandardImageView!
    
    @IBOutlet weak var partnerName: UILabel!
    @IBOutlet weak var partnerLocation: UILabel!
    
    @IBOutlet weak var partnerRating: CosmosView!
    
    @IBOutlet weak var numberOfTasks: UILabel!
    @IBOutlet weak var partnerDescription: UILabel!
    
    @IBOutlet weak var bidAmount: UILabel!
    
    @IBOutlet weak var viewProfileBTNRef: IPButton!
    
    @IBOutlet weak var detailCollapseButton: IPButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUpCellWith(model:APIClosedTasks){
        self.taskName.text = model.subcat_name
        self.taskLocation.text = model.location
        self.taskImage.kf.setImage(with: URL(string: "\(model.subcat_image)")!, placeholder: UIImage(named: "painting-paint-roller-"), options: [.requestModifier(AppState.modifier)])
        self.taskClosedDate.text = model.closeddate
        
        self.partnerName.text = model.HiredPartnerDetails.partner_name
        self.partnerLocation.text = "\(model.HiredPartnerDetails.partner_location) \(model.HiredPartnerDetails.partnerdistance) Km Away"
        self.partnerRating.rating = model.HiredPartnerDetails.partner_rating
        self.numberOfTasks.text = "\(model.HiredPartnerDetails.tasksCompleted) tasks"
        self.bidAmount.text = "Rs.\(model.bidamount) "
        self.partnerDescription.text = model.HiredPartnerDetails.description
        self.partnerImage.kf.setImage(with: URL(string: "\(model.HiredPartnerDetails.partner_image)")!, placeholder: UIImage(named: "painting-paint-roller-"), options: [.requestModifier(AppState.modifier)])
    }

}
