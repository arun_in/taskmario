//
//  TMNotificationCell.swift
//  TaskMario
//
//  Created by Appzoc on 20/03/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class TMNotificationCell: UITableViewCell {
    
    
    @IBOutlet weak var notificationTitle: UILabel!
    @IBOutlet weak var notificationDate: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
