//
//  TMAboutController.swift
//  TaskMario
//
//  Created by Appzoc on 27/02/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class TMAboutController: UIViewController {
    
    
    @IBOutlet weak var scrollRef: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollRef.contentSize = CGSize(width: UIScreen.main.bounds.width, height: 558)
        // Do any additional setup after loading the view.
    }

    
    @IBAction func backBtN(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //TabBarControls
    
    @IBAction func homeScreenBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadHomeTab(navigationController: self.navigationController!)
    }
    @IBAction func newTaskBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadNewTaskTab(navigationController: self.navigationController!)
    }
    @IBAction func tasksBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadAllTasksTab(navigationController: self.navigationController!)
    }
    
    @IBAction func chatBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadChatTab(navigationController: self.navigationController!)
    }
    
    @IBAction func profileScreenBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadProfileTab(navigationController: self.navigationController!)
    }
    

}
