//
//  TMViewNotificationsController.swift
//  TaskMario
//
//  Created by Appzoc on 20/03/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class TMViewNotificationsController: UIViewController, UITableViewDataSource, UITableViewDelegate, JSONParserDelegate {
    
    
    @IBOutlet weak var tableRef: UITableView!
    
    var notificationSource:[APINotifications] = []{
        didSet{
            if notificationSource.count == 0 {
                listEmptyIndicator.isHidden = false
            }else{
                listEmptyIndicator.isHidden = true
            }
        }
    }
    var activityView = ActivityIndicatorAlter()
    
    @IBOutlet weak var listEmptyIndicator: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableRef.tableFooterView = UIView()
        activityView.start()
        setUpWebCall()
    }
    
    func setUpWebCall(){
        let jsonParser = JSONParser(url: "http://taskmario.dev.webcastle.in/api/notificationLists", httpMethod: .post, parameters: ["userid":AppState.userData!.userid], delegate: self, into: .Notifications)
        jsonParser.callWebService()
    }
    
    func operateOnJSONResult(model: Any) {
        if let dataModel = model as? [APINotifications]{
            notificationSource = dataModel
            tableRef.reloadData()
            print(dataModel.first as Any)
            activityView.stop()
        }
    }
    
    func didReturnWithError(error: Error) {
        activityView.stop()
        Banner.showBannerOn(VC: self, title: "Network Error", message: " ", style: .warning)
        print(error)
    }
    
    
    @IBAction func backBTNaction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notificationSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TMNotificationCell") as! TMNotificationCell
        
        cell.notificationTitle.text = notificationSource[indexPath.row].notification_name
        cell.notificationDate.text = notificationSource[indexPath.row].notification_date
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    //TabBarControls
    
    @IBAction func homeScreenBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadHomeTab(navigationController: self.navigationController!)
    }
    @IBAction func newTaskBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadNewTaskTab(navigationController: self.navigationController!)
    }
    @IBAction func tasksBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadAllTasksTab(navigationController: self.navigationController!)
    }
    
    @IBAction func chatBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadChatTab(navigationController: self.navigationController!)
    }
    
    @IBAction func profileScreenBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadProfileTab(navigationController: self.navigationController!)
    }


}
