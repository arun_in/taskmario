//
//  TMMyProfileController.swift
//  TaskMario
//
//  Created by Appzoc on 08/02/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import ImagePicker
import Alamofire
import Cosmos
import Kingfisher
import GooglePlaces
import DropDown
import MapKit

class TMMyProfileController: UIViewController, ImagePickerDelegate, JSONParserDelegate, UITextFieldDelegate, CLLocationManagerDelegate, UITextViewDelegate {
    
   
    var image = UIImage()

    @IBOutlet weak var scrollRef: UIScrollView!
    
    @IBOutlet weak var profileImage: StandardImageView!
    
    @IBOutlet weak var userName: UILabel!
    
    @IBOutlet weak var userLocation: UILabel!
    
    @IBOutlet weak var numberOfJobPosted: UILabel!
    
    @IBOutlet weak var numberOfHired: UILabel!
    
    @IBOutlet weak var numberOfCompleted: UILabel!
    
    @IBOutlet weak var mobileNumber: UITextField!
    
    @IBOutlet weak var emailAddress: UITextField!
    
    @IBOutlet weak var rating: UILabel!
    
    @IBOutlet weak var starRating: CosmosView!
    
    @IBOutlet weak var numberOfReviews: UILabel!
    
    @IBOutlet weak var howToRateText: UILabel!
    
    @IBOutlet weak var addressField: UITextView!
    
    @IBOutlet weak var addressDropDownAnchor: UIView!
    
    @IBOutlet weak var editProfileBTNRef: StandardButton!
    
    @IBOutlet var emailVerifyBTNRef: UIButton!
    
    @IBOutlet weak var emailUnderline: UIView!
    
    @IBOutlet weak var addressUnderline: UIView!
    
    
    
    var latitude:String = AppState.latitude
    var longitude:String = AppState.longitude
    var placeID:[String] = []
    var dropDownList:[String] = []
    let dropDown = DropDown()
    var fetcher:GMSAutocompleteFetcher = GMSAutocompleteFetcher()
    
    var city:String = ""
    var district:String = ""
    var state:String = ""
    var country:String = ""
    
    let activityView = ActivityIndicatorAlter()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetcher.delegate = self
        
        addressField.delegate = self
        scrollRef.contentSize = CGSize(width: UIScreen.main.bounds.width, height: 809)
        setUpDropDown()
        
       // ActivityIndicator.setUpActivityIndicator(baseView:self.view)
        setUpBorder(textView: addressField, on: false)
        //setUpBorder(textField: mobileNumber, on: false)
        setUpBorder(textField: emailAddress, on: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        activityView.start()
        setUpWebCall()
    }
    
    
    func textViewDidChange(_ textView: UITextView) {
        fetcher.sourceTextHasChanged(textView.text)
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.text = nil
       // setUpBorder(textView: textView, on: true)
        scrollRef.setContentOffset(CGPoint(x: 0, y: 420), animated: true)
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
       // setUpBorder(textView: textView, on: false)
        scrollRef.setContentOffset(CGPoint(x: 0, y: 380), animated: true)
    }
    
    func setUpBorder(textView:UITextView, on:Bool){
//        DispatchQueue.main.async {
//            let border = CALayer()
//            border.borderColor = UIColor.blue.cgColor
//            var width = CGFloat(0)
//            if on{
//                width = CGFloat(1.0)
//                border.frame = CGRect(x: 0, y: textView.frame.size.height - width, width:  textView.frame.size.width, height: textView.frame.size.height)
//
//                border.borderWidth = width
//                textView.layer.addSublayer(border)
//                textView.layer.masksToBounds = true
//            }else{
//                //width = CGFloat(0)
//
//                if let layers = textView.layer.sublayers{
//                    if layers.count > 1 {
//                        textView.layer.sublayers?.removeLast()
//                    }
//                }
//            }
//
//        }
        if on{
            addressUnderline.isHidden = false
        }else{
            addressUnderline.isHidden = true
        }
    }
    
    func setUpBorder(textField:UITextField, on:Bool){
        if on{
            emailUnderline.isHidden = false
        }else{
            emailUnderline.isHidden = true
        }
        
    }

    
//    func setUpBorder(textField:UITextField, on:Bool){
//        DispatchQueue.main.async {
//
//            let border = CALayer()
//            border.borderColor = UIColor.blue.cgColor
//            var width = CGFloat(0)
//            if on{
//                width = CGFloat(1.0)
//                border.frame = CGRect(x: 0, y: textField.frame.size.height - width, width:  textField.frame.size.width, height: textField.frame.size.height)
//
//                border.borderWidth = width
//                textField.layer.addSublayer(border)
//                textField.layer.masksToBounds = true
//            }else{
//                //width = CGFloat(0)
//                //textField.layer.sublayers?.removeAll()
//                border.removeFromSuperlayer()
//            }
//        }
//
//    }
    
    
    @IBAction func backBTN(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //TabBarControls
    
    @IBAction func homeScreenBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadHomeTab(navigationController: self.navigationController!)
    }
    @IBAction func newTaskBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadNewTaskTab(navigationController: self.navigationController!)
    }
    @IBAction func tasksBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadAllTasksTab(navigationController: self.navigationController!)
    }
    
    @IBAction func chatBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadChatTab(navigationController: self.navigationController!)
    }
    
    @IBAction func profileScreenBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadProfileTab(navigationController: self.navigationController!)
    }
    
    @IBAction func emailVerifyTapped(_ sender: UIButton) {
        let segueVC = storyboard?.instantiateViewController(withIdentifier: "TMMyProfileEmailVerificationVC") as! TMMyProfileEmailVerificationVC
        segueVC.emailID = emailAddress.text ?? ""
        segueVC.userName = userName.text ?? ""
        present(segueVC, animated: true, completion: nil)
    }
    
    func fetchAddress(location: CLLocation, completion: @escaping (String, String, String, String) -> ())
    {
        CLGeocoder().reverseGeocodeLocation(location)
        { placemarks, error in
            
            var place = ""
            var adminArea = ""
            var locality = ""
            var country = ""
            
            if let error = error
            {
                print(error)
            }
            
            if let p1 = placemarks?.first?.name{
                place = p1
            }
            if let p2 = placemarks?.first?.administrativeArea{
               adminArea = p2
            }
            if let p3 = placemarks?.first?.subAdministrativeArea{
                locality = p3
            }
            if let p4 = placemarks?.first?.country{
                country = p4
            }
               // let subAdministrativeArea = placemarks?.first?.subAdministrativeArea,
               // let postalCode = placemarks?.first?.postalCode,
               // let country =  placemarks?.first?.country
            
                completion(place, adminArea, locality, country)
            }
        }
    
    
    @IBAction func editProfileDetails(_ sender: StandardButton) {
        
        if sender.titleLabel?.text == "Edit"{
            sender.setTitle("Save", for: [])
            mobileNumber.isUserInteractionEnabled = false
            emailAddress.isUserInteractionEnabled = true
            addressField.isUserInteractionEnabled = true
            
            setUpBorder(textView: addressField, on: true)
            //setUpBorder(textField: mobileNumber, on: false)
            setUpBorder(textField: emailAddress, on: true)
            
        }else if sender.titleLabel?.text == "Save"{
            sender.setTitle("Edit", for: [])
           // mobileNumber.isUserInteractionEnabled = false
            emailAddress.isUserInteractionEnabled = false
            addressField.isUserInteractionEnabled = false
            
            setUpBorder(textView: addressField, on: false)
            //setUpBorder(textField: mobileNumber, on: false)
            setUpBorder(textField: emailAddress, on: false)
            if addressField.text != nil{
                if emailAddress.text != "" && emailAddress.text != " "{
                    if let emailUnwrapped = emailAddress.text {
                        if BaseValidator.isValid(email: emailUnwrapped) {
                            var nameTemp = ""
                            if AppState.registerData.name == "", let name = userName.text{
                                nameTemp = name
                            }else{
                                nameTemp = "\(AppState.registerData.name)"
                            }
                            
                            //if let lat = Double(latitude), let long = Double(longitude){
                                fetchAddress(location: CLLocation(latitude:Double(latitude)! , longitude: Double(longitude)!), completion: { (place, subAdminArea, locality, country) in
                                   
                                    if self.addressField.text == ""{
                                        self.latitude = "0"
                                        self.longitude = "0"
                                        self.city = " "
                                        self.district = " "
                                        self.country = " "
                                    }
                                    
                                    if self.latitude != "0" && self.longitude != "0"{
                                    self.city = place
                                    self.district = subAdminArea
                                    self.country = country
                                    }
                                    
//                                    if self.latitude == "0" && self.longitude == "0"{
//                                        self.latitude = ""
//                                        self.longitude = ""
//                                    }
                                    
                                    
                                    let editedData = ["userid":AppState.userData!.userid,
                                                      "email":"\(emailUnwrapped)",
                                        "name":"\(nameTemp)",
                                        "city":"\(self.city)",
                                        "district":"\(self.district)",
                                        "state":" ",
                                        "country":"\(self.country)",
                                        "latitude":"\(self.latitude)",
                                        "longitude":"\(self.longitude)"
                                        ] as [String : Any]
                                    
                                    
                                    
                                    self.uploadEditedProfileDetails(parameters: editedData)
                                })
                           // }
                            
                            
                        }else{
                            editProfileBTNRef.setTitle("Save", for: [])
                            //mobileNumber.isUserInteractionEnabled = false
                            emailAddress.isUserInteractionEnabled = true
                            addressField.isUserInteractionEnabled = true
                            
                            setUpBorder(textView: addressField, on: true)
                            //setUpBorder(textField: mobileNumber, on: false)
                            setUpBorder(textField: emailAddress, on: true)
                            Banner.showBannerOn(VC: self, title: "Enter A Valid Email ID", message: "", style: .danger)
                            print("Alert Invalid Email")
                        }
                    }
                }else{
                    editProfileBTNRef.setTitle("Save", for: [])
                    //mobileNumber.isUserInteractionEnabled = false
                    emailAddress.isUserInteractionEnabled = true
                    addressField.isUserInteractionEnabled = true
                    
                    setUpBorder(textView: addressField, on: true)
                    //setUpBorder(textField: mobileNumber, on: false)
                    setUpBorder(textField: emailAddress, on: true)

                    Banner.showBannerOn(VC: self, title: "Email ID is mandatory", message: "", style: .danger)
                }
            }
        }
    }
    
    
    func uploadEditedProfileDetails(parameters:[String:Any]){
        activityView.start()
        Alamofire.request("http://taskmario.dev.webcastle.in/api/editProfile", method:.post, parameters: parameters, encoding: JSONEncoding.default , headers: nil).authenticate(user: "dev", password: "dev").responseJSON {
            (response) in
            let jsonData = response.result.value as? [String:Any]
            print(jsonData)
            switch response.result{
            case .success(_):
                self.activityView.stop()
                 Banner.showBannerOn(VC: self, title: "", message: "Profile Edited Successfully", style: .success)
                //ActivityIndicator.setUpActivityIndicator(baseView: self.view)
                self.setUpWebCall()
            case.failure(let err):
                print(err)
                Banner.showBannerOn(VC: self, title: "Network Error", message: " ", style: .warning)
            }
            
        }
    }
    
    @IBAction func changeProfileImage(_ sender: UIButton) {
        let imagePickerController = ImagePickerController()
        imagePickerController.imageLimit = 1
        imagePickerController.delegate = self
        present(imagePickerController, animated: true, completion: nil)
    }
    
    //MARK: Webcall
    
    func setUpWebCall(){
        let jsonParser = JSONParser(url: "http://taskmario.dev.webcastle.in/api/myProfile", httpMethod: .post, parameters: ["userid":AppState.userData!.userid], delegate: self, into: .MyProfile)
        jsonParser.callWebService()
    }
    
    func operateOnJSONResult(model: Any) {
        if let dataObject = model as? APIMyProfile{
            print(dataObject.emailid)
            self.userName.text = dataObject.username
            //Update AppState
            AppState.registerData.name = dataObject.username
            AppState.registerData.email = dataObject.emailid
            
            
            self.profileImage.kf.setImage(with: URL(string: "\(dataObject.profile_image)")!, placeholder: UIImage(named: "dpPlaceHolder"), options: [.requestModifier(AppState.modifier)])
            self.numberOfJobPosted.text = "\(dataObject.no_ofjobsposted)"
            self.numberOfHired.text = "\(dataObject.no_ofhired)"
            self.numberOfCompleted.text = "\(dataObject.no_oftaskcompleted)"
            self.mobileNumber.text = "+\(dataObject.country_code) \(dataObject.mobile)"
            self.emailAddress.text = "\(dataObject.emailid)"
            self.rating.text = "\(dataObject.rating)"
            self.numberOfReviews.text = "Based on \(dataObject.no_ofreviews) reviews"
            self.howToRateText.text = "\(dataObject.howtorate)"
            if dataObject.email_verification == 0{
                emailVerifyBTNRef.isHidden = false
            }else{
                emailVerifyBTNRef.isHidden = true
            }
            if dataObject.user_location == "North Atlantic Ocean," || self.addressField.text == nil || dataObject.user_location == "" {
                self.addressField.text = "Choose Location"
                self.userLocation.text = ""
            }else{
                self.addressField.text = "\(dataObject.user_location)"
                self.userLocation.text = dataObject.user_location
            }
            
        }
        activityView.stop()
    }
    
    func didReturnWithError(error: Error) {
        activityView.stop()
        Banner.showBannerOn(VC: self, title: "Network Error", message: " ", style: .warning)
        print(error)
    }
    
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        
    }
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        self.image = images.first!
        print(image)
        //self.profileImage.image = images.first!
        imagePicker.dismiss(animated: true) {
            self.activityView.start()
            self.uploadImage()
        }
        //ActivityIndicator.setUpActivityIndicatorWith(title: "Uploading...", baseView: self.view)
 
//        postMethodMultiPartImage(url: "http://taskmario.dev.webcastle.in/api/changeprofileimage", parameter: ["userid":"92"], imageParameter: ["image":[self.image]]) { (status, dictionary) in
//            print(dictionary)
//        }
    }
    
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    
    func uploadImage(){
        
        //let imgData = UIImageJPEGRepresentation(image, 0.2)!
        let imgData = UIImageJPEGRepresentation(image, 1)
        
        var keyValue = "\(AppState.userData!.userid)"
        let valueData = Data(bytes: &keyValue, count: MemoryLayout.size(ofValue: keyValue))
        
        //let alterValue = NSData(bytes: &keyValue, length: MemoryLayout<Int>.size)
        print(valueData)
        let parameters = ["userid": keyValue]
        
        
        let username = "dev"
        let password = "dev"
        
        let credentialData = "\(username):\(password)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString()
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(imgData!, withName: "image",fileName: "image.jpg", mimeType: "image/jpg")
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: .utf8)!, withName: key)
            }
        }, usingThreshold: UInt64.init(), to: "http://taskmario.dev.webcastle.in/api/changeprofileimage", method: .post, headers: ["Authorization": base64Credentials]) { (result) in
            switch result {
            case .success(let upload, _, _):
                self.activityView.start()
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                    if progress.fractionCompleted >= 1{
//                        self.activityView.stop()
//                        self.setUpWebCall()
                    }
                })

                upload.responseJSON { response in
                    print(response.result)
                    print(response.result.value)
                    switch response.result{
                    case .success(_):
                        self.profileImage.image = self.image
                        self.activityView.stop()
                        Banner.showBannerOn(VC: self, title: "", message: "Upload Successful", style: .success)
                    case .failure(let err):
                        print(err)
                        Banner.showBannerOn(VC: self, message: "Upload Failed")
                        self.activityView.stop()
                    }
                    //self.activityView.stop()
                    //self.setUpWebCall()

//                    do{
//                    let a = try JSONSerialization.data(withJSONObject: response.result.value, options: .prettyPrinted)
//                    print(a)
//                    }
//                    catch{
//                        print(error)
//                    }
                    
                }

            case .failure(let encodingError):
                print(encodingError)
                Banner.showBannerOn(VC: self, title: "Network Error", message: "Upload Failed", style: .warning)
                self.activityView.stop()
            }
        }
        
        
//        //From Net
//        Alamofire.upload(multipartFormData: { (multipartFormData) in
//            multipartFormData.append(imgData!, withName: "picture", fileName: "Profile_Image.jpeg", mimeType: "image/jpeg")
//        }, to:"http://taskmario.dev.webcastle.in/api/changeprofileimage")
//        { (result) in
//            switch result {
//            case .success(let upload, _, _):
//                print(result)
//
//                upload.uploadProgress(closure: { (progress) in
//                    print(progress)
//                })
//
//                upload.responseJSON { response in
//                    //print response.result
//                    print(response);
//                }
//
//            case .failure(let encodingError):
//                print(encodingError);
//            }
//        }
//
        
        
        
        
        
        
//        Alamofire.upload(multipartFormData: { multipartFormData in
//            multipartFormData.append(imgData, withName: "fileset",fileName: "file.jpg", mimeType: "image/jpg")
//            for (key, value) in parameters {
//                multipartFormData.append(valueData, withName: key)
//            }
//        },
//                         to:"http://taskmario.dev.webcastle.in/api/changeprofileimage")
//        { (result) in
//            switch result {
//            case .success(let upload, _, _):
//
//                upload.uploadProgress(closure: { (progress) in
//                    print("Upload Progress: \(progress.fractionCompleted)")
//                })
//
//                upload.responseJSON { response in
//                    print(response.result.value)
//                }
//
//            case .failure(let encodingError):
//                print(encodingError)
//            }
//        }
//
//
        
        
        
        
        
        
    }
    
    
    func postMethodMultiPartImage(url:String, parameter:[String:Any], imageParameter: [String:[UIImage]] , CompletionHandler:@escaping ((Bool,[String:Any])->())) {
        
        print("\(url)")
        
        let user = "dev"
        let password = "dev"
        let credentialData = "\(user):\(password)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)","Content-Type":"application/json"]
        
        Alamofire.upload(multipartFormData:
        { multipartFormData in
                
                for (key, value) in parameter
                {
                    multipartFormData.append((value as! String).data(using: .utf8)!, withName: key)
                }
                
                if !imageParameter.isEmpty {
                    let imageKey = imageParameter[imageParameter.startIndex].key
                    let imageArray = imageParameter[imageKey]
                    var index: Int = 1
                    for image in imageArray! {
                        let imageData = UIImageJPEGRepresentation(image, 1)!
                        let imageFileName = imageKey + index.description + ".jpeg"
                        multipartFormData.append(imageData, withName: imageKey, fileName: imageFileName, mimeType: "image/jpeg")
                        index += 1
                    }
                    
                }
                
        },
                         to: "\(url)",
            headers : headers,
            encodingCompletion:
            { encodingResult in
                
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    
                    upload.responseJSON(completionHandler: { response in
                        print(response.result)
                        if response.result.value != nil
                        {
                            let result = response.result.value as! [String:Any]
                            debugPrint(result)
                            
                            let Errorcode = result["Error_Code"] as? String ?? "1"
                            let Message = result["Message"] as? String ?? "Error occured while fetching data"
                            
                            if Errorcode == "0"
                            {
                                CompletionHandler(true, result)
                            }
                            else
                            {   CompletionHandler(false, ["":""])
                            }
                        }
                    })
                    
                case .failure(_):
                    CompletionHandler(false, ["":""])
                    
                    
                }
        }
        )
        
    }
    
    
    func setUpDropDown(){
        dropDown.anchorView = addressDropDownAnchor
        dropDown.direction = .bottom
        dropDown.dataSource = dropDownList
        dropDown.selectionAction = {
            index,item in
            self.addressField.text = item
            print(item)
            self.dropDown.hide()
            
            GMSPlacesClient.shared().lookUpPlaceID(self.placeID[index]){place, error in
                if let error = error {
                    print("lookup place id query error: \(error.localizedDescription)")
                    return
                }
                
                guard let place = place else {
                    print("No place details for \(self.placeID)")
                    return
                }
                
                self.latitude = "\(place.coordinate.latitude)"
                self.longitude = "\(place.coordinate.longitude)"
                AppState.latitude = self.latitude
                AppState.longitude = self.longitude
                print(place.formattedAddress)
                //self.locationLabel.text = place.name
                print("Latitude ",self.latitude)
                print("Longitude ",self.longitude)
                
              //  print("Place name \(place.name)")
              //  print("Place address \(place.formattedAddress!)")
                
            }
            
            self.addressField.resignFirstResponder()
        }
    }
    
    
    
    
    
}

extension TMMyProfileController:GMSAutocompleteFetcherDelegate{
    func didAutocomplete(with predictions: [GMSAutocompletePrediction]) {
        dropDownList.removeAll()
        
        var tempPlaceID:[String] = []
        for prediction in predictions {
            dropDownList.append(prediction.attributedPrimaryText.string)
            
            tempPlaceID.append(prediction.placeID!)
            //print("Place ID:\(prediction.placeID)")
            //print("\n",prediction.attributedFullText.string)
            //print("\n",prediction.attributedPrimaryText.string)
            //print("\n********")
        }
        placeID = tempPlaceID
        dropDown.dataSource = dropDownList
        dropDown.show()
    }
    
    func didFailAutocompleteWithError(_ error: Error) {
        print(error)
    }
    
    
}


