//
//  ProfileTabController.swift
//  TaskMario
//
//  Created by Appzoc on 26/02/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import Kingfisher
import Alamofire

class ProfileTabController: UIViewController, JSONParserDelegate {
    
    
    @IBOutlet weak var notificationIndicator: StandardView!
    
    @IBOutlet weak var profileImage: StandardImageView!
    
    @IBOutlet weak var profileLocation: UILabel!
    @IBOutlet weak var profileName: UILabel!
    
    var reloadCount = 2
    
    let activityView = ActivityIndicatorAlter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //checkForNewNotification()
        //setUpWebCall()
        //ActivityIndicator.setUpActivityIndicator(baseView: self.view)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        checkForNewNotification()
        setUpWebCall()
    }
    
    func checkForNewNotification(){
        activityView.start()
        Alamofire.request("http://taskmario.dev.webcastle.in/api/profileMenu", method: .post, parameters: ["userid":AppState.userData!.userid], encoding: URLEncoding.default, headers: nil).authenticate(user: "dev", password: "dev").responseJSON { (response) in
            switch response.result{
            case .success(let data):
                let formattedData = response.result.value as! [String:Any]
                let tempData = formattedData["Data"] as! [String:Bool]
                let notificationStatus = tempData["notification"]!
                if notificationStatus{
                    self.notificationIndicator.isHidden = false
                }else{
                    self.notificationIndicator.isHidden = true
                }
            case .failure(let err):
                Banner.showBanner(title: "Network Error", message: "", style: .danger)
                print(err)
            }
            self.activityView.stop()
           // print("Notification Status",notificationStatus)
        }
    }
    
    func setUpWebCall(){
        activityView.start()
        let jsonParser = JSONParser(url: "http://taskmario.dev.webcastle.in/api/myProfile", httpMethod: .post, parameters: ["userid":AppState.userData!.userid], delegate: self, into: .MyProfile)
        jsonParser.callWebService()
        
    }
    
    func operateOnJSONResult(model: Any) {
        if let dataModel = model as? APIMyProfile{
            self.profileName.text = dataModel.username
            //self.profileLocation.text = dataModel.user_location
            self.profileImage.kf.setImage(with: URL(string: "\(dataModel.profile_image)")!, placeholder: UIImage(named: "dpPlaceHolder"), options: [.requestModifier(AppState.modifier)])
            if dataModel.user_location == "North Atlantic Ocean," || self.profileLocation.text == nil{
                self.profileLocation.text = ""
            }else{
                self.profileLocation.text = "\(dataModel.user_location)"
            }
            activityView.stop()
        }
    }
    
    func didReturnWithError(error: Error) {
        activityView.stop()
        reloadCount -= 1
        if Connectivity.connectionAvailable() && reloadCount > 0{
            self.setUpWebCall()
        }else{
            Banner.showBannerOn(VC: self, message: "Network Error")
            print(error)
        }
    }
    
    @IBAction func myProfileSelect(_ sender: UIButton) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMMyProfileController") as! TMMyProfileController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func notificationSelect(_ sender: UIButton) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMViewNotificationsController") as! TMViewNotificationsController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func closedTaskSelect(_ sender: UIButton) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMClosedTasksController") as! TMClosedTasksController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func supportCenterSelect(_ sender: UIButton) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMSupportCenterController") as! TMSupportCenterController
        Banner.showBanner(message: "Work in Progress")
       // self.navigationController?.pushViewController(vc, animated: true)
        //self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func aboutSelect(_ sender: UIButton) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMAboutController") as! TMAboutController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func rateTaskmarioSelect(_ sender: UIButton) {
        Banner.showBanner(message: "Work in Progress")

    }
    
    @IBAction func registerAsPartnerSelect(_ sender: UIButton) {
        Banner.showBanner(message: "Work in Progress")

    }
    
    @IBAction func logoutSelect(_ sender: UIButton) {
        AppState.isLoggedIn = false
        UserDefaults.standard.removeObject(forKey: "location")
        UserDefaults.standard.removeObject(forKey: "lat")
        UserDefaults.standard.removeObject(forKey: "long")
        UserDefaults.standard.removeObject(forKey: "userData")
        UserDefaults.standard.removeObject(forKey: "regData")
        LoadTabBar.shared.loadHomeTab(navigationController: self.navigationController!)
    }
    
    //TabBar Controls
    @IBAction func homeScreenBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadHomeTab(navigationController: self.navigationController!)
    }
    @IBAction func newTaskBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadNewTaskTab(navigationController: self.navigationController!)
    }
    @IBAction func tasksBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadAllTasksTab(navigationController: self.navigationController!)
    }
    
    @IBAction func chatBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadChatTab(navigationController: self.navigationController!)
    }
    
    @IBAction func profileScreenBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadProfileTab(navigationController: self.navigationController!)
    }

}
