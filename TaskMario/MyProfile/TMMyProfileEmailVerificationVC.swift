//
//  TMMyProfileEmailVerificationVC.swift
//  TaskMario
//
//  Created by Appzoc on 24/04/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import Alamofire

class TMMyProfileEmailVerificationVC: UIViewController {

    @IBOutlet var nameLBL: UILabel!
    @IBOutlet var mailLBL: UILabel!
    
    final var userName: String = ""
    final var emailID: String = ""
    let activityView = ActivityIndicatorAlter()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        nameLBL.text = userName + " using TASKMARIO,"
        let attributedEmailId = NSAttributedString(string: emailID, attributes: [NSAttributedStringKey.foregroundColor : UIColor.TMButtonBlue()])
        
        let blackColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)

        let attributedAppendString = NSAttributedString(string: "email ID ", attributes: [NSAttributedStringKey.foregroundColor : blackColor])

        let fullAttributedString = NSMutableAttributedString()
        fullAttributedString.append(attributedAppendString)
        fullAttributedString.append(attributedEmailId)

        mailLBL.attributedText = fullAttributedString
        
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func clickToVerifyTapped(_ sender: UIButton) {
        postData()
    }
    
    fileprivate func postData(){
        activityView.start()
        Alamofire.request("http://taskmario.dev.webcastle.in/api/emailVerification", method: .post, parameters: ["userid":AppState.userData!.userid], encoding: URLEncoding.default, headers: nil).authenticate(user: "dev", password: "dev").responseJSON { (response) in
            print(response.result.value as Any)
            switch response.result {
            case .success( _):
                //Banner.showBannerOn(VC: self, message: "Email ID Verification Request Send")
                Banner.showBannerOn(VC: self, title: "", message: "Email ID Verification Request Send", style: .success)
                DispatchQueue.main.async {
                    self.activityView.stop()
                    self.dismiss(animated: true, completion: nil)
                }
            case .failure( _):
                Banner.showBannerOn(VC: self, message: "Network Error")
                DispatchQueue.main.async {
                    self.activityView.stop()
                    self.dismiss(animated: true, completion: nil)
                }
            }
            
            
          
        }
    }

}
