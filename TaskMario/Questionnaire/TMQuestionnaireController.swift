//
//  TMQuestionnaireController.swift
//  TaskMario
//
//  Created by Appzoc on 16/02/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class TMQuestionnaireController: UIViewController {
    
    @IBOutlet weak var questionLabel: UILabel!
    
    @IBOutlet weak var tableRef: UITableView!
    
    var cellHeight:[CGFloat] = []
    
    var selectedCell:Int = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for item in 0...5{
            cellHeight.append(411)
        }
    }
    
    func tableScroll(){
        let index = tableRef.indexPathsForVisibleRows?.first
        tableRef.scrollToRow(at: index!, at: .bottom, animated: true)
    }

    

}

extension TMQuestionnaireController: UITableViewDataSource, UITableViewDelegate, SetTableControl{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TMQuestionaireSectionCell") as! TMQuestionaireSectionCell
        let cellBlank = tableView.dequeueReusableCell(withIdentifier: "TMQuestionaireBlankCell") as! TMQuestionaireBlankCell
        cell.mainTableController = self
        cell.mainTableDelegate = self
        cell.selectedCell = indexPath.row
        
        if indexPath.row == 0{
        return cellBlank
        }else{
            if indexPath.row % 2 == 0{
                cell.backgroundColor = UIColor.red
            }else{
                cell.backgroundColor = UIColor.black
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
        return 120
        }else{
            return cellHeight[indexPath.row]
        }
    }
    
    func setRowHeight() {
        print("Set Height call")
        let index = IndexPath(row: self.selectedCell, section: 0)
        UIView.animate(withDuration: 1) {
            self.cellHeight[self.selectedCell] = 237
            self.tableRef.reloadRows(at: [index], with: .none)
            self.tableRef.scrollToRow(at: index, at: .top, animated: true)
        }
        
    }
    
    
}
