//
//  TMQuestionaireAnswersCell.swift
//  TaskMario
//
//  Created by Appzoc on 16/02/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class TMQuestionaireAnswersCell: UITableViewCell {
    
    var tableRef:UITableView?
    
    var tableController:TMQuestionaireSectionCell?
    
    var mainTableController:TMQuestionnaireController?
    
    var mainTableDelegate:SetTableControl?
    
    
    @IBOutlet weak var cellButton: StandardButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func optionSelected(_ sender: StandardButton) {
        //sender.tag = 2
        tableController?.optionSelected = true
        let visibleCellHeight = CGFloat(55)
        //CGFloat((tableRef?.visibleCells.count)!) * 52
        UIView.animate(withDuration: 1) {
           self.tableRef?.frame = CGRect(x: (self.tableRef?.frame.minX)!, y: (self.tableRef?.frame.minY)!, width: (self.tableRef?.bounds.width)!, height: visibleCellHeight)
        }
        
        mainTableDelegate?.setRowHeight()
        mainTableDelegate?.selectedCell = sender.tag
        tableRef?.reloadData()
        //let index = tableRef?.indexPathForRow(at: CGPoint(x: 100, y: 157))
       // mainTableController?.tableRef.scrollToRow(at: index!, at: .bottom, animated: true)
       // mainTableController?.tableRef.contentOffset = CGPoint(x: 0, y: 420)
       // print(mainTableController?.questionLabel.text ?? "qwertty")
        //tableRef?.scrollToRow(at: index!, at: .bottom, animated: true)
    }
    

}

protocol SetTableControl {
    var selectedCell:Int {get set}
    func setRowHeight()
}
