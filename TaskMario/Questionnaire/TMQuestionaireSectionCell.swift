//
//  TMQuestionaireSectionCell.swift
//  TaskMario
//
//  Created by Appzoc on 16/02/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class TMQuestionaireSectionCell: UITableViewCell {
    
    @IBOutlet weak var questionLabel: UILabel!
    
    @IBOutlet weak var tableRef: UITableView!
    
    
    var mainTableDelegate:SetTableControl?
    
    var mainTableController:TMQuestionnaireController?
    
    var selectedCell:Int = 0
    
    var optionSelected:Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        tableRef.delegate = self
        tableRef.dataSource = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    

}

extension TMQuestionaireSectionCell: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if optionSelected{
            return 1
        }else{
        return 2
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TMQuestionaireAnswersCell") as! TMQuestionaireAnswersCell
        cell.tableRef = self.tableRef
        cell.tableController = self
        cell.mainTableController = self.mainTableController
        cell.mainTableDelegate = self.mainTableDelegate
        cell.cellButton.tag = selectedCell
        cell.cellButton.setTitle("Someone Else \(indexPath.row)", for: [])
//        if optionSelected{
//            if cell.cellButton.tag == 2{
//                return cell
//            }
//        }else{
//
//        }
        if cell.cellButton.tag == 1{
            cell.cellButton.isHidden = true
            cell.cellButton.tag = 0
        }
        return cell
    }
    
    
    
}
