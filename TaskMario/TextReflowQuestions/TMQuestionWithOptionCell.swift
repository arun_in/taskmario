//
//  TMQuestionWithOptionCell.swift
//  TaskMario
//
//  Created by Appzoc on 19/02/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class TMQuestionWithOptionCell: UITableViewCell, SelectAnswerDelegate {
    
    var checkSubQuestion:CheckSubQuestion!
    
    var checkForSubQuestion:SubQuestionIdentifier!
    
    var isCheckBox:Bool = false
    
    var parentIndex:IndexPath?
    
    var selectedIndexPath:IndexPath?
    var prevIndexPath:IndexPath?

    var questionForCell:QuestionHandler!{
        didSet{
            questionLabel.text = questionForCell.question
        }
    }
    
    var answerForCell:[AnswerHandler] {
        return questionForCell.answers
    }
    
    var tableScroller:TableScroller?
    
    @IBOutlet weak var questionLabel: UILabel!
    
    @IBOutlet weak var collectionRef: UICollectionView!
    
    @IBOutlet weak var continueButton: IPButton!
    
    var collectionReference: UICollectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout.init())
    
    var questionAnswered:Bool = false
    
    var selectedIndex:IndexPath? = nil
    
    var optionsRestoration:Int? = nil
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setUpCell()
        collectionRef.delegate = self
        collectionRef.dataSource = self
        collectionReference = self.collectionRef
    }
    
    var buttonState:[Int] = [0,0,0,0]
    
    func setButtonState(title:String) {
       var index = 0
        for item in answerForCell{
            if item.label == title{
                buttonState[index] = 1
            }
            index += 1
        }
        collectionReference.reloadData()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func continueAction(_ sender: IPButton) {
        let mainView = tableScroller?.referencingTable.superview
       // tableScroller?.scrollTableDown(frame: (mainView?.convert(sender.frame, from: sender.superview))!)
        if isCheckBox{
            var answerFromCheckBox = ""
            var indexer = 0
            for item in AppState.checkBoxAnswered{
                if item == 1{
                    answerFromCheckBox.append(" \(AppState.checkBoxLabels[indexer])")
                }
                 indexer += 1
            }
            print("Answered State",AppState.checkBoxAnswered)
           print("Answer From CheckBox",answerFromCheckBox)
            if let senderIndex = sender.indexPath?.row{
                if AppState.answerForDisplay.count > senderIndex-2 {
                    AppState.answerForDisplay[senderIndex-2] = answerFromCheckBox
                }else{
                    AppState.answerForDisplay.append(answerFromCheckBox)
                }
            }
           AppState.checkBoxLabels.removeAll()
           AppState.checkBoxAnswered = [0,0,0,0]
        }else{
                if let senderIndex = sender.indexPath{
                    if AppState.answerForDisplay.count > senderIndex.row - 2{
                        AppState.answerForDisplay[senderIndex.row - 2] = " \(AppState.radioButtonSelected)"
                    }else{
                        AppState.answerForDisplay.append(" \(AppState.radioButtonSelected)")
                      //  AppState.textReflowTableRef.reloadData()
                    }
                }
        }
        checkForSubQuestion.checkForSubQuestion()
        tableScroller?.scrollTableDown(button: sender)
        AppState.textReflowTableRef.reloadData()
    }
    
    func setUpCell(){
        
    }
    
}

extension TMQuestionWithOptionCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return answerForCell.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TMOptionsCollectionCell", for: indexPath) as! TMOptionsCollectionCell
        cell.questionForCell = self.questionForCell
      //  cell.optionButton.setTitleColor(UIColor.blue, for: [])
        //cell.backgroundColor = UIColor.white
        let checkBoxCell = collectionView.dequeueReusableCell(withReuseIdentifier: "TMCheckBoxCollectionCell", for: indexPath) as! TMCheckBoxCollectionCell
        if isCheckBox{
            checkBoxCell.checkBox.tag = indexPath.row
            AppState.checkBoxLabels.append(answerForCell[indexPath.row].label)
            checkBoxCell.optionLabel.text = "\(answerForCell[indexPath.row].label)"
            return checkBoxCell
        }else{
            
            if selectedIndexPath == indexPath{
                cell.optionButton.backgroundColor = UIColor.blue
                cell.optionButton.setTitleColor(UIColor.white, for: .normal)
               // selectedIndexPath = nil
            }
//            else if prevIndexPath == indexPath{
//                cell.optionButton.backgroundColor = UIColor.white
//                cell.optionButton.setTitleColor(UIColor.blue, for: .normal)
//                //selectedIndexPath = nil
//            }
            else{
                cell.optionButton.backgroundColor = UIColor.white
                cell.optionButton.setTitleColor(UIColor.blue, for: .normal)
            }
        cell.checkSubQuestion = self.checkSubQuestion
        cell.optionButton.indexPath = parentIndex
        cell.optionButton.tag = questionForCell.id
        cell.optionButton.setTitle("\(answerForCell[indexPath.row].label)", for: [])
        cell.selectedAnswer = self
            
//          print("IndexPath",indexPath.row, buttonState[indexPath.row])
//            if buttonState[indexPath.row] == 0{
//                cell.optionButton.setTitleColor(UIColor.blue, for: [])
//                cell.optionButton.backgroundColor = UIColor.white
//            }
//            if buttonState[indexPath.row] == 1{
//                cell.optionButton.setTitleColor(UIColor.white, for: [])
//                cell.optionButton.backgroundColor = UIColor.blue
//            }
    //----------------------------------------------------------------------
//        if cell.optionButton.indexPath != self.selectedIndex{
//            selectOneOption(button: cell.optionButton)
//        }
        
//        if let option = optionsRestoration{
//            print("Option",option)
//            if indexPath.row == option{
//                cell.buttonSelected = true
//                cell.setButtonState()
//            }else{
//                cell.buttonSelected = false
//                cell.setButtonState()
//            }
//        }
        return cell
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TMOptionsCollectionCell", for: indexPath) as! TMOptionsCollectionCell
        if let cell = collectionView.cellForItem(at: indexPath) as? TMOptionsCollectionCell{
            print("Option CLicked", indexPath.row)
            
            AppState.radioButtonSelected = (cell.optionButton.titleLabel?.text)!
            
               // if questionForCell.hasCondition == 1{
                    AppState.subquestionIdentifier[questionForCell.id] = cell.optionButton.titleLabel!.text!
               // }
//            if AppState.answerForDisplay.count > (cell.optionButton.indexPath?.row)!{
//            AppState.answerForDisplay[(cell.optionButton.indexPath?.row)!] = (cell.optionButton.titleLabel?.text)!
//            }else{
//                AppState.answerForDisplay.append((cell.optionButton.titleLabel?.text)!)
//            }
//            print(AppState.answerForDisplay)
            
            prevIndexPath = selectedIndexPath
            selectedIndexPath = indexPath
            collectionView.reloadItems(at: [indexPath])
            if prevIndexPath != nil{
            collectionView.reloadItems(at: [prevIndexPath!])
            }
        }
//        DispatchQueue.main.async {
//            cell.optionButton.backgroundColor = UIColor.blue
//            cell.optionButton.setTitleColor(UIColor.white, for: .normal)
//        }
        
        
    }
    
    

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
            let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
            let cellsCount = 2
            let totalSpace = flowLayout.sectionInset.left
                + flowLayout.sectionInset.right
                + (flowLayout.minimumInteritemSpacing * CGFloat(cellsCount - 1))
            let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(cellsCount))
            //let size = CGFloat(UIScreen.main.bounds.width * 0.4186)
            
            let height = Int(Double(size) * 0.5) //0.825
           // print("size",size,"Height",height)
            return CGSize(width: size, height: height)
            
//        }
        
    }
    
    
    func selectOneOption(button:IPButton){
        button.backgroundColor = UIColor.white
        button.setTitleColor(UIColor.blue, for: [])
    }
    
}

