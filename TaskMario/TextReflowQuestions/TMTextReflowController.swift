//
//  TMTextReflowController.swift
//  TaskMario
//
//  Created by Appzoc on 19/02/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class TMTextReflowController: UIViewController, TableScroller,JSONParserDelegate, CheckSubQuestion, AccessNavigation, SubQuestionIdentifier {
    
    var navigationReference:UINavigationController?
    
    let activityView = ActivityIndicatorAlter()
    
    var addedSubQuestions:[OptionToCheck] = []
    
    var optionToCheck: OptionToCheck = OptionToCheck(label: "", index: IndexPath(row: 0, section: 0)){
        didSet{
            //checkForSubQuestion()
        }
    }
    
    var tableViewReferencer: UITableView = UITableView()
    
    var lastCellIndex:Int = 0
    
    var isRegisterScreen:Bool = false
    
    var subQuestionID:Int!{
        didSet{
            AppState.selectedSubCategory = subQuestionID
            print("SubQuestionID ",subQuestionID)
        }
    }
    
    var referencingTable: UITableView = UITableView(){
        didSet{
            //0.3015
            cellOneHeight = referencingTable.frame.height * 0.3015
            print("cellOneHeight",cellOneHeight)
            cellTwoHeight = referencingTable.frame.height * 0.6984
            print("cellTwoHeight",cellTwoHeight)
        }
    }
    
    @IBOutlet weak var tableRef: UITableView!{
        didSet{
//            //0.3015
//            cellOneHeight = tableRef.frame.height * 0.3015
//            print("cellOneHeight",cellOneHeight)
//            cellTwoHeight = tableRef.frame.height * 0.6984
//            print("cellTwoHeight",cellTwoHeight)
        }
    }
    
    var mainQuestions:[QuestionHandler] = []
    var displayMainQuestions:[QuestionHandler] = []
    var subQuestions:[QuestionHandler] = []
    
    var answeredArray:[Int] = []
    var questionCount = 8
    
    var rowToReload:IndexPath? = nil
    
    var cellOneHeight:CGFloat = 0
    
    var cellTwoHeight:CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isRegisterScreen == false{
            activityView.start()
        //ActivityIndicator.setUpActivityIndicator(baseView: self.view)
            
        }
        
        AppState.textReflowTableRef = self.tableRef

//            if let nav = self.navigationController{
//                navigationAccess = nav
//            }
//
        print("navigation controller:",self.navigationController)
        
        tableRef.register(UINib(nibName: "TMTextfieldTableCell", bundle: nil), forCellReuseIdentifier: "TMTextfieldTableCell")
        tableRef.register(UINib(nibName: "TMProceedTableCell", bundle: nil), forCellReuseIdentifier: "TMProceedTableCell")
        tableRef.register(UINib(nibName: "TMDatePickerCell", bundle: nil), forCellReuseIdentifier: "TMDatePickerCell")
        referencingTable = self.tableRef
       // let startIndex = IndexPath(row: 0, section: 0)
       // referencingTable.scrollToRow(at: startIndex, at: .bottom, animated: true)
        for item in 1...questionCount{
            answeredArray.append(0)
        }
        if isRegisterScreen{
            
        }else{
        setUpWebCall()
        }
        tableViewReferencer = tableRef
        // Do any additional setup after loading the view.
    }
    
    //["unix_time":"1800","token_key":"asdasd","subcategoryid":"20"]
    
    func setUpRegisterationData(){
        
    }
    
    func accessNavigation(viewCon: UIViewController) {
        DispatchQueue.main.async {
            if let nav = self.navigationController{
                nav.pushViewController(viewCon, animated: true)
            }else{
                self.navigationReference?.pushViewController(viewCon, animated: true)
            }
            
        }
        
    }
    
    func presentThroughController(viewCon: UIViewController) {
        self.present(viewCon, animated: true, completion: nil)
    }
    
    @IBAction func closeControllerBTN(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
//    func checkForSubQuestion(){
//        if optionToCheck.label != ""{
//            for sub in subQuestions{
//                if sub.conditionalValue == optionToCheck.label{
//                    addedSubQuestions.append(optionToCheck)
//                    mainQuestions.insert(sub, at: optionToCheck.index.row-2)
//                    for item in mainQuestions{
//                    print("\nMain Question ",item.question)
//                        print("\n")
//                    }
//                    print("\n\n\n")
//                    tableRef.reloadData()
//                }else{
//                    for item in addedSubQuestions{
//                        if item.index == optionToCheck.index{
//                            mainQuestions.remove(at: optionToCheck.index.row-2)
//                            tableRef.reloadData()
//                        }
//                    }
////                    for ques in mainQuestions{
////                        if sub.question == ques.question{
////                            mainQuestions.remove(at: count)
////                            tableRef.reloadData()
////                        }
////                        count += 1
////                    }
//                }
//            }
//        }
//    }
    
    func checkForSubQuestion(){
        displayMainQuestions = mainQuestions
        var index = 1
        for mainQ in displayMainQuestions{
            if mainQ.hasCondition == 1{
              for subQ in subQuestions{
                    if AppState.subquestionIdentifier[mainQ.id] == subQ.conditionalValue{
                        if subQ.conditional == mainQ.id{
                            if displayMainQuestions.count > index{
                                displayMainQuestions.insert(subQ, at: index)
                            }else{
                                displayMainQuestions.append(subQ)
                            }
                        }
                    
                }
              }
            }
           index += 1
        }
       // tableRef.reloadData()
    }
    
    
    
    func setUpWebCall() {
        print("Sub:\(subQuestionID)")
    let jsonParser = JSONParser(url: "http://taskmario.dev.webcastle.in/api/questionnaire", httpMethod: .post, parameters: ["subcategoryid":"\(subQuestionID!)"], delegate: self, into:.Questionaire)
       // let jsonParser = JSONParser(url: "http://taskmario.dev.webcastle.in/api/listCategories", httpMethod: .get, parameters: nil, delegate: self, into:.HomePage)
        jsonParser.callWebService()
    }
    
    func operateOnJSONResult(model: Any) {
        if let questionairre = model as? [QuestionsAPI]{
            mainQuestions.removeAll()
            subQuestions.removeAll()
        //   print("Question",questionairre.first?.question)
            for question in questionairre{
                let mainQuestion = QuestionHandler(question: question)
               // print("Input Type",mainQuestion.inputType)
                mainQuestions.append(mainQuestion)
                if mainQuestion.hasCondition == 1{
                    for sub in question.subquestions{
                    let subQuestion = QuestionHandler(subquestion: sub)
                        subQuestions.append(subQuestion)
                    }
                }
            }
            displayMainQuestions = mainQuestions
            print("Main Questions")
            for q in mainQuestions{
                print(q.question)
                print(q.inputType)
            }
            print("Sub Questions")
            for q in subQuestions{
                print(q.question)
                //print(q.answers)
                print("Conditional Value - ",q.conditionalValue)
                //print(q.id)
                print(q.inputType)
            }
            
           
            
            DispatchQueue.main.async {
                self.tableRef.reloadData()
                self.activityView.stop()
            }
          
        }
        
       // print("\n\n\n\nMain Question\n",mainQuestions,"\n\n\n\nSub Questions\n",subQuestions)
    }
    
    func didReturnWithError(error: Error) {
        print(error)
    }

    
    
}


extension TMTextReflowController: UITableViewDelegate, UITableViewDataSource{
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isRegisterScreen{
            return 5
        }else{
        return displayMainQuestions.count+3
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       // print("Cell for row called : Index:",indexPath.row)
        let answeredCell = tableView.dequeueReusableCell(withIdentifier: "TMAnsweredCell") as! TMAnsweredCell
        let questionWithOptionCell = tableView.dequeueReusableCell(withIdentifier: "TMQuestionWithOptionCell") as! TMQuestionWithOptionCell
        
        let textfieldCell = tableView.dequeueReusableCell(withIdentifier: "TMTextfieldTableCell") as! TMTextfieldTableCell
        let datePickerCell = tableView.dequeueReusableCell(withIdentifier: "TMDatePickerCell") as! TMDatePickerCell
        //let checkBoxCell = tableView.dequeueReusableCell(withIdentifier: "")
        
        let proceedCell = tableView.dequeueReusableCell(withIdentifier: "TMProceedTableCell") as! TMProceedTableCell
        proceedCell.accessNav = self
        proceedCell.isFromRegister = self.isRegisterScreen
        
        
        answeredCell.tableScroller = self
        questionWithOptionCell.tableScroller = self
        textfieldCell.tableScroller = self
        datePickerCell.tableScroller = self
        
        questionWithOptionCell.checkForSubQuestion = self
        
        datePickerCell.continueBTNRef.indexPath = indexPath
        answeredCell.continueButton.indexPath = indexPath
        questionWithOptionCell.continueButton.indexPath = indexPath
        textfieldCell.continueButtonRef.indexPath = indexPath
        questionWithOptionCell.checkSubQuestion = self
        let newParentIndex = IndexPath(row: indexPath.row-1, section: indexPath.section)
        questionWithOptionCell.parentIndex = newParentIndex
        AppState.parentIndexForCheckBox = newParentIndex
        
       let blankCell = UITableViewCell(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height * 0.6))
        
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
        if isRegisterScreen{
            //answeredArray = [0,0,0]
            textfieldCell.isFromRegister = true
            textfieldCell.continueButtonRef.tag = indexPath.row
            switch indexPath.row {
                
            case 0:
                return blankCell
            case 1:
                if AppState.registerData.name != ""{
                    answeredCell.questionLabel.text = "Enter your name"
                    answeredCell.questionAnswered = true
                    answeredCell.answerLabel.text = AppState.registerData.name
                    return answeredCell
                }else{
                textfieldCell.questionLabel.text = "Enter your name."
               // if AppState.registerData.name != ""{
                  //  textfieldCell.answerTextField.text = "\(AppState.registerData.name)"
               // }
                textfieldCell.continueButtonRef.tag = indexPath.row-1
                return textfieldCell
                }
                
            case 2:
                if AppState.passedNumberField{
                    answeredCell.questionLabel.text = "Enter your number"
                    answeredCell.questionAnswered = true
                    answeredCell.answerLabel.text = "\(AppState.userLoginData!.0) \(AppState.userLoginData!.1)"
                    return answeredCell
                }else{
                textfieldCell.questionLabel.text = "Registered mobile number."
                textfieldCell.answerTextField.isUserInteractionEnabled = false
                textfieldCell.continueButtonRef.tag = indexPath.row-1
                textfieldCell.answerTextField.text = "\((AppState.userLoginData?.0)!) \((AppState.userLoginData?.1)!)"
                return textfieldCell
                }
                
            case 3:
                if AppState.registerData.email != ""{
                    answeredCell.questionLabel.text = "Enter your email"
                    answeredCell.questionAnswered = true
                    answeredCell.answerLabel.text = AppState.registerData.email
                    return answeredCell
                }else{
                    textfieldCell.questionLabel.text = "Enter your email-id."
                    textfieldCell.answerTextField.text = "\(AppState.registerData.email)"
                    textfieldCell.continueButtonRef.tag = indexPath.row-1
                    return textfieldCell
                }
            case 4:
                return proceedCell
                
            default:
                return answeredCell
            }
            
        }else{
            switch indexPath.row {
            case 0:
                return blankCell
                
            case 1:
                answeredCell.questionLabel.text = " We will ask a few questions to connect you with the right artists"
                answeredCell.answerLabel.isHidden = true
                return answeredCell
                
            default:
                if (indexPath.row-2) == displayMainQuestions.count{
                    
                    return proceedCell
                }else{
                    questionWithOptionCell.questionForCell = displayMainQuestions[indexPath.row-2]
                    if answeredArray[indexPath.row] == 1{
                        answeredCell.questionLabel.text = "\(displayMainQuestions[indexPath.row-2].question)"
                        answeredCell.questionAnswered = true
                        //print(AppState.answerForDisplay.count)
                        if AppState.answerForDisplay.count > indexPath.row-2{
                        answeredCell.answerLabel.text = AppState.answerForDisplay[indexPath.row-2]
                        }
                        // print("Returning Answer Cell")
                        return answeredCell
                    }else{
                       // print("Returning Question Cell")
                        switch displayMainQuestions[indexPath.row-2].inputType{
                        case "text":
                            textfieldCell.questionLabel.text = displayMainQuestions[indexPath.row-2].question
                            return textfieldCell
                        case "checkbox":
                            questionWithOptionCell.isCheckBox = true
                            return questionWithOptionCell
                        case "datetimepicker":
                            return datePickerCell
                            //radio, checkbox, datetimepicker, text, textarea                       autocomplete
                        default:
                            return questionWithOptionCell
                        }
                        
                        
                       
                    }
                }
                
                
               // answeredCell.continueButton.isHidden = true
                //answeredCell.questionLabel.text = "\(indexPath.row + 1). We will ask a few questions to connect you\n with the right artists"
               // return answeredCell
                
            }
        }
        
        
        
       
        
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let height1:CGFloat = 155
        let height2:CGFloat = 359
        
        if isRegisterScreen{
            switch indexPath.row {
            case 0:
                return UIScreen.main.bounds.height * 0.45
            case 1:
                return 208
            //155,359
            case 2:
                return 208
            default:
                return 208
            }
        }else{
        
            switch indexPath.row {
            case 0:
                return 280
            case 1:
                return height1
                //155,359
            case 2:
                if answeredArray[indexPath.row] == 1{
                    return cellOneHeight
                }else{
                    return cellTwoHeight
                }
    //            if let cell = tableView.cellForRow(at: indexPath) as? TMQuestionWithOptionCell{
    //                print("Cast OK")
    //                if answeredArray[indexPath.row] == 1{
    //                    cell.questionAnswered = true
    //                }else{
    //                    cell.questionAnswered = false
    //                }
    //            if cell.questionAnswered{
    //                print(cell.questionAnswered)
    //                return 155
    //            }else{
    //            return 359
    //                }}
    //            else{
    //                return 359
    //            }
            case 3:
                if answeredArray[indexPath.row] == 1{
                    return cellOneHeight
                }else{
                    return cellTwoHeight
                }
            default:
                if (indexPath.row-2) == displayMainQuestions.count{
                    return cellOneHeight
                }else{
                    if answeredArray[indexPath.row] == 1{
                        return cellOneHeight
                    }else{
                        return cellTwoHeight
                    }
                }
                
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if isRegisterScreen{
            switch indexPath.row{
            case 1:
                AppState.registerData.name = ""
                tableView.reloadRows(at: [indexPath], with: .fade)
                tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
            case 2:
                AppState.passedNumberField = false
                tableView.reloadRows(at: [indexPath], with: .fade)
                tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
            case 3:
                AppState.registerData.email = ""
                tableView.reloadRows(at: [indexPath], with: .fade)
                tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
            default:
                print("Blank reverted")
            }
        }else{
            let arrayRepresentation = answeredArray[indexPath.row]
            
            switch indexPath.row {
            case 1:
                if arrayRepresentation == 1{
                    answeredArray[indexPath.row] = 0
                }
                //tableView.reloadRows(at: [indexPath], with: .none)
                DispatchQueue.main.async {
                    self.referencingTable.reloadRowData(index: indexPath) {
                        self.referencingTable.scrollToRow(at: indexPath, at: .bottom, animated: true)
                    }
                }
            default:
                if arrayRepresentation == 1{
                    answeredArray[indexPath.row] = 0
                }
                //tableView.reloadRows(at: [indexPath], with: .none)
                DispatchQueue.main.async {
                    self.referencingTable.reloadRowData(index: indexPath) {
                        self.referencingTable.scrollToRow(at: indexPath, at: .bottom, animated: true)
                    }
                }
            }
        }
        
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
//        DispatchQueue.main.async {
//            //self.referencingTable.reloadRows(at: [button.indexPath!], with: .none)
//            if self.rowToReload != nil{
//            self.referencingTable.reloadRows(at: [self.rowToReload!], with: .none)
//            }
//        }

    }
    
    func scrollTableDown(button:IPButton){
        if (button.indexPath?.row)! < questionCount-1 {
            print("Scrolling Table Down")
            let nextIndex = IndexPath(row: button.indexPath!.row + 1, section: button.indexPath!.section)
            print("Next Index",nextIndex.row)
            print("Button Index",button.indexPath!.row)
            
            if let cell = referencingTable.cellForRow(at: button.indexPath!) as? TMQuestionWithOptionCell{
                cell.questionAnswered = true
                //referencingTable.setNeedsLayout()
                
                
                
                rowToReload = button.indexPath
                print("Row to reload:",rowToReload?.row)
                
    //            if rowToReload != nil{
    //                print("Reloading row:",rowToReload!.row)
    //                referencingTable.reloadRows(at: [rowToReload!], with: .none)
    //            }
                
               //  referencingTable.beginUpdates()
                
               
                
               //  referencingTable.endUpdates()
                //referencingTable.reloadData()
            }
            
            
            
    //        let index = referencingTable.indexPathsForRows(in: frame)
    //        for item in index!{
    //            print("Indices :",item.row)
    //        }
            //print(index!.first!.row)
            //referencingTable.scrollToRow(at: index!.first!, at: .top, animated: true)
            answeredArray[button.indexPath!.row] = 1
            DispatchQueue.main.async {
                self.referencingTable.reloadData()
                self.referencingTable.reloadRowData(index: button.indexPath!) {
                    self.referencingTable.scrollToRow(at: nextIndex, at: .bottom, animated: true)
                }
            }
            
            
            //referencingTable.scrollToRow(at: nextIndex, at: .bottom, animated: true)
           // referencingTable.layoutIfNeeded()
           // answeredArray[button.indexPath!.row] = 1
        
           
           
           
           // print(answeredArray)
            
        }
    }
}
