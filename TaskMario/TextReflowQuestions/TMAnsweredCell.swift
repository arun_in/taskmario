//
//  TMAnsweredCell.swift
//  TaskMario
//
//  Created by Appzoc on 19/02/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class TMAnsweredCell: UITableViewCell {
    
    var tableScroller:TableScroller?
    
    
    @IBOutlet weak var questionLabel: UILabel!
    
    @IBOutlet weak var answerLabel: UILabel!
    
    @IBOutlet weak var continueButton: IPButton!
    
    var questionAnswered:Bool = false{
        didSet{
            if questionAnswered{
                continueButton.isHidden = true
                self.contentView.alpha = 0.5
            }else{
                continueButton.isHidden = false
            }
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func continueAction(_ sender: IPButton) {
        let mainView = tableScroller?.referencingTable.superview
       // tableScroller?.scrollTableDown(frame: (mainView?.convert(sender.frame, from: sender.superview))!)
        DispatchQueue.main.async {
            self.tableScroller?.scrollTableDown(button: sender)
        }
        
        
    }
    
    
}

protocol TableScroller {
    var referencingTable:UITableView{get set}
    func scrollTableDown(button:IPButton)
}
