//
//  TMProceedTableCell.swift
//  TaskMario
//
//  Created by Appzoc on 22/03/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import Alamofire

class TMProceedTableCell: UITableViewCell {
    
    let activityView = ActivityIndicatorAlter()
    
    var accessNav:AccessNavigation!{
        didSet{
            print("AccessNav is set")
        }
    }
    
    var isFromRegister:Bool = false

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func proceedBTNAction(_ sender: StandardButton) {
        //Check Login
        print("Proceeding")
        // activityView.start()
        if let mainView = self.contentView.superview {
            //activityView.start()
           // ActivityIndicator.setUpActivityIndicator(baseView: mainView)
        }
        //let feedback = "[{"answer":"Reception","id":"208"},{"answer":"Videography,Photography","id":"210"}]"
        
        if isFromRegister{
            activityView.start()
            Alamofire.request("http://taskmario.dev.webcastle.in/api/editProfile", method: .post, parameters: ["userid":AppState.userData!.userid,
                            "name":"\(AppState.registerData.name)",
                            "email":"\(AppState.registerData.email)",
                            "city":"",
                            "district":"",
                            "state":"",
                            "country":"",
                            "latitude":"\(AppState.latitude)",
                            "longitude":"\(AppState.longitude)"
                ],
                                  encoding: URLEncoding.default, headers: nil).authenticate(user: "dev", password: "dev").responseJSON { (response) in
                    
                    print(response.result)
                    print(response.result.value)
                    self.activityView.stop()
                    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMHomeScreenController") as! TMHomeScreenController
                    self.accessNav.accessNavigation(viewCon: vc)
                }
            
            
        }else{
           // let feedback = [["answer":"Reception","id":"208"],["answer":"Videography,Photography","id":"210"]]
            var feedback:[[String:String]] = []
            
            for (key,value) in AppState.subquestionIdentifier{
                var diction = ["answer":"\(value)","id":"\(key)"]
                feedback.append(diction)
            }
            
            var feedbackString = "["
            var index = 0
            for item in feedback{
                let tempA = item.description.replacingOccurrences(of: "[", with: "{")
                let tempB = tempA.replacingOccurrences(of: "]", with: "}")
                feedbackString.append(tempB)
                index += 1
                if index < feedback.count{
                    feedbackString.append(", ")
                }
            }
            feedbackString.append("]")
            
            print("Feedback",feedbackString)
            
            AppState.feedback = feedbackString
            AppState.passedNumberField = false
            if AppState.selectedLocation == "Choose Location"{
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMSetLocationController") as! TMSetLocationController
                vc.isFromQuestionairre = true
                self.accessNav.accessNavigation(viewCon: vc)
            }else if AppState.isLoggedIn == false{
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMLoginController") as! TMLoginController
                vc.isFromQuestionairre = true
                self.accessNav.accessNavigation(viewCon: vc)
            }else{
                //Ras-Al-Kaimah - 25.754389, 56.038599
                activityView.start()
                Alamofire.request("http://taskmario.dev.webcastle.in/api/submitTask", method: .post, parameters: ["userid":AppState.userData!.userid, "location":"\(AppState.selectedLocation)", "latitude":"\(AppState.latitude)", "longitude":"\(AppState.longitude)", "category_id":AppState.selectedSubCategory, "feedback": "\(feedbackString)"], encoding: URLEncoding.default, headers: nil).authenticate(user: "dev", password: "dev").responseJSON { (response) in
                    
                    print(response.result)
                    print(response.result.value)
                    self.activityView.stop()
                    
                    //let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMAllTasksController") as! TMAllTasksController
                    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMGoodJobController") as! TMGoodJobController
                    self.accessNav.presentThroughController(viewCon: vc)
                }
                
                
            }
        
        }
        
    }
    
}
