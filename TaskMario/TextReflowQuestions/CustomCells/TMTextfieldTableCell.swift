//
//  TMTextfieldTableCell.swift
//  TaskMario
//
//  Created by Appzoc on 22/03/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class TMTextfieldTableCell: UITableViewCell {
    
    var tableScroller:TableScroller?
    
    var isFromRegister:Bool = false
    
    @IBOutlet weak var questionLabel: UILabel!
    
    @IBOutlet weak var answerTextField: UITextField!
    
    @IBOutlet weak var continueButtonRef: IPButton!
    
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func continueBTN(_ sender: IPButton) {
        if isFromRegister{
            switch sender.tag {
            case 0:
                if let text = answerTextField.text{
                AppState.registerData.name = text
                }
            case 1:
                AppState.passedNumberField = true
            case 2:
                if let text = answerTextField.text{
                    AppState.registerData.email = text
                }
            default:
                print("Ignoring Name")
            }
            print("Register Data",AppState.registerData)
            DispatchQueue.main.async {
                self.tableScroller?.scrollTableDown(button: sender)
                self.tableScroller?.referencingTable.reloadData()
            }
        }else{
            if let text = answerTextField.text{
                
                if let senderIndex = sender.indexPath{
                    if AppState.answerForDisplay.count > senderIndex.row - 2{
                        AppState.answerForDisplay[senderIndex.row - 2] = " \(text)"
                    }else{
                        AppState.answerForDisplay.append(" \(text)")
                        //  AppState.textReflowTableRef.reloadData()
                    }
                }
                
                DispatchQueue.main.async {
                    self.tableScroller?.scrollTableDown(button: sender)
                    self.tableScroller?.referencingTable.reloadData()
                }
            }
            
        }
        
        
    }
    
    
    
    
}
