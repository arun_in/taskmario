//
//  TMDatePickerCell.swift
//  TaskMario
//
//  Created by Appzoc on 12/04/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class TMDatePickerCell: UITableViewCell {
    
    var tableScroller:TableScroller?
    
    @IBOutlet weak var questionLabel: UILabel!
    
    
    @IBOutlet weak var continueBTNRef: IPButton!
    
    @IBOutlet weak var datePickerRef: UIDatePicker!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @IBAction func continueBTN(_ sender: IPButton) {
        let componenets = Calendar.current.dateComponents([.year, .month, .day], from: datePickerRef.date)
        if let day = componenets.day, let month = componenets.month, let year = componenets.year {
            print("\(day) \(month) \(year)")
        }
            DispatchQueue.main.async {
                self.tableScroller?.scrollTableDown(button: sender)
                self.tableScroller?.referencingTable.reloadData()
            }
    }
    
}
