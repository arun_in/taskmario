//
//  TMCheckBoxCollectionCell.swift
//  TaskMario
//
//  Created by Appzoc on 07/04/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class TMCheckBoxCollectionCell: UICollectionViewCell {
    
    var checkBoxSelected = false
    
    @IBOutlet weak var optionLabel: UILabel!
    
    var selectedAnswersForCheckBox:[String] = []{
        didSet{
           // print("Array Count",selectedAnswersForCheckBox.count)
        }
    }

    @IBOutlet weak var checkBox: StandardButton!
    
    @IBAction func toggleCheckBoxBTN(_ sender: StandardButton) {
        if checkBoxSelected{
            checkBoxSelected = false
            checkBox.borderWidth = 1
            checkBox.borderColor = UIColor.lightGray
            checkBox.setImage(UIImage(), for: [])
            AppState.checkBoxAnswered[sender.tag] = 0
//            var index = 0
//            for item in selectedAnswersForCheckBox{
//                if item == optionLabel.text!{
//                    selectedAnswersForCheckBox.remove(at: index)
//                    index += 1
//                }
//            }
            
        }else{
            AppState.checkBoxAnswered[sender.tag] = 1
           // print("Parent Index",(AppState.parentIndexForCheckBox?.row)!-1)
            
          //  selectedAnswersForCheckBox.append(optionLabel.text!)
         //   print(selectedAnswersForCheckBox)
//            var answer = ""
//            for item in selectedAnswersForCheckBox{
//                answer.append("\(item) ")
//            }
//            print(answer)
            
//            if AppState.answerForDisplay.count > ((AppState.parentIndexForCheckBox?.row)!-1){
//              AppState.answerForDisplay[((AppState.parentIndexForCheckBox?.row)!-1)] = "\(answer)"
//            }else{
//                AppState.answerForDisplay.append((optionLabel.text)!)
//            }
            
            checkBox.borderWidth = 0
            checkBox.setImage(UIImage(named: "verification-square-button"), for: [])
            checkBoxSelected = true
        }
    }
    
}
