//
//  TMOptionsCollectionCell.swift
//  TaskMario
//
//  Created by Appzoc on 19/02/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class TMOptionsCollectionCell: UICollectionViewCell {
    
    var questionForCell:QuestionHandler?
    
    @IBOutlet weak var optionButton: IPButton!{
        didSet{
            optionButton.isUserInteractionEnabled = false
        }
    }
    
    var selectedAnswer:SelectAnswerDelegate?
    
    
    var buttonSelected:Bool = false{
        didSet{
            DispatchQueue.main.async {
                self.setButtonState()
            }
        }
    }
    
    var checkSubQuestion:CheckSubQuestion!
    
    
    @IBAction func optionSelected(_ sender: IPButton) {
        
       // sender.backgroundColor = UIColor(red: 0.3, green: 0.3, blue: 1, alpha: 1)
        //sender.setTitleColor(UIColor.white, for: [])
        
        selectedAnswer?.setButtonState(title: "\(sender.titleLabel!.text)")
        let chosenAnswer = ["id":"\(sender.tag)","answer":"\(sender.titleLabel!.text!)"]
        
        //MARK: Bug Chosen Answers
        AppState.chosenAnswers.append(chosenAnswer)
        
        AppState.radioButtonSelected = (sender.titleLabel?.text)!
//        if let senderIndex = sender.indexPath{
//            if AppState.answerForDisplay.count > senderIndex.row - 2{
//                AppState.answerForDisplay[senderIndex.row - 2] = " \(sender.titleLabel!.text!)"
//            }else{
//                AppState.answerForDisplay.append(" \(sender.titleLabel!.text!)")
//              //  AppState.textReflowTableRef.reloadData()
//            }
//        }
        
      //  print(AppState.chosenAnswers)
       // print("Button IndexPath",sender.indexPath?.row)
       // let checkOption = OptionToCheck(label: (sender.titleLabel?.text)!, index: sender.indexPath!)
      //  checkSubQuestion.optionToCheck = checkOption
        //checkSubQuestion.tableViewReferencer.reloadData()
//        selectedAnswer?.selectedIndex = sender.indexPath
//        selectedAnswer?.optionsRestoration = sender.indexPath?.row
//        selectedAnswer?.collectionReference.reloadData()
        
        if buttonSelected{
            self.buttonSelected = false
        }else{
            buttonSelected = true
        }
        
    }
    
    func setButtonState(){
        if buttonSelected{
            optionButton.backgroundColor = UIColor.blue
            optionButton.setTitleColor(UIColor.white, for: [])
        }else{
            optionButton.backgroundColor = UIColor.white
            optionButton.setTitleColor(UIColor.blue, for: [])
        }
    }
    
}

struct OptionToCheck{
    var label:String
    var index:IndexPath
}

protocol SelectAnswerDelegate {
    func setButtonState(title:String)
    var selectedIndex:IndexPath? {get set}
    var collectionReference:UICollectionView{get set}
    var optionsRestoration:Int? {get set}
}

protocol CheckSubQuestion {
    var optionToCheck:OptionToCheck {get set}
    var tableViewReferencer:UITableView {get set}
}
