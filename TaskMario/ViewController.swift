//
//  ViewController.swift
//  TaskMario
//
//  Created by Appzoc on 06/02/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController, JSONParserDelegate {
    
    @IBOutlet weak var progressRef: UIProgressView!
    
    var timer = Timer()
    
    var reloadCount:Int = 2
    
    var loadedData:APIHomePage? = nil
    
    var networkAvailable:Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        AppState.isLoggedIn = false
//        UserDefaults.standard.removeObject(forKey: "location")
//        UserDefaults.standard.removeObject(forKey: "lat")
//        UserDefaults.standard.removeObject(forKey: "long")
//        UserDefaults.standard.removeObject(forKey: "userData")
//        UserDefaults.standard.removeObject(forKey: "regData")
        
        NotificationCenter.default.addObserver(self, selector: #selector(statusManager), name: .flagsChanged, object: Network.reachability)
        updateUserInterface()
        progressRef.transform = progressRef.transform.scaledBy(x: 1, y: 3)
        progressRef.layer.cornerRadius = 3
        progressRef.layer.masksToBounds = true
        progressRef.trackTintColor = UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1)
       // setUpWebCall()
        if #available(iOS 10.0, *) {
            timer = Timer.scheduledTimer(withTimeInterval: 0.01, repeats: true, block: { (timer) in
                self.progressRef.progress += 0.005
                if self.progressRef.progress == 1{
                    timer.invalidate()
                    //self.proceedAfterLoad()
                }
            })
        } else {
            
            
            
            
            // Fallback on earlier versions
        }
        
        separatedWebCall()
        
    }
    
    func separatedWebCall(){
        print("Call Start")
        Alamofire.request("http://taskmario.dev.webcastle.in/api/listCategories").authenticate(user: "dev", password: "dev").responseJSON{
            response in
            //print(response.result.value)
            switch response.result{
            case .success(let jsonData):
                let decoder = JSONDecoder()
                let jsonData = response.result.value as? [String:Any]
                //print(jsonData)
                let dataSegment = jsonData!["Data"] as? [String:Any]
                
                do{
                    let jsonObject = try JSONSerialization.data(withJSONObject: dataSegment!, options: .prettyPrinted)
                    let populatedModel = try decoder.decode(APIHomePage.self, from: jsonObject)
                    // print(populatedModel.Categorymain)
                    self.loadedData = populatedModel
                    self.proceedAfterLoad()
                    
                }catch{
                    print(error)
                }
                
            case .failure(let err):
                self.reloadCount -= 1
                if Connectivity.connectionAvailable() && self.reloadCount > 0{
                    if #available(iOS 10.0, *) {
                        self.timer = Timer.scheduledTimer(withTimeInterval: 0.01, repeats: true, block: { (timer) in
                            self.progressRef.progress += 0.005
                            if self.progressRef.progress == 1{
                                timer.invalidate()
                                //self.proceedAfterLoad()
                            }
                        })
                    } else {
                        // Fallback on earlier versions
                    }
                    self.progressRef.progress = 0
                    self.separatedWebCall()
                }else{
                    Banner.showBannerOn(VC: self, message: "Network Error")
                    print(err)
                }
//                if self.networkAvailable{
//                    if #available(iOS 10.0, *) {
//                        self.timer = Timer.scheduledTimer(withTimeInterval: 0.01, repeats: true, block: { (timer) in
//                            self.progressRef.progress += 0.005
//                            if self.progressRef.progress == 1{
//                                timer.invalidate()
//                                //self.proceedAfterLoad()
//                            }
//                        })
//                    } else {
//                        // Fallback on earlier versions
//                    }
//                    self.progressRef.progress = 0
//                    self.separatedWebCall()
//                }
                //print(err)
            }
            
        }
    }
    
    //["unix_time":"1900","token_key":"123qwerty"]
    
    func updateUserInterface() {
        guard let status = Network.reachability?.status else { return }
        switch status {
        case .unreachable:
            print("No Connection")
            //Banner.showBannerOn(VC: self, title: "Network Error", message: "Network Unreachable", style: .warning)
            //view.backgroundColor = .red
        default:
            print("Network Connected")
//        case .wifi:
//            Banner.showBannerOn(VC: self, title: "Wifi Connected", message: "", style: .success)
//            //view.backgroundColor = .green
//        case .wwan:
//            Banner.showBannerOn(VC: self, title: "WAN Connected", message: "", style: .success)
//            //view.backgroundColor = .yellow
        }
        print("Reachability Summary")
        print("Status:", status)
        print("HostName:", Network.reachability?.hostname ?? "nil")
        print("Reachable:", Network.reachability?.isReachable ?? "nil")
        print("Wifi:", Network.reachability?.isReachableViaWiFi ?? "nil")
    }
    
    @objc func statusManager(_ notification: Notification) {
        updateUserInterface()
    }
    
    func setUpWebCall(){
        let jsonParser = JSONParser(url: "http://taskmario.dev.webcastle.in/api/listCategories", httpMethod: .get, parameters:nil, delegate: self, into: .HomePage)
        jsonParser.callWebService()
    }
    
    func operateOnJSONResult(model: Any) {
        if let homeObject = model as? APIHomePage{
            print("\n\n\nCategory List",homeObject.CategoryList)
        }
    }
    
    func didReturnWithError(error: Error) {
        reloadCount -= 1
        if Connectivity.connectionAvailable() && reloadCount > 0{
            self.setUpWebCall()
        }else{
            Banner.showBannerOn(VC: self, message: "Network Error")
            print(error)
        }
    }
    
    
    func proceedAfterLoad(){
        //let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TutorialScreenStart")
        AppState.navigation = self.navigationController
        AppState.homePageData = loadedData
        
        if let ifFirstTime = UserDefaults.standard.string(forKey: "firstTime"){

            if ifFirstTime == "false"{
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMHomeScreenController") as! TMHomeScreenController

                self.navigationController?.pushViewController(vc, animated: true)
            }
        }else{

            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMOnboardingController") as! TMOnboardingController
        self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    
    
}

