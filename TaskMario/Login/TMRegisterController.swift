//
//  TMRegisterController.swift
//  TaskMario
//
//  Created by Appzoc on 22/03/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import Alamofire

class TMRegisterController: UIViewController {
    
    @IBOutlet weak var nameField: UITextField!
    
    @IBOutlet weak var mobileNumberField: UITextField!
    
    @IBOutlet weak var emailField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mobileNumberField.text = "\(AppState.userLoginData!.0) \(AppState.userLoginData!.1)"
        mobileNumberField.isUserInteractionEnabled = false
        // Do any additional setup after loading the view.
    }
    
    @IBAction func proceedBTN(_ sender: UIButton) {
        if let name = nameField.text, let email = emailField.text{
            let editedData = ["userid":AppState.userData!.userid, "name":name,"email":email,"city":"","district":"","state":"","country":"","latitude":"","longitude":""] as [String : Any]
            uploadEditedProfileDetails(parameters: editedData)
            self.dismiss(animated: true, completion: {
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMHomeScreenController") as! TMHomeScreenController
                self.navigationController?.pushViewController(vc, animated: true)
            })
        }
    }
    
    
    func uploadEditedProfileDetails(parameters:[String:Any]){
        Alamofire.request("http://taskmario.dev.webcastle.in/api/editProfile", method:.post, parameters: parameters, encoding: JSONEncoding.default , headers: nil).authenticate(user: "dev", password: "dev").responseJSON {
            (response) in
            let jsonData = response.result.value as? [String:Any]
            print(jsonData as Any)
        }
    }
    
}
