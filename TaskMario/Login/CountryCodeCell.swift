//
//  CountryCodeCell.swift
//  TaskMario
//
//  Created by Appzoc on 12/02/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import DropDown

class CountryCodeCell: DropDownCell {
    
    
    @IBOutlet weak var countryLabel: UILabel!
    
    @IBOutlet weak var countryFlag: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
