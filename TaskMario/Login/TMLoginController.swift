//
//  TMLoginController.swift
//  TaskMario
//
//  Created by Appzoc on 12/02/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import DropDown
import FBSDKLoginKit
import FacebookLogin
import Alamofire
import NotificationBannerSwift

class TMLoginController: UIViewController, JSONParserDelegate {
    
    let indicator = ActivityIndicatorAlter()
    var isFromQuestionairre:Bool = false
    
    var userData:APILogin?
    
    @IBOutlet weak var fieldCountryImage: UIImageView!
    @IBOutlet weak var fieldCountryLabel: UILabel!
    
    @IBOutlet weak var mobileNumberField: UITextField!
    @IBOutlet weak var dropDownAnchor: UIView!
    
    let dropDown = DropDown()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpDropDown()
       // dropDown.show()
        //dataTaskWebCall()
        
//        postMethod(url: "http://taskmario.dev.webcastle.in/api/normalLogin", parameter: ["country_code":"01","mobile":"9876543210"], CompletionHandler: { (a, b) in
//            //print(b)
//            })
    }
    
    
    @IBAction func closeController(_ sender: UIButton) {
       self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func dropDownClick(_ sender: UIButton) {
        if dropDown.isHidden{
            mobileNumberField.resignFirstResponder()
            dropDown.show()
        }else{
            dropDown.hide()
        }
    }
    
    @IBAction func normalLoginBTN(_ sender: StandardButton) {
        if let countryCode = fieldCountryLabel.text, let mobileNumber = mobileNumberField.text{
            if mobileNumber.count == 10{
                indicator.start()
               // AppState.userLoginData = (countryCode,mobileNumber)
               // print(AppState.userLoginData)
                var passingCountryCode = countryCode
                passingCountryCode.remove(at: passingCountryCode.startIndex)
                print(mobileNumber)
                postMethod(url: "http://taskmario.dev.webcastle.in/api/normalLogin", parameter: ["country_code":"\(passingCountryCode)","mobile":"\(mobileNumber)"], CompletionHandler: { (a, b) in
                    if let _ = AppState.tempUserData{
                        if AppState.tempUserData!.history == "new"{
                        //AppState.shouldRegister = true
                        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMHandleOTPController") as! TMHandleOTPController
                        if self.isFromQuestionairre{
                            vc.isFromQuestionairre = true
                        }
                        vc.userData = (countryCode,mobileNumber)
                        self.indicator.stop()
                        self.navigationController?.pushViewController(vc, animated: true)
                    }else{
                        //AppState.shouldRegister = false
                        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMHandleOTPController") as! TMHandleOTPController
                        vc.userData = (countryCode,mobileNumber)
                        if self.isFromQuestionairre{
                            vc.isFromQuestionairre = true
                        }
                        self.indicator.stop()
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    }else{
                        Banner.showBannerOn(VC: self, message: "Login Failed. Try Again.")
                    }
                })
            }else{
                if mobileNumber.count == 0{
                    Banner.showBannerOn(VC: self, title: "Alert", message: "Enter mobile number", style: .danger)
                }else{
                Banner.showBannerOn(VC: self, title: "Alert", message: "Mobile number length must be 10", style: .danger)
                //print("Number length not 10")
                }
            }
        }
        
    }
    
    func dataTaskWebCall(){
        
        let username = "dev"
        let password = "dev"
        let loginString = String(format: "%@:%@", username, password)
        let loginData = loginString.data(using: String.Encoding.utf8)!
        let base64LoginString = loginData.base64EncodedString()
        
        // create the request
        let url = URL(string: "http://taskmario.dev.webcastle.in/api/normalLogin")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")
        let param = ["country_code":"91","mobile":"9876543210"]
        Alamofire.request("http://taskmario.dev.webcastle.in/api/normalLogin", method:.post, parameters: param, encoding: JSONEncoding.default , headers: ["Authorization":"Basic \(base64LoginString)","Content-Type": "application/json"]).responseJSON {
            (response) in
            print(response.result.value as Any)
            let jsonData = response.result.value as? [String:Any]
            print(jsonData as Any)
        }
        
    }
    
    func postMethod(url:String, parameter:[String:Any], CompletionHandler:@escaping ((Bool,[String:Any])->()))
    {
        
        let user = "dev"
        let password = "dev"
        let credentialData = "\(user):\(password)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)","Content-Type":"application/x-www-form-urlencoded"]
        
        Alamofire.request("\(url)",
            method: .post,
            parameters: parameter,
            encoding: URLEncoding.default,
            headers:headers)
            .responseJSON
            { response in
                print(response.result)
                switch(response.result)
                {
                case .success(_):
                    if response.result.value != nil
                    {
                        let result = response.result.value as! [String:Any]
                        let resultData = result["Data"] as! [String:Any]
                        let loginData = APILogin(data: resultData)
                        AppState.tempUserData = loginData
                        if loginData.isCompleted == 1{
                            AppState.shouldRegister = false
                        }else{
                            AppState.shouldRegister = true
                        }
                        print(loginData)
                    
                        let banner = NotificationBanner(title: loginData.otp.description)
                        banner.show()
                        
                        //print(result)
                        
                        let Errorcode = result["Error_Code"] as? String ?? "1"
                        _ = result["Message"] as? String ?? "Error occured while fetching data"
                        
                        if Errorcode == "0"
                        {
                            CompletionHandler(true, result)
                        }
                        else
                        {   CompletionHandler(false, ["":""])
                        }
                    }
                    
                    break
                    
                case .failure(_):
                    print("Failure")
                    CompletionHandler(false, ["":""])

                }
        }
    }
    
    @IBAction func faceBookLoginBTN(_ sender: UIButton) {
        Facebook.LoginRequest(viewcontroller: self, completion: { (isFinished, dic) in
            if isFinished
            {
                print("Name",dic.name)
                
               // self.profileLabel.text = dic.name
                //self.imageview.image = UIImage(data: dic.imagedata as! Data)
                let param = ["name":"\(dic.name)","email":"\(dic.email)","facebook_id":"\(dic.userId)"]
                
                Alamofire.request("http://taskmario.dev.webcastle.in/api/socialLogin", method:.post, parameters: param, encoding: JSONEncoding.default , headers: nil).authenticate(user: "dev", password: "dev").responseJSON {
                    (response) in
                    //print(response.result.value)
                    //let jsonData = response.result.value as? [String:Any]
                    // print(jsonData)
                    switch response.result{
                    case .success(let data):
                        let result = response.result.value as! [String:Any]
                        let resultData = result["Data"] as! [String:Any]
                        let loginData = APILogin(data: resultData)
                        AppState.userData = loginData
                        if loginData.isCompleted == 0{
                            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMQuestionsVC") as! TMQuestionsVC
                            vc.isRegisterScreen = true
                            self.navigationController?.pushViewController(vc, animated: true)
                        }else{
                            if self.isFromQuestionairre{
                                Alamofire.request("http://taskmario.dev.webcastle.in/api/submitTask", method: .post, parameters: ["userid":AppState.tempUserData!.userid, "location":"\(AppState.selectedLocation)", "latitude":"\(AppState.latitude)", "longitude":"\(AppState.longitude)", "category_id":AppState.selectedSubCategory, "feedback": "\(AppState.feedback)"], encoding: URLEncoding.default, headers: nil).authenticate(user: "dev", password: "dev").responseJSON { (response) in
            
                                    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMGoodJobController") as! TMGoodJobController
                                    self.present(vc, animated: true, completion: nil)
                                }
                            }else{
                                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMHomeScreenController") as! TMHomeScreenController
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                        }
                        
                    case .failure(let err):
                        print(err)
                        Banner.showBannerOn(VC: self, message: "Login Failed")
                    }
                }
            }
            
        })
    }
    
    
    func setUpDropDown(){
        let countryCodes = getCountryCode()
        var countryList:[String] = []
        for item in countryCodes{
            countryList.append(item.name)
        }
        print("Country",countryCodes[0].name)
        dropDown.anchorView = dropDownAnchor
        dropDown.direction = .bottom
        dropDown.dataSource = countryList
        dropDown.cellNib = UINib(nibName: "CountryCodeCell", bundle: nil)
        dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? CountryCodeCell else { return }
            
            // Setup your custom UI components
            cell.countryLabel.text = " \(countryCodes[index].name)  (\(countryCodes[index].dial_code))"
            cell.countryFlag.image = UIImage(named: "\(countryCodes[index].code)")
        }
        dropDown.selectionAction = {
            index, item in
            self.fieldCountryLabel.text = countryCodes[index].dial_code
            self.fieldCountryImage.image = UIImage(named: "\(countryCodes[index].code)")
            //self.dropDown.hide()
        }
    }
    
    func getCountryCode() -> [CountryCodeModel]{
        var countryCode:[CountryCodeModel] = []
        let dataForm = try! JSONSerialization.data(withJSONObject: CountryCodeIndex.data, options: .prettyPrinted)
        let decoder = JSONDecoder()
        do{
        let parsedData = try decoder.decode([CountryCodeModel].self, from: dataForm)
        countryCode = parsedData
        }catch{
            print(error)
        }
        return countryCode
    }
    
    func operateOnJSONResult(model: Any) {
        if let loginData = model as? APILogin{
            AppState.userData = loginData
            print(loginData)
        }
    }
    
    func didReturnWithError(error: Error) {
        print(error)
    }
    
}


/* OLD LoginBTN Code
 
 
 let param = ["country_code":"91","mobile":"9876543210"]
 let username = "dev"
 let password = "dev"
 
 let credentialData = "\(username):\(password)".data(using: String.Encoding.utf8)!
 let base64Credentials = credentialData.base64EncodedString()
 
 // let jsonParser = JSONParser(url: "http://taskmario.dev.webcastle.in/api/myProfile", httpMethod: .post, parameters: param, delegate: self, into: .Login)
 // jsonParser.callWebService()
 Alamofire.request("http://taskmario.dev.webcastle.in/api/normalLogin", method:.post, parameters: param, encoding: URLEncoding.default , headers: ["Authorization":"Basic \(base64Credentials)","Content-Type": "application/json"]).responseJSON {
 (response) in
 print("Error ",response.error)
 print(response.result.value)
 let jsonData = response.result.value as? [String:Any]
 // print(jsonData)
 }
 
 //http://taskmario.dev.webcastle.in/api/myProfile
 //http://taskmario.dev.webcastle.in/api/normalLogin
 
 */


//Zoccers ID- 724894077898316

