//
//  TMHandleOTPController.swift
//  TaskMario
//
//  Created by Appzoc on 14/03/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import Alamofire
import PinCodeTextField


class TMHandleOTPController: UIViewController, UITextFieldDelegate {
    
    var isFromQuestionairre:Bool = false
    
    @IBOutlet weak var displayNumber: UILabel!
    
    
    var userData:(String,String)? = nil
    

    @IBOutlet weak var otpFieldRef: PinCodeTextField!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        otpFieldRef.delegate = self
        otpFieldRef.underlineColor = UIColor(red: 223/255, green: 240/255, blue: 252/255, alpha: 1)
        otpFieldRef.underlineHeight = 1
        otpFieldRef.keyboardType = .numberPad
        
        displayNumber.text = "\(userData!.0) \(userData!.1)"
        
        NotificationCenter.default.addObserver(self, selector: #selector(TMHandleOTPController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(TMHandleOTPController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }

    @IBAction func editMobileNumber(_ sender: UIButton) {
        
      self.navigationController?.popViewController(animated: true)
        
    }
    
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
     //   textField.resignFirstResponder()
        
        if textField.text == "\(AppState.tempUserData!.otp)"{
            print("Verified")
            AppState.tempUserData?.history = "existing"
            AppState.userLoginData = self.userData
            
            if AppState.shouldRegister{
                
                if isFromQuestionairre{
                    Alamofire.request("http://taskmario.dev.webcastle.in/api/submitTask", method: .post, parameters: ["userid":AppState.userData!.userid, "location":"\(AppState.selectedLocation)", "latitude":"\(AppState.latitude)", "longitude":"\(AppState.longitude)", "category_id":AppState.selectedSubCategory, "feedback": "\(AppState.feedback)"], encoding: URLEncoding.default, headers: nil).authenticate(user: "dev", password: "dev").responseJSON { (response) in
                        print(AppState.chosenAnswers)
                        print(response.result)
                        print(response.result.value as Any)
                        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMQuestionsVC") as!TMQuestionsVC
                        //vc.navigationReference = self.navigationController
                        AppState.answerForDisplay.removeAll()
                        vc.isRegisterScreen = true
                        self.present(vc, animated: true, completion: nil)
                    }
                }else{
                    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMQuestionsVC") as!TMQuestionsVC
                    //vc.navigationReference = self.navigationController
                    AppState.answerForDisplay.removeAll()
                    vc.isRegisterScreen = true
                    self.navigationController?.pushViewController(vc, animated: true)
                    //   self.present(vc, animated: true, completion: nil)
                    
                }
                
                
                // self.navigationController?.pushViewController(vc, animated: true)
            }else{
                if isFromQuestionairre{
                    Alamofire.request("http://taskmario.dev.webcastle.in/api/submitTask", method: .post, parameters: ["userid":AppState.userData!.userid, "location":"\(AppState.selectedLocation)", "latitude":"\(AppState.latitude)", "longitude":"\(AppState.longitude)", "category_id":AppState.selectedSubCategory, "feedback": "\(AppState.feedback)"], encoding: URLEncoding.default, headers: nil).authenticate(user: "dev", password: "dev").responseJSON { (response) in
                        print(AppState.chosenAnswers)
                        print(response.result)
                        print(response.result.value as Any)
                       // let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMAllTasksController") as! TMAllTasksController
                        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMGoodJobController") as! TMGoodJobController
                        self.present(vc, animated: true, completion: nil)
                    }
                }else{
                    
                    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMHomeScreenController") as! TMHomeScreenController
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                }
                
                
            }
            
        }else{
            print("Failed")
        }
        print(textField.text as Any)
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        if textField.text == "\(AppState.userData!.otp)"{
            print("Verified")
            AppState.userData?.history = "existing"
            
            if AppState.shouldRegister{
                
                if isFromQuestionairre{
                    Alamofire.request("http://taskmario.dev.webcastle.in/api/submitTask", method: .post, parameters: ["userid":AppState.userData!.userid, "location":"\(AppState.selectedLocation)", "latitude":"\(AppState.latitude)", "longitude":"\(AppState.longitude)", "category_id":AppState.selectedSubCategory, "feedback": "\(AppState.feedback)"], encoding: URLEncoding.default, headers: nil).authenticate(user: "dev", password: "dev").responseJSON { (response) in
                        print(AppState.chosenAnswers)
                        print(response.result)
                        print(response.result.value as Any)
                        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMQuestionsVC") as!TMQuestionsVC
                        //vc.navigationReference = self.navigationController
                        AppState.answerForDisplay.removeAll()
                        vc.isRegisterScreen = true
                        self.present(vc, animated: true, completion: nil)
                    }
                }else{
                    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMQuestionsVC") as!TMQuestionsVC
                    //vc.navigationReference = self.navigationController
                    AppState.answerForDisplay.removeAll()
                    vc.isRegisterScreen = true
                    self.navigationController?.pushViewController(vc, animated: true)
                 //   self.present(vc, animated: true, completion: nil)
                    
                }
                
                
               // self.navigationController?.pushViewController(vc, animated: true)
            }else{
                if isFromQuestionairre{
                    Alamofire.request("http://taskmario.dev.webcastle.in/api/submitTask", method: .post, parameters: ["userid":AppState.userData!.userid, "location":"\(AppState.selectedLocation)", "latitude":"\(AppState.latitude)", "longitude":"\(AppState.longitude)", "category_id":AppState.selectedSubCategory, "feedback": "\(AppState.feedback)"], encoding: URLEncoding.default, headers: nil).authenticate(user: "dev", password: "dev").responseJSON { (response) in
                        print(AppState.chosenAnswers)
                        print(response.result)
                        print(response.result.value as Any)
                        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMAllTasksController") as! TMAllTasksController
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }else{
                    
                    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMHomeScreenController") as! TMHomeScreenController
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                }
                
            
            }
            
        }else{
            print("Failed")
        }
        print(textField.text as Any)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print(textField.text as Any)
        return true
    }
    
    
    @IBAction func resendOTP(_ sender: UIButton) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMLoginController") as! TMLoginController
         print(AppState.userLoginData as Any)
        print("tempData",AppState.userLoginData?.0 as Any)
        print(self.userData!.1 as Any,"userData",self.userData!.0 as Any)
        // AppState.userLoginData!.1
        //AppState.userLoginData!.0
        var countryCode = self.userData!.0
        countryCode.remove(at: countryCode.startIndex)
        vc.postMethod(url: "http://taskmario.dev.webcastle.in/api/normalLogin", parameter: ["country_code":"\(countryCode)","mobile":"\(self.userData!.1)"], CompletionHandler: { (a, b) in
                print(a,b)
            })
        }
    
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y += keyboardSize.height
            }
        }
    }
    
}

extension TMHandleOTPController: PinCodeTextFieldDelegate{
    
    func textFieldValueChanged(_ textField: PinCodeTextField) {
        if textField.text?.count == 4{
            //print(textField.text!)
            if textField.text == "\(AppState.tempUserData!.otp)"{
                print("Verified")
                AppState.tempUserData?.history = "existing"
                AppState.userData = AppState.tempUserData
                AppState.userLoginData = self.userData
                
                if AppState.shouldRegister{
                    
                    if isFromQuestionairre{
                        Alamofire.request("http://taskmario.dev.webcastle.in/api/submitTask", method: .post, parameters: ["userid":AppState.tempUserData!.userid, "location":"\(AppState.selectedLocation)", "latitude":"\(AppState.latitude)", "longitude":"\(AppState.longitude)", "category_id":AppState.selectedSubCategory, "feedback": "\(AppState.feedback)"], encoding: URLEncoding.default, headers: nil).authenticate(user: "dev", password: "dev").responseJSON { (response) in
                            print(AppState.chosenAnswers)
                            print("responseResult",response.result)
                            print("abcdefghi",response.result.value)
                            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMQuestionsVC") as!TMQuestionsVC
                            //vc.navigationReference = self.navigationController
                            AppState.answerForDisplay.removeAll()
                            vc.isRegisterScreen = true
                            self.present(vc, animated: true, completion: nil)
                        }
                    }else{
                        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMQuestionsVC") as! TMQuestionsVC
                        //vc.navigationReference = self.navigationController
                        AppState.answerForDisplay.removeAll()
                        vc.isRegisterScreen = true
                        self.navigationController?.pushViewController(vc, animated: true)
                        //   self.present(vc, animated: true, completion: nil)
                        
                    }
                    
                    
                    // self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    if isFromQuestionairre{
                        if AppState.selectedLocation == "Choose Location"{
                            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMSetLocationController") as! TMSetLocationController
                            vc.isFromQuestionairre = true
                            self.navigationController?.pushViewController(vc, animated: true)
                        }else{
                        Alamofire.request("http://taskmario.dev.webcastle.in/api/submitTask", method: .post, parameters: ["userid":AppState.userData!.userid, "location":"\(AppState.selectedLocation)", "latitude":"\(AppState.latitude)", "longitude":"\(AppState.longitude)", "category_id":AppState.selectedSubCategory, "feedback": "\(AppState.feedback)"], encoding: URLEncoding.default, headers: nil).authenticate(user: "dev", password: "dev").responseJSON { (response) in
                            print(AppState.chosenAnswers)
                            print(response.result)
                            print(response.result.value)
                            // let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMAllTasksController") as! TMAllTasksController
                            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMGoodJobController") as! TMGoodJobController
                            self.present(vc, animated: true, completion: nil)
                        }
                        }
                    }else{
                        
                        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMHomeScreenController") as! TMHomeScreenController
                        self.navigationController?.pushViewController(vc, animated: true)
                        
                    }
                    
                    
                }
                
            }else{
                Banner.showBannerOn(VC: self, title: "Invalid OTP", message: "", style: .warning)
                print("Failed")
            }
        }
    }
    
//    func textFieldDidEndEditing(_ textField: PinCodeTextField) {
//        if textField.text?.count == 4{
//            //print(textField.text!)
//            if textField.text == "\(AppState.userData!.otp)"{
//                print("Verified")
//                AppState.userData?.history = "existing"
//                AppState.userLoginData = self.userData
//
//                if AppState.shouldRegister{
//
//                    if isFromQuestionairre{
//                        Alamofire.request("http://taskmario.dev.webcastle.in/api/submitTask", method: .post, parameters: ["userid":AppState.userData!.userid, "location":"\(AppState.selectedLocation)", "latitude":"\(AppState.latitude)", "longitude":"\(AppState.longitude)", "category_id":AppState.selectedSubCategory, "feedback": "\(AppState.feedback)"], encoding: URLEncoding.default, headers: nil).authenticate(user: "dev", password: "dev").responseJSON { (response) in
//                            print(AppState.chosenAnswers)
//                            print(response.result)
//                            print(response.result.value)
//                            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMTextReflowController") as!TMTextReflowController
//                            vc.navigationReference = self.navigationController
//                            AppState.answerForDisplay.removeAll()
//                            vc.isRegisterScreen = true
//                            self.present(vc, animated: true, completion: nil)
//                        }
//                    }else{
//                        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMTextReflowController") as!TMTextReflowController
//                        vc.navigationReference = self.navigationController
//                        AppState.answerForDisplay.removeAll()
//                        vc.isRegisterScreen = true
//                        self.navigationController?.pushViewController(vc, animated: true)
//                        //   self.present(vc, animated: true, completion: nil)
//
//                    }
//
//
//                    // self.navigationController?.pushViewController(vc, animated: true)
//                }else{
//                    if isFromQuestionairre{
//                        Alamofire.request("http://taskmario.dev.webcastle.in/api/submitTask", method: .post, parameters: ["userid":92, "location":"\(AppState.selectedLocation)", "latitude":"\(AppState.latitude)", "longitude":"\(AppState.longitude)", "category_id":AppState.selectedSubCategory, "feedback": "\(AppState.feedback)"], encoding: URLEncoding.default, headers: nil).authenticate(user: "dev", password: "dev").responseJSON { (response) in
//                            print(AppState.chosenAnswers)
//                            print(response.result)
//                            print(response.result.value)
//                            // let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMAllTasksController") as! TMAllTasksController
//                            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMGoodJobController") as! TMGoodJobController
//                            self.navigationController?.pushViewController(vc, animated: true)
//                        }
//                    }else{
//
//                        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMHomeScreenController") as! TMHomeScreenController
//                        self.navigationController?.pushViewController(vc, animated: true)
//
//                    }
//
//
//                }
//
//            }else{
//                Banner.showBannerOn(VC: self, title: "Invalid OTP", message: "", style: .warning)
//                print("Failed")
//            }
//        }
//    }
    func textFieldDidBeginEditing(_ textField: PinCodeTextField) {
        textField.text = nil
    }
    
    func textFieldShouldReturn(_ textField: PinCodeTextField) -> Bool {
        if textField.text?.count == 4{
            //print(textField.text!)
            if textField.text == "\(AppState.tempUserData!.otp)"{
                print("Verified")
                AppState.tempUserData?.history = "existing"
                AppState.userData = AppState.tempUserData
                AppState.userLoginData = self.userData
                
                if AppState.shouldRegister{
                    
                    if isFromQuestionairre{
                        Alamofire.request("http://taskmario.dev.webcastle.in/api/submitTask", method: .post, parameters: ["userid":AppState.userData!.userid, "location":"\(AppState.selectedLocation)", "latitude":"\(AppState.latitude)", "longitude":"\(AppState.longitude)", "category_id":AppState.selectedSubCategory, "feedback": "\(AppState.feedback)"], encoding: URLEncoding.default, headers: nil).authenticate(user: "dev", password: "dev").responseJSON { (response) in
                            print(AppState.chosenAnswers)
                            print(response.result)
                            print(response.result.value)
                            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMQuestionsVC") as!TMQuestionsVC
                            //vc.navigationReference = self.navigationController
                            AppState.answerForDisplay.removeAll()
                            vc.isRegisterScreen = true
                            self.present(vc, animated: true, completion: nil)
                        }
                    }else{
                        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMQuestionsVC") as!TMQuestionsVC
                        //vc.navigationReference = self.navigationController
                        AppState.answerForDisplay.removeAll()
                        vc.isRegisterScreen = true
                        self.navigationController?.pushViewController(vc, animated: true)
                        //   self.present(vc, animated: true, completion: nil)
                        
                    }
                    
                    
                    // self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    if isFromQuestionairre{
                        Alamofire.request("http://taskmario.dev.webcastle.in/api/submitTask", method: .post, parameters: ["userid":AppState.userData!.userid, "location":"\(AppState.selectedLocation)", "latitude":"\(AppState.latitude)", "longitude":"\(AppState.longitude)", "category_id":AppState.selectedSubCategory, "feedback": "\(AppState.feedback)"], encoding: URLEncoding.default, headers: nil).authenticate(user: "dev", password: "dev").responseJSON { (response) in
                            print(AppState.chosenAnswers)
                            print(response.result)
                            print(response.result.value)
                            // let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMAllTasksController") as! TMAllTasksController
                            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMGoodJobController") as! TMGoodJobController
                            self.present(vc, animated: true, completion: nil)
                        }
                    }else{
                        
                        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMHomeScreenController") as! TMHomeScreenController
                        self.navigationController?.pushViewController(vc, animated: true)
                        
                    }
                    
                    
                }
                
            }else{
                Banner.showBannerOn(VC: self, title: "Invalid OTP", message: "", style: .warning)
                print("Failed")
            }
        }
        
        return true
    }
    
    
}
