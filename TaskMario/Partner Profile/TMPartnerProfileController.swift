//
//  TMPartnerProfileController.swift
//  TaskMario
//
//  Created by Appzoc on 08/02/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import Kingfisher
import Cosmos
import DropDown
import ImageViewer
import ReadMoreTextView
import ExpandableLabel



class TMPartnerProfileController: UIViewController, JSONParserDelegate, ExpandableLabelDelegate {
    
    
    @IBOutlet weak var scrollContentWrapper: UIView!
    
    @IBOutlet weak var partnerImage: StandardImageView!
    
    @IBOutlet weak var partnerName: UILabel!
    
    @IBOutlet weak var partnerLocation: UILabel!
    
    @IBOutlet weak var partnerRating: UILabel!
    
    @IBOutlet weak var yearsOfExperience: UILabel!
    
    @IBOutlet weak var experienceType: UILabel!
    
    @IBOutlet weak var tasksCompleted: UILabel!
    
    
    @IBOutlet weak var partnerDescription: ReadMoreTextView!
    
    @IBOutlet weak var numberOfReviews: UILabel!
    
    @IBOutlet weak var partnerRatingLarge: UILabel!
    
    @IBOutlet weak var scrollRef: UIScrollView!
    
    
    @IBOutlet weak var arrowReversalRef: UIImageView!
    
    
    var partnerID:Int = 118
    
    var portfolioImages:[String] = []
    
    var clientReviews:[ClientReviews] = []
    
    var imageViewerSource:[UIImage] = []
    
    @IBOutlet weak var tableRef: UITableView!
    
    @IBOutlet weak var collectionRef: UICollectionView!
    
    
    @IBOutlet weak var mainRating: CosmosView!
    
    
    @IBOutlet weak var excellentRating: CosmosView!
    
    @IBOutlet weak var goodRating: CosmosView!
    @IBOutlet weak var averageRating: CosmosView!
    @IBOutlet weak var belowAverageRating: CosmosView!
    @IBOutlet weak var badRating: CosmosView!
    
    
    @IBOutlet weak var partnerNameOnTabar: UILabel!
    
    
    @IBOutlet weak var subCategoryName: UILabel!
    
    @IBOutlet weak var docTableRef: UITableView!
    
    
    @IBOutlet weak var docHeightView: NSLayoutConstraint!
    
    @IBOutlet weak var tableHeightRef: NSLayoutConstraint!
    
    @IBOutlet weak var docTableHeightRef: NSLayoutConstraint!
    
    
    @IBOutlet weak var starRatingVIew: CosmosView!
    
    @IBOutlet weak var dropDownAnchor: UIView!
    
    var partnerJob:String = ""
    
    let activityView = ActivityIndicatorAlter()
    var dropDownDataSource:[String] = []
    let dropDown = DropDown()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //1083 - 356 + 570 = 1297
        //1083 - 356 = 727
        //scrollRef.contentSize = CGSize(width: UIScreen.main.bounds.width, height: 1527)
        //scrollRef.setContentOffset(CGPoint(x: 0, y: 500), animated: true)
        
        
        setUpPortfolioCollectionView()
        setUpWebCall()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        activityView.start()
        setUpWebCall()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tableRef.reloadData()
    }
    
    func setupExpandableLabels(){
        partnerDescription.shouldTrim = true
        partnerDescription.maximumNumberOfLines = 3
        partnerDescription.attributedReadMoreText = NSAttributedString(string: "...More", attributes: [.foregroundColor:AppState.blueColor])
        partnerDescription.attributedReadLessText = NSAttributedString(string: "...Less", attributes: [.foregroundColor:UIColor.red])
    }
    
    func willExpandLabel(_ label: ExpandableLabel) {
        tableRef.beginUpdates()
    }
    
    func didExpandLabel(_ label: ExpandableLabel) {
        let point = label.convert(CGPoint.zero, to: tableRef)
        if let indexPath = tableRef.indexPathForRow(at: point) as IndexPath? {
            // states[indexPath.row] = false
            //            let cell = tableRef.cellForRow(at: indexPath) as! TableViewCell
            //            cell.expandableLabel.collapsed = false
            DispatchQueue.main.async { [weak self] in
                // self?.tableRef.reloadRows(at: [indexPath], with: .none)
                self?.tableHeightRef.constant = (self?.tableRef.contentSize.height)!
                self?.tableRef.scrollToRow(at: indexPath, at: .top, animated: true)
            }
        }
        tableRef.endUpdates()
    }
    
    func willCollapseLabel(_ label: ExpandableLabel) {
        tableRef.beginUpdates()
    }
    
    func didCollapseLabel(_ label: ExpandableLabel) {
        let point = label.convert(CGPoint.zero, to: tableRef)
        if let indexPath = tableRef.indexPathForRow(at: point) as IndexPath? {
            //states[indexPath.row] = true
            //            let cell = tableRef.cellForRow(at: indexPath) as! TableViewCell
            //            cell.expandableLabel.collapsed = false
            DispatchQueue.main.async { [weak self] in
                //self?.tableRef.reloadRows(at: [indexPath], with: .none)
                self?.tableHeightRef.constant = self!.tableRef.contentSize.height
                self?.tableRef.scrollToRow(at: indexPath, at: .top, animated: true)
            }
        }
        tableRef.endUpdates()
    }
    
/*
    let width = UIScreen.main.bounds.width
    layout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
    layout.itemSize = CGSize(width: width / 2, height: width / 2)
    layout.minimumInteritemSpacing = 0
    layout.minimumLineSpacing = 0
    collectionView!.collectionViewLayout = layout
*/
    
    func setUpPortfolioCollectionView(){
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: 68, height: 73)
        layout.minimumInteritemSpacing = 1
        layout.minimumLineSpacing = 1
        collectionRef.collectionViewLayout = layout
    }
    
    func setUpWebCall(){
        //partnerID - to test - 118
        let jsonParser = JSONParser(url: "http://taskmario.dev.webcastle.in/api/viewpartnerprofile", httpMethod: .post, parameters: ["partnerid":partnerID], delegate: self, into: .PartnerProfile)
        jsonParser.callWebService()
    }
    
    func operateOnJSONResult(model: Any) {
        if let dataModel = model as? APIPartnerProfile{
            print(dataModel.partner_name)
            self.partnerNameOnTabar.text = dataModel.partner_name
            self.partnerName.text = dataModel.partner_name
            self.subCategoryName.text = self.partnerJob
            self.partnerRating.text = "\(dataModel.rating)"
            self.starRatingVIew.rating = Double(dataModel.rating)
            self.partnerImage.kf.setImage(with: URL(string: "\(dataModel.partner_image)")!, placeholder: UIImage(named: "painting-paint-roller-"), options: [.requestModifier(AppState.modifier)])
            self.partnerLocation.text = dataModel.partner_location
            self.yearsOfExperience.text = "\(dataModel.yearsOfexperiance)"
            self.experienceType.text = "\(dataModel.yearsOfexperianceType) of \nExperience"
            self.tasksCompleted.text = "\(dataModel.tasksCompleted)"
            self.partnerRatingLarge.text = "\(dataModel.rating)"
            self.numberOfReviews.text = "Based on \(dataModel.no_ofreviews) reviews"
            self.partnerDescription.text = dataModel.partner_description
            self.setupExpandableLabels()
            self.mainRating.rating = Double(dataModel.rating)
            self.portfolioImages = dataModel.portfolio
            
            setImageViewerSource()
            
            self.clientReviews = dataModel.Clientreviews
            setReviewTableHeight(rows: dataModel.Clientreviews.count)
            var tempSource:[String] = []
            for item in dataModel.verifiedcertificates{
                let parsed = item.components(separatedBy: "http://taskmario.dev.webcastle.in/storage/app/servdocuments/")[1]
                tempSource.append(parsed)
            }
            
            self.dropDownDataSource = tempSource
            self.tableRef.reloadData()
            self.docTableRef.reloadData()
            self.collectionRef.reloadData()
            self.excellentRating.rating = Double(dataModel.AllRatings["Excellent"]!)
            self.goodRating.rating = Double(dataModel.AllRatings["Good"]!)
            self.averageRating.rating = Double(dataModel.AllRatings["Satisfactory"]!)
            self.belowAverageRating.rating = Double(dataModel.AllRatings["Below Average"]!)
            self.badRating.rating = Double(dataModel.AllRatings["Poor"]!)
            /*
             Excellent": 0,
             "Good": 1,
             "Satisfactory": 0,
             "Below Average": 2,
             "Poor"
             */
            setUpDropDown()
            activityView.stop()
            
        }
    }
    
    func didReturnWithError(error: Error) {
        activityView.stop()
        Banner.showBannerOn(VC: self, title: "Network Error", message: "", style: .warning)
        print(error)
    }
    
    func setReviewTableHeight(rows:Int){
        DispatchQueue.main.async {
            //self.tableRef.frame = CGRect(x: self.tableRef.frame.minX, y: self.tableRef.frame.minY, width: self.tableRef.frame.width, height: self.tableRef.contentSize.height)
            //self.tableRef.reloadData()
            self.tableHeightRef.constant = self.tableRef.contentSize.height
        }
    }
    
    func setImageViewerSource(){
        //completion: @escaping([UIImageView]) -> ()
        //var imageViewArray:[UIImageView] = []
        for item in portfolioImages{
            var imageViewTemp = UIImageView()
            //  imageViewTemp.kf.setImage(with: URL(string: "\(item)")!, placeholder: UIImage(named: "painting-paint-roller-"), options: [.requestModifier(AppState.modifier)])
            imageViewTemp.kf.setImage(with: URL(string: "\(item)")!, placeholder: nil, options: [.requestModifier(AppState.modifier)], progressBlock: nil, completionHandler: { (image, err, cacheType, url) in
                self.imageViewerSource.append(image!)
            })
            
        }
        
        
    }
    
    //MARK: Report Partner
    @IBAction func reportPersonBTN(_ sender: UIButton) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMReportPartnerController") as! TMReportPartnerController
        vc.partnerID = self.partnerID
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func backBTN(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func dropDownBTN(_ sender: UIButton) {
        if docHeightView.constant == 80{
            docTableHeightRef.constant = docTableRef.contentSize.height
            //docHeightView.constant = 55
            docHeightView.constant += docTableHeightRef.constant
            arrowReversalRef.image = UIImage(named: "up-arrow")
        }else{
            docHeightView.constant = 80
            arrowReversalRef.image = UIImage(named: "arrow-down-sign-to-navigate")
        }
        
    }
    
    func setUpDropDown(){
        dropDown.anchorView = dropDownAnchor
        dropDown.direction = .bottom
        dropDown.dataSource = dropDownDataSource
        dropDown.selectionAction = {
            index, item in
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMPdfViewController") as! TMPdfViewController
            vc.loaderURL = "http://taskmario.dev.webcastle.in/storage/app/servdocuments/\(item)"
            self.navigationController?.pushViewController(vc, animated: true)
            // print(item)
        }
        dropDown.cancelAction = {
            self.arrowReversalRef.image = UIImage(named: "arrow-down-sign-to-navigate")
        }
    }
    
    
    //TabBarControls
    
    @IBAction func homeScreenBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadHomeTab(navigationController: self.navigationController!)
    }
    @IBAction func newTaskBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadNewTaskTab(navigationController: self.navigationController!)
    }
    @IBAction func tasksBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadAllTasksTab(navigationController: self.navigationController!)
    }
    
    @IBAction func chatBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadChatTab(navigationController: self.navigationController!)
    }
    
    @IBAction func profileScreenBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadProfileTab(navigationController: self.navigationController!)
    }
    
    
}

extension TMPartnerProfileController:GalleryItemsDataSource {
    func itemCount() -> Int {
        return portfolioImages.count
    }
    
    func provideGalleryItem(_ index: Int) -> GalleryItem {
        // var imageView = UIImageView()
        // imageView.kf.setImage(with: URL(string: "\(clientReviews[index].client_image)")!, placeholder: UIImage(named: "painting-paint-roller-"), options: [.requestModifier(AppState.modifier)])
        let galleryItem = GalleryItem.image{ $0(self.imageViewerSource[index]) }
        return galleryItem
    }
    /*
     let image = imageView.image ?? UIImage(named: "0")!
     galleryItem = GalleryItem.image { $0(image) }
     */
    
    
}

extension TMPartnerProfileController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableRef{
            if clientReviews.count < 3{
                return clientReviews.count
            }else{
                return 3
            }
        }
        //if tableView == docTableRef{
        return self.dropDownDataSource.count
        // }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tableRef{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TMReviewsReceivedTableCell") as! TMReviewsReceivedTableCell
            cell.reviewerName.text = clientReviews[indexPath.row].client_name
            cell.reviewerLocation.text = clientReviews[indexPath.row].client_location
            cell.reviewRating.rating = Double(clientReviews[indexPath.row].client_rating)
            
            cell.reviewComment.delegate = self
            cell.reviewComment.shouldCollapse = true
            cell.reviewComment.collapsed = true
            cell.reviewComment.ellipsis = NSAttributedString(string: "...")
            cell.reviewComment.textReplacementType = .character
            cell.reviewComment.collapsedAttributedLink = NSAttributedString(string: "More", attributes: [.foregroundColor:UIColor.blue])
            cell.reviewComment.setLessLinkWith(lessLink: "Less", attributes: [.foregroundColor:UIColor.red], position: nil)
            cell.reviewComment.text = clientReviews[indexPath.row].client_comment
            cell.reviewImage.kf.setImage(with: URL(string: "\(clientReviews[indexPath.row].client_image)")!, placeholder: UIImage(named: "painting-paint-roller-"), options: [.requestModifier(AppState.modifier)])
            return cell
        }
        //if tableView == docTableRef{
        let cell = tableView.dequeueReusableCell(withIdentifier: "TMExpandDocsCell") as! TMExpandDocsCell
        cell.docLabel.text = dropDownDataSource[indexPath.row]
        if dropDownDataSource[indexPath.row].suffix(3).lowercased().contains("pdf"){
            cell.docImage.image = UIImage(named: "document")
        }else{
            cell.docImage.image = UIImage(named: "image-desc")
        }
        return cell
        // }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tableRef{
            return UITableViewAutomaticDimension
        }else{
            return 47
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == docTableRef{
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMPdfViewController") as! TMPdfViewController
            vc.loaderURL = "http://taskmario.dev.webcastle.in/storage/app/servdocuments/\(dropDownDataSource[indexPath.row])"
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension TMPartnerProfileController:UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return portfolioImages.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // if collectionView.tag == 0{
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TMPartnerPortfolioCell", for: indexPath) as! TMPartnerPortfolioCell
        
        switch indexPath.row{
        case 0,1,2:
            cell.portfolioImage.kf.setImage(with: URL(string: "\(portfolioImages[indexPath.row])")!, placeholder: UIImage(named: "painting-paint-roller-"), options: [.requestModifier(AppState.modifier)])
            cell.portfolioImage.backgroundColor = UIColor.clear
            cell.extraImagesCount.isHidden = true
        default:
            if portfolioImages.count-3 > 0{
                cell.portfolioImage.image = UIImage()
                cell.portfolioImage.backgroundColor = AppState.blueColor
                cell.extraImagesCount.text = "+\(portfolioImages.count-3)"
                cell.extraImagesCount.isHidden = false
            }else{
                cell.portfolioImage.backgroundColor = UIColor.clear
                cell.extraImagesCount.isHidden = true
            }
        }
//        if indexPath.row == 3{
//            cell.extraImagesCount.isHidden = false
//        }
        return cell
        //  }
        //        else{
        //            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TMClientReviewCell", for: indexPath) as! TMClientReviewCell
        //            cell.reviewerImage.backgroundColor = UIColor.red
        //            return cell
        //        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // let cell = collectionView.cellForItem(at: indexPath) as! TMPartnerPortfolioCell
        self.presentImageGallery(GalleryViewController(startIndex: indexPath.row, itemsDataSource: self, configuration: [ .deleteButtonMode(.none)]))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let height = self.view.frame.height
        let width = self.view.frame.width
        let size = CGSize(width: width * 0.181, height: height * 0.109)
        return size
    }
    
    
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        let layout = UICollectionViewFlowLayout()
//        layout.minimumInteritemSpacing = 10
//        layout.minimumLineSpacing = 10
//        collectionRef.collectionViewLayout = layout
//        return 10
//    }
    
    
    
}

