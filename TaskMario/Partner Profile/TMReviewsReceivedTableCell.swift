//
//  TMReviewsReceivedTableCell.swift
//  TaskMario
//
//  Created by Appzoc on 28/02/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import Cosmos
import ExpandableLabel

class TMReviewsReceivedTableCell: UITableViewCell {

    
    @IBOutlet weak var reviewImage: UIImageView!
    
    @IBOutlet weak var reviewerName: UILabel!
    
    @IBOutlet weak var reviewerLocation: UILabel!
    
    @IBOutlet weak var reviewRating: CosmosView!
    
    
    @IBOutlet weak var reviewComment: ExpandableLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
