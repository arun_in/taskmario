//
//  TMClientReviewCell.swift
//  TaskMario
//
//  Created by Appzoc on 09/02/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import Cosmos

class TMClientReviewCell: UICollectionViewCell {
    
    @IBOutlet weak var reviewerImage: UIImageView!
    
    @IBOutlet weak var reviewerName: UILabel!
    
    @IBOutlet weak var reviewerLocation: UILabel!
    
    @IBOutlet weak var reviewerRating: CosmosView!
    
    @IBOutlet weak var reviewComment: UILabel!
    
    
    
    
    
    
    
}
