//
//  TMExpandDocsCell.swift
//  TaskMario
//
//  Created by Appzoc on 25/04/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class TMExpandDocsCell: UITableViewCell {
    
    
    @IBOutlet weak var docLabel: UILabel!
    
    @IBOutlet weak var docImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
