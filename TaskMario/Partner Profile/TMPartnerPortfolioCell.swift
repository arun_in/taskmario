//
//  TMPartnerPortfolioCell.swift
//  TaskMario
//
//  Created by Appzoc on 09/02/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class TMPartnerPortfolioCell: UICollectionViewCell {
    
    
    @IBOutlet weak var portfolioImage: UIImageView!
    
    @IBOutlet weak var extraImagesCount: UILabel!
}
