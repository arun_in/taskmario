//
//  TMOnboardingController.swift
//  TaskMario
//
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class TMOnboardingController: UIViewController
{
    
    @IBOutlet weak var collectionRef: UICollectionView!
    
    var imageSource:[UIImage] = [UIImage(named: "Onboarding1")!, UIImage(named: "Onboarding2")!, UIImage(named: "Onboarding3")!, UIImage(named: "Onboarding4")!, UIImage(named: "Onboarding5")!]
    var titleSource:[String] = ["Share what you need","Get the best quotes","Sit back and relax","Make the payment","Mark a review"]
    var descriptionSource:[String] = ["Tell us your requirement & what service you want by answering a few simple questions.",
                                      "You will receive up to 5 quotes within 24 Hours from nearby professionals. Compare the quotes, view the service provider’s profile & hire the best professional.",
                                      "Sit back and focus on your little things. Let TASKMARIO Partners complete your task.",
                                      "Choose our online or offline flexible payment options for TASKMARIO Partners Payment","In order to provide quality assurance, rate TASKMARIO Partner & Write a genuine review. This will help next customer to choose the right professional."]
    
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var progressRef: UIPageControl!
    
    var showGetStarted = false
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        
        let sizeW = collectionRef.frame.width
        let sizeH = collectionRef.frame.height
        print("viewDidAppear",sizeH,"Width",sizeW)
    }
    
    @IBAction func getStartAction(_ sender: UIButton)
    {
        UserDefaults.standard.set("false", forKey: "firstTime")
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMHomeScreenController") as! TMHomeScreenController
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func skipAction(_ sender: UIButton)
    {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMHomeScreenController") as! TMHomeScreenController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func nextAction(_ sender: UIButton)
    {
        let currentIndex = collectionRef.indexPathsForVisibleItems.first
        print("pathsss",collectionRef.indexPathsForVisibleItems)
        if let cIndex = currentIndex
        {
            
            if cIndex.row == 3
            {
                bottomView.isHidden = true
                showGetStarted = true
            }

            let nextIndex = IndexPath(item: cIndex.row+1, section: cIndex.section)
            progressRef.currentPage = nextIndex.row
            collectionRef.scrollToItem(at: nextIndex, at: .centeredHorizontally, animated: true)
        }
    }
    

}

extension TMOnboardingController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return titleSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TMOnboardingCollectionCell", for: indexPath) as! TMOnboardingCollectionCell
        print("Index Path",indexPath)
        
        if indexPath.item == 4
        {
            cell.getStartBTNRef.isHidden = false
            cell.getStartViewRef.isHidden = false
            cell.image.image = imageSource[indexPath.item]
            cell.titleLabel.text = titleSource[indexPath.item]
            cell.descriptionLabel.text = descriptionSource[indexPath.item]
        }
        else
        {
            cell.getStartBTNRef.isHidden = true
            cell.getStartViewRef.isHidden = true
            cell.image.image = imageSource[indexPath.item]
            cell.titleLabel.text = titleSource[indexPath.item]
            cell.descriptionLabel.text = descriptionSource[indexPath.item]
            
        }
        
        if  showGetStarted
        {
            bottomView.isHidden = true
        }
        else
        {
            bottomView.isHidden = false
        }
        
        
        return cell
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        let currentIndex = collectionRef.indexPathsForVisibleItems.first
        if let cIndex = currentIndex{
            if cIndex.row == 4
            {
                let cell = collectionRef.cellForItem(at: cIndex) as! TMOnboardingCollectionCell
                cell.getStartBTNRef.isHidden = false
                cell.getStartViewRef.isHidden = false
                bottomView.isHidden = true
                showGetStarted = true
            }
            else
            {
                showGetStarted = false
            }
            progressRef.currentPage = cIndex.row
        }
        collectionRef.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        
        
        let sizeW = collectionView.frame.width
        let sizeH = collectionView.frame.height
        print("Height",sizeH,"Width",sizeW)
        return CGSize(width: sizeW, height: sizeH)
        
        //        }
        
    }
    
    
}
