//
//  TMOnboardingCollectionCell.swift
//  TaskMario
//
//  Created by Arun Induchoodan on 19/04/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class TMOnboardingCollectionCell: UICollectionViewCell {
    
    
    
    @IBOutlet weak var getStartBTNRef: UIButton!
    
    @IBOutlet weak var getStartViewRef: StandardView!
    
    @IBOutlet weak var image: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    
}
