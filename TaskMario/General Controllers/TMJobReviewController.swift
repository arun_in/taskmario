//
//  TMJobReviewController.swift
//  TaskMario
//
//  Created by Arun Induchoodan on 08/04/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import Cosmos
import Alamofire

class TMJobReviewController: UIViewController, UITextViewDelegate {
    
    
    @IBOutlet weak var partnerImage: StandardImageView!
    
    @IBOutlet weak var partnerName: UILabel!
    @IBOutlet weak var taskName: UILabel!
    @IBOutlet weak var location: UILabel!
    
    @IBOutlet weak var partnerRating: CosmosView!
    @IBOutlet weak var reviewText: UITextView!
    

    var activityView = ActivityIndicatorAlter()
    var syncWithWeb:SyncTasks?
    
    var partnerDetails:APITaskList?
    
   //  userid
    var provider_id:Int = 0
     
    var review_comment:String = ""

    var request_id:Int = 0

    var review_question_1:Int = 0
 
    var review_question_2:Int = 0
    
    var review_question_3:Int = 0
    
    var review_question_4:Int = 0
    
    var review_question_5:Int = 0
    
    var starRating:Double {
        return Double(review_question_1 + review_question_2 + review_question_3 + review_question_4 + review_question_5)
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        reviewText.delegate = self
        //reviewText.layer.borderWidth = 1
       // reviewText.layer.borderColor = UIColor.gray.cgColor
        // Do any additional setup after loading the view.
        setUpReviewPage()
    }
    
    func setUpReviewPage(){
        if let partnerData = partnerDetails{
            partnerName.text = partnerData.respondersList[0].partnerName
            taskName.text = partnerData.subcat_name
            location.text = "\(partnerData.respondersList[0].partnerlocation) \(partnerData.respondersList[0].partnerdistance) Km Away"
            partnerImage.kf.setImage(with: URL(string: "\(partnerData.respondersList[0].partnerimage)")!, placeholder: UIImage(named: "dpPlaceHolder"), options: [.requestModifier(AppState.modifier)])
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.text = ""
    }
    
    
    @IBAction func closeBTN(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func ratingSelect(_ sender: StandardButton) {
        
        if sender.image(for: .normal) == UIImage(named: "reviewCheck"){
            sender.setImage(UIImage(), for: .normal)
            sender.borderColor = .gray
            sender.borderWidth = 1
        }else{
            sender.setImage(UIImage(named: "reviewCheck"), for: .normal)
            sender.borderWidth = 0
        }
        
        switch sender.tag {
        case 1:
            if review_question_1 == 1 {
                review_question_1 = 0
            }else{
                review_question_1 = 1
            }
        case 2:
            if review_question_2 == 1 {
                review_question_2 = 0
            }else{
                review_question_2 = 1
            }
        case 3:
            if review_question_3 == 1 {
                review_question_3 = 0
            }else{
                review_question_3 = 1
            }
        case 4:
            if review_question_4 == 1 {
                review_question_4 = 0
            }else{
                review_question_4 = 1
            }
        case 5:
            if review_question_5 == 1 {
                review_question_5 = 0
            }else{
                review_question_5 = 1
            }
            
        default:
            review_question_1 = 1
        }
        
        partnerRating.rating = starRating
    }
    
    
    

    @IBAction func submitReview(_ sender: StandardButton) {
        if !reviewText.text.isEmpty{
            let param = [
                "userid":AppState.userData!.userid,
                "provider_id":provider_id,
                "review_comment":reviewText.text!,
                "request_id":request_id, //TaskID
                "review_question_1":review_question_1,
                "review_question_2":review_question_2,
                "review_question_3":review_question_3,
                "review_question_4":review_question_4,
                "review_question_5":review_question_5
                ] as [String : Any]
            activityView.start()
            Alamofire.request("http://taskmario.dev.webcastle.in/api/closeTask", method: .post, parameters:param, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
                print(response.result)
                print(response.result.value)
                switch response.result{
                case .success(let responseData):
                    self.syncWithWeb?.syncWithWeb()
                    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMThankYouController") as! TMThankYouController
                    vc.navigation = self.navigationController
                    self.present(vc, animated: true, completion: {
                        self.activityView.stop()
                    })
                    
                case .failure(let err):
                    self.activityView.stop()
                    Banner.showBannerOn(VC: self, message: "Network Error")
                }
               
                //self.navigationController?.popViewController(animated: true)
            }
        }else{
            Banner.showBannerOn(VC: self, message: "Please Enter Your Review")
        }
    }
    
}

protocol SyncTasks {
    func syncWithWeb()
}
