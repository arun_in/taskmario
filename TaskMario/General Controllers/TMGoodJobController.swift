//
//  TMGoodJobController.swift
//  TaskMario
//
//  Created by Appzoc on 18/04/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class TMGoodJobController: UIViewController {
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }

    @IBAction func okBTN(_ sender: StandardButton) {
        self.dismiss(animated: true) {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMAllTasksController") as! TMAllTasksController
            AppState.navigation?.pushViewController(vc, animated: true)
        }
        
    }
    
}


