//
//  TMPdfViewController.swift
//  TaskMario
//
//  Created by Arun Induchoodan on 08/04/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import WebKit

class TMPdfViewController: UIViewController {
    
    var loaderURL:String = ""
    
    @IBOutlet weak var webView: UIWebView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let username = "dev"
        let password = "dev"
        let loginString = String(format: "%@:%@", username, password)
        let loginData = loginString.data(using: String.Encoding.utf8)!
        let base64LoginString = loginData.base64EncodedString()
        
        if let targetURL = URL(string: "\(loaderURL)") {
            var request = URLRequest(url: targetURL)
            request.httpMethod = "POST"
            request.setValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")
            webView.loadRequest(request)
        }
    }


    @IBAction func closeBTN(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
