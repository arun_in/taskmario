//
//  TMHirePartnerController.swift
//  TaskMario
//
//  Created by Appzoc on 09/04/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import Alamofire

class TMHirePartnerController: UIViewController {
    
    var accessNav:AccessNavigation?
    
    //var hiringResponder:Responders?
    
    //var chatPartnerDetails:APIChatList?
    
    var partnerID:Int = 0
    
    var taskID:Int = 0
    
    var syncWithWeb:SyncTasks?
    
    var activityView = ActivityIndicatorAlter()
    
    var chatRoomData:ChatRoomData?
    
    var taskName:String = ""
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var mainLabel: UILabel!
    
    @IBOutlet weak var subLabel: UILabel!
    
    @IBOutlet weak var heightContraint: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageView.isHidden = true
        
        subLabel.text = "For \(taskName) task are you confirm this?"
        
        if let chatData = self.chatRoomData{
            partnerID = chatData.partnerID
            taskID = chatData.taskID
        }else{
            print("Failed, No Data Available")
        }
        
        // Do any additional setup after loading the view.
    }

    
    @IBAction func closePopup(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func confirmHire(_ sender: StandardButton) {
        if sender.titleLabel?.text == "CONFIRM"{
            activityView.start()
            Alamofire.request("http://taskmario.dev.webcastle.in/api/hirePartner", method: .post, parameters: ["userid":AppState.userData!.userid, "partnerid":partnerID, "taskid":taskID], encoding: URLEncoding.default, headers: nil).authenticate(user: "dev", password: "dev").responseJSON { (response) in
                print(response.result)
                print(response.result.value)
                switch response.result{
                case .success(let responseData):
                    self.syncWithWeb?.syncWithWeb()
                    self.imageView.isHidden = false
                    self.mainLabel.text = "Thank You"
                    self.heightContraint.constant = 300
                    sender.setTitle("OK", for: [])
                    self.activityView.stop()
                case .failure(let err):
                    Banner.showBannerOn(VC: self, message: "Network Error")
                    self.activityView.stop()
                }
                
                
            }
        }else if sender.titleLabel?.text == "OK"{
            DispatchQueue.main.async {
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMAllTasksController") as! TMAllTasksController
                vc.hiredFromChat = true
                self.dismiss(animated: true, completion: nil)
                self.dismiss(animated: true, completion: {
                    if let _ = self.accessNav{
                        self.accessNav?.accessNavigation(viewCon: vc)
                    }else{
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                })

            }
        }
    }
    
}
