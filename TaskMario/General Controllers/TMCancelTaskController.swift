//
//  TMCancelTaskController.swift
//  TaskMario
//
//  Created by Appzoc on 11/04/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import Alamofire

class TMCancelTaskController: UIViewController {

    var dataForCell: APITaskList?
    
    var syncWithWeb:SyncTasks?
    
    var activityView = ActivityIndicatorAlter()
    
    @IBOutlet weak var reasonTextBox: UITextView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        reasonTextBox.layer.borderColor = UIColor.lightGray.cgColor
        reasonTextBox.layer.cornerRadius = 7
        reasonTextBox.layer.borderWidth = 1
        
    }
    
    
    
    @IBAction func closeController(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func confirmCancel(_ sender: StandardButton) {
        
        if let text = reasonTextBox.text, let taskID = dataForCell?.taskid {
            if !text.isEmpty{
                activityView.start()
                let param = ["userid":AppState.userData!.userid,"reason":text,"taskid":taskID] as [String : Any]
                Alamofire.request("http://taskmario.dev.webcastle.in/api/cancelTask", method: .post, parameters:param, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
                    print(response.result)
                    print(response.result.value)
                    self.syncWithWeb?.syncWithWeb()
                    self.dismiss(animated: true, completion: {
                        self.activityView.stop()
                    })
                }
            }else{
                Banner.showBannerOn(VC: self, message: "Enter reason for cancelling")
            }
        }
    }
    

}


