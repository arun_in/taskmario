//
//  TMThankYouController.swift
//  TaskMario
//
//  Created by Appzoc on 16/04/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class TMThankYouController: UIViewController {

    var navigation:UINavigationController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }

    
    @IBAction func okBTN(_ sender: StandardButton) {
        self.dismiss(animated: true) {
           // let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMAllTasksController") as! TMAllTasksController
            //self.navigation?.popViewController(animated: true)
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMClosedTasksController") as! TMClosedTasksController
            self.navigation?.pushViewController(vc, animated: true)
            
        }
    }
    
   
}
