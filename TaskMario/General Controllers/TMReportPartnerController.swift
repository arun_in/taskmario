//
//  TMReportPartnerController.swift
//  TaskMario
//
//  Created by Appzoc on 17/04/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import Alamofire

class TMReportPartnerController: UIViewController {
    
    let activityView = ActivityIndicatorAlter()
    
    @IBOutlet weak var comment: UITextView!
    
    var partnerID:Int = 0
    var userID = AppState.userData!.userid
    
    override func viewDidLoad() {
        super.viewDidLoad()
        comment.layer.borderWidth = 1
        comment.layer.borderColor = UIColor.gray.cgColor
        comment.layer.cornerRadius = 10
    }

    
    @IBAction func proceedBTN(_ sender: StandardButton) {
        
        if BaseValidator.isNotEmpty(string: comment.text){
            activityView.start()
            
            Alamofire.request("http://taskmario.dev.webcastle.in/api/reportProvider", method: .post, parameters: ["userid":userID, "partnerid": partnerID, "comment": comment.text], encoding: URLEncoding.default, headers: nil).authenticate(user: "dev", password: "dev").responseJSON { (response) in
                
                switch response.result {
                case .success(_):
                    Banner.showBannerOn(VC: self, message: "Success")
                case .failure(_):
                    Banner.showBannerOn(VC: self, message: "Network error")
                }
                DispatchQueue.main.async {
                    self.activityView.stop()
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }else{
            Banner.showBannerOn(VC: self, message: "Enter reason")
        }
    }
    
    
    @IBAction func closeControllerBTN(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
