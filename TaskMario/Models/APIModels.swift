//
//  APIModels.swift
//  TaskMario
//
//  Created by Appzoc on 02/03/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import Foundation
import Alamofire


//MARK: Preloading

struct ServiceChangeCheck:Codable {
    let categoryChanged:Bool
    let subcategoryChanged:Bool
}

/*
 {
 
 "ErrorCode": 0,
 "Data": {
 "categoryChanged": true,
 "subcategoryChanged": true
 
 },
 "Message": "Success"
 
 
 }
 */



//MARK: Normal Login - Check Datatype
struct NormalLogin: Codable {
    let otp:String
    let userid:String
}

struct FacebookLogin:Codable {
    let userid:String
}

/*
 {
 "ErrorCode": 0,
 "Data":  "result": [
 {
 "taskid": "1",
 "taskStatus":"ChatNotStarted",
 "newresponse":true,
 "subcat_id": "1",
 "subcat_name": "cinematography","taskLocation":"Delhi,India",
 "subcat_image": "http://dev.mt.webcastle.in/jadoo/uploads/biodata/thumbs/JDP48INKL/5881b0b0d48e3.jpeg",
 "taskdate": timestamp,
 "taskDescription": "AC Repair",
 "totalresponse": 5,
 "response_received": 1,
 "Chats": [{"partner_id":"1","partner_name":"Megan","partner_image":image_url,"unreadmsg":true,"Lastseen":"2 min ago"},{"partner_id":"1","partner_name":"Megan","partner_image":image_url,"unreadmsg":true,"Lastseen":"2 min ago"}],
 "HiredPartnerDetails": {"partner_id":"1","partner_name":"Megan","partner_image":image_url,"unreadmsg":true,"Lastseen":"2 min ago"},
 
 },
 {
 "taskid": "2",
 "taskStatus":"ChatNotStarted",
 "subcat_id": "1",
 "newresponse":true,
 "subcat_name": "cinematography","taskLocation":"Delhi,India",
 "subcat_image": "http://dev.mt.webcastle.in/jadoo/uploads/biodata/thumbs/JDP48INKL/5881b0b0d48e3.jpeg",
 "taskdate": timestamp,
 "taskDescription": "Car wash",
 "totalresponse": 5,
 "response_received": 1,
 "Chats": [{"partner_id":"1","partner_name":"Megan","partner_image":image_url,"unreadmsg":true,"Lastseen":"2 min ago"},{"partner_id":"1","partner_name":"Megan","partner_image":image_url,"unreadmsg":true,"Lastseen":"2 min ago"}],
 "HiredPartnerDetails": {"partner_id":"1","partner_name":"Megan","partner_image":image_url,"unreadmsg":true,"Lastseen":"2 min ago"},
 
 }
 ],
 
 "Message": "Success"
 }
 */

//MARK: Questionairre API

struct QuestionsAPI:Codable {
    let id:Int
    let question:String
    let input_type:String
    let conditional:Int
    let conditional_value:String
    let category_id:Int
    let reorder:Int
    let has_condition:Int
    let answers:[Answers]
    let subquestions:[SubQuestions]
}
/*
 "id":59,
 "question_id":30,
 "label":"Repair",
 "value":"Repair",
 "status":1,
 "created_at":"2018-02-26 05:48:26",
 "updated_at":"2018-02-26 05:48:26"
 */
struct Answers:Codable {
    let id:Int
    let question_id:Int
    let label:String
    let value:String
    let status:Int
    let created_at:String
    let updated_at:String
}

/*
 "id":237,
 "question":"What type of damage are you facing",
 "input_type":"radio",
 "conditional":30,
 "conditional_value":"Service",
 "category_id":20,
 "reorder":2,
 "status":1,
 "created_at":"2018-03-02 08:49:19",
 "updated_at":"2018-03-02 08:51:19",
 "has_other":0,
 "has_condition":1,
 "answers":
 */

struct SubQuestions:Codable {
    let id:Int
    let question:String
    let input_type:String
    let conditional:Int
    let conditional_value:String
    let category_id:Int
    let reorder:Int
   // let status:Int
   // let created_at:String
   // let updated_at:String
   // let has_other:Int
    let has_condition:Int
    let answers:[Answers]
}

//MARK: HomeScreen API

struct APIHomePage:Codable{
    let Categorymain:[Categorymain]
    let CategoryList:[CategoryList]
    let services_count:Int
}

struct Categorymain:Codable{
    let catid:Int
    let categoryname:String
    let categoryimage:String
}

struct CategoryList:Codable {
    let catid:Int
    let catname:String
    let Subcategories:[Subcategories]
}

struct Subcategories:Codable {
    let subcatid:Int
    let subcat_name:String
    let subcatimage:String
    let subcaticon:String
    let thumb:String
}

//MARK: My Profile API

struct APIMyProfile:Codable{
    let username:String
    let profile_image:String
    let user_location:String
    let no_ofjobsposted:Int
    let no_ofhired:Int
    let no_oftaskcompleted:Int
    let mobile:String
    let country_code:String
    let emailid:String
    let email_verification:Int
    let rating:Int
    let no_ofreviews:Int
    let howtorate:String
}

//MARK: Login API

struct APILogin:Codable{
    let otp:Int
    let userid:Int
    var history:String
    let status:String
    var isCompleted:Int
    
    init(data:[String:Any]){
        self.otp = data["otp"] as! Int
        self.userid = data["userid"] as! Int
        self.history = data["history"] as! String
        self.status = data["status"] as! String
        self.isCompleted = data["isCompleted"] as! Int
    }
}

//MARK: Partner Profile API
/*
 "partner_name": "arun j",
 "partner_image": "http://taskmario.dev.webcastle.in/storage/app/provider/default_profile_img.png",
 "partner_location": "",
 "rating": 3,
 "no_ofreviews": 3,
 "yearsOfexperiance": "24 Days",
 "tasksCompleted": 0,
 "partner_description": "",
 "verifiedcertificates": [
 "http://taskmario.dev.webcastle.in/storage/app/servdocuments/6cCsIgN1wM5n3oePLGzShAnsfe5gRi68vufFyqpp.pdf"
 ],
 "portfolio": [
 "http://taskmario.dev.webcastle.in/storage/app/servdocuments/7zWVhlxbo0Jnyvx7mb1eCAp3u9hpW72tYt208DTX.png",
 "http://taskmario.dev.webcastle.in/storage/app/servdocuments/fYtJ4x8xtHV6ccpgk4dErxmXyHxW6bymX7qHVJfR.png"
 ],
 "AllRatings": {
 "Good": 1,
 "Below Average": 2
 },
 "Clientreviews": [
 {
 "client_name": "RemiXX Rah",
 "client_image": "http://taskmario.dev.webcastle.in/storage/app/provider/default_profile_img.png",
 "client_location": "Kochi, Kerala, India",
 "client_rating": 4,
 "client_comment": "jhjfjjfifyhkjgk"
 }
 ]
 */

struct APIPartnerProfile:Codable{
    let partner_name:String
    let partner_image:String
    let partner_location:String
    let rating:Int
    let no_ofreviews:Int
    let yearsOfexperiance:Int
    let yearsOfexperianceType:String
    let tasksCompleted:Int
    let partner_description:String
    let verifiedcertificates:[String]
    let portfolio:[String]
    let AllRatings:[String:Int]
    let Clientreviews:[ClientReviews]
}

struct ClientReviews:Codable{
    let client_name:String
    let client_image:String
    let client_location:String
    let client_rating:Int
    let client_comment:String
}

//MARK: Closed Tasks API

struct APIClosedTasks:Codable{
    let task_status:String
    let subcat_id:Int
    let subcat_image:String
    let subcat_name:String
    let closeddate:String
    let location:String
    let bidamount:String
    let HiredPartnerDetails:HiredPartnerDetails
}

struct HiredPartnerDetails:Codable {
    let partner_id:Int
    let partner_name:String
    let partner_image:String
    let partnerdistance:String
    let partner_location:String
    let partner_rating:Double
    let tasksCompleted:Int
    let description:String
}

/*
 [
     {
         "notification_id": 7,
         "notification_name": "Task Completed",
         "notification_url": "http://taskmario.dev.webcastle.in/customer/request/320",
         "notification_date": "08-02-2018"
     },
     {
         "notification_id": 16,
         "notification_name": "Task Completed",
         "notification_url": "http://taskmario.dev.webcastle.in/customer/request/159",
         "notification_date": "19-02-2018"
     },
 ]
 */
//MARK: Notifications API

struct APINotifications:Codable{
    let notification_id:Int
    let notification_name:String
    let notification_url:String
    let notification_date:String
}


//MARK: Chat List API

/*
 "partner_id": 92,
 "partner_name": "Smithu Izudheen",
 "partner_image": "http://taskmario.dev.webcastle.in/storage/app/provider/343175image.png",
 "partner_profession": "Wedding Planner",
 "channel": "Taskmario_212_227_92",
 "channel_sid": "CHa8ce6790000b44f38f4bfa693d181d1a",
 "last_chat": "hey",
 "last_chat_time": "17 minutes ago",
 "unreadmsg": true,
 "unreadMsgCount": 1,
 "task_id": 212
 */

struct APIChatList:Codable {
    let partner_id:Int
    let partner_name:String
    let partner_image:String
    let partner_profession:String
    let channel:String
    let channel_sid:String
    let last_chat:String
    let last_chat_time:String
    let unreadmsg:Bool
    let unreadMsgCount:Int
    let task_id:Int
    let hired:Int
}

//MARK: Tasks API

/*
 {
     "taskid": 260,
     "task_status": "On Going",
     "subcat_id": 21,
     "subcat_name": "Architect",
     "subcat_image": "public/category/image/rwDXQjGxEHQW8dfh1kEgDs7fskAZhdn8lPIXZVGG.jpeg",
     "location": "Ras Al-Khaimah - North Ras Al Khaimah - United Arab Emirates",
     "receivedResponses": 1,
     "maxResponses": "5",
     "taskdate": {
         "date": "2018-03-28 09:41:46.000000",
         "timezone_type": 3,
         "timezone": "UTC"
     },
     "isCompleted": false,
     "mainTaskDescription": "Task Description",
     "subTaskDescription": "Sub Description",
     "respondersList": [
         {
             "partnerID": 118,
             "partnerName": "arunp",
             "rating": 4,
             "numberOfTasks": 2,
             "description": "Architect",
             "bidAmount": 21232,
             "channel": "Taskmario_260_92_118",
             "channelSID": "CH1b590b4075a44274894150c799b83c62",
             "last_chat": "asadsad",
             "last_chat_time": {
                 "date": "2018-03-28 10:33:04.000000",
                 "timezone_type": 3,
                 "timezone": "UTC"
             }
         }
     ]
 }
 */

struct APITaskList:Codable{
    let taskid:Int
    let task_status:String
    let subcat_id:Int
    let subcat_name:String
    let subcat_image:String
    let location:String
    let receivedResponses:Int
    let maxResponses:String
    let taskdate:TaskDate
    let isCompleted:Bool
    let mainTaskDescription:String
    let subTaskDescription:String
    let respondersList:[Responders]
}

struct TaskDate:Codable {
    let date:String
    let timezone_type:Int
    let timezone:String
}

/*
 {
 "partnerID": 118,
 "partnerName": "arunp",
 "rating": 4,
 "numberOfTasks": 2,
 "description": "Architect",
 "bidAmount": 21232,
 "channel": "Taskmario_260_92_118",
 "channelSID": "CH1b590b4075a44274894150c799b83c62",
 "last_chat": "asadsad",
 "last_chat_time": {
 "date": "2018-03-28 10:33:04.000000",
 "timezone_type": 3,
 "timezone": "UTC"
 }
 }
 */


struct Responders:Codable {
    let partnerID:Int
    let partnerName:String
    let partnerimage:String
    let rating:Double
    let numberOfTasks:Int
    let description:String
    let bidAmount:String
    let channel:String
    let channelSID:String
    let last_chat:String
    let last_chat_time:TaskDate
    let partnerlocation:String
    let partnerdistance:String
}















