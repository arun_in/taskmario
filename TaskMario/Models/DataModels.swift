//
//  DataModels.swift
//  TaskMario
//
//  Created by Appzoc on 12/03/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import Foundation
import UIKit


struct QuestionHandler {
    let id:Int
    let question:String
    let inputType:String
    let conditional:Int
    let conditionalValue:String
    let categoryID:Int
    let hasCondition:Int
    let answers:[AnswerHandler]
    
    // data source
    var textAnswer: String = "" // for text input type
    var textareaAnswer: String = ""
    var autocompleteAnswer: String = ""
    var radioAnswer: String = ""
    var dateTimePickerAnswer: String = ""
    var checkboxAnswer: [String] = []
    var radioId: Int = 0
    var checkboxId: Int = 0
    var registerAnswer: String = ""
    
    init(registerQuestion: String) {
        self.question = registerQuestion
        self.id = 0
        self.inputType = ""
        self.conditional = 0
        self.conditionalValue = ""
        self.categoryID = 0
        self.hasCondition = 0
        self.answers = []
    }

    init(question:QuestionsAPI) {
        self.id = question.id
        self.question = question.question
        self.inputType = question.input_type
        self.conditional = question.conditional
        self.conditionalValue = question.conditional_value
        self.categoryID = question.category_id
        self.hasCondition = question.has_condition
        
        var tempArray:[AnswerHandler] = []
        for item in question.answers{
            let a = AnswerHandler(id: item.id, questionID: item.question_id, label: item.label)
            tempArray.append(a)
        }
        
        self.answers = tempArray
    }
    
    init(subquestion:SubQuestions) {
        self.id = subquestion.id
        self.question = subquestion.question
        self.inputType = subquestion.input_type
        self.conditional = subquestion.conditional
        self.conditionalValue = subquestion.conditional_value
        self.categoryID = subquestion.category_id
        self.hasCondition = subquestion.has_condition
        
        var tempArray:[AnswerHandler] = []
        
        for item in subquestion.answers{
            print(item)
            let a = AnswerHandler(id: item.id, questionID: item.question_id, label: item.label)
            tempArray.append(a)
        }
//        for item in subquestion.answers{
//            let a = AnswerHandler(id: item.id, questionID: item.question_id, label: item.label)
//            tempArray.append(a)
//        }
        
        self.answers = tempArray
    }
}

struct AnswerHandler{
    let id:Int
    let questionID:Int
    let label:String
}

struct ChatRoomData{
    let partnerID:Int
    let taskID:Int
    let partnerName:String
    let partnerImage:String
    let subcatName:String
}



protocol SubQuestionIdentifier {
    func checkForSubQuestion()
}



