//
//  AppState.swift
//  TaskMario
//
//  Created by Appzoc on 13/02/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import Foundation
import Kingfisher


struct AppState {
    static var selectedLocation:String{
        get{
            if let loc = UserDefaults.standard.string(forKey: "location") {
                return loc
            }else{
                return "Choose Location"
            }
        }
        set{
           // selectedLocation = newValue
            UserDefaults.standard.set(newValue, forKey: "location")
        }
    }
    
    static var isLoggedIn:Bool{
        get{
            if let logStatus = UserDefaults.standard.string(forKey: "logStatus"){
                if logStatus == "yes"{
                    return true
                }else{
                    return false
                }
            }else{
                return false
            }
        }
        set{
          //  isLoggedIn = newValue
            if newValue{
                UserDefaults.standard.set("yes", forKey: "logStatus")
            }else{
                UserDefaults.standard.set("no", forKey: "logStatus")
            }
        }
    }
    
    //Ras Al Khaimah - 25.785774, 55.989474
    
    static var latitude:String {
        get{
            if let lat = UserDefaults.standard.string(forKey: "lat"){
                return lat
            }else{
                return "25.785774"
            }
        }
        set{
           // latitude = newValue
            UserDefaults.standard.set(newValue, forKey: "lat")
        }
    }
    static var longitude:String{
        get{
            if let long = UserDefaults.standard.string(forKey: "long"){
                return long
            }else{
                return "55.989474"
            }
        }
        set{
           // longitude = newValue
            UserDefaults.standard.set(newValue, forKey: "long")
        }
    }
    
    
    static var homePageData:APIHomePage?{
        didSet{
            subcategoryListMain.removeAll()
            for cat in (homePageData?.CategoryList)! {
                for sub in cat.Subcategories{
                 subcategoryListMain.append(sub)
                }
            }
        }
    }
    
    static var subcategoryListMain:[Subcategories] = []
    
    static let modifier = AnyModifier { request in
        let username = "dev"
        let password = "dev"
        let loginString = String(format: "%@:%@", username, password)
        let loginData = loginString.data(using: String.Encoding.utf8)!
        let base64LoginString = loginData.base64EncodedString()
        
        var r = request
        r.setValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")
        return r
    }
    
    static var userData:APILogin?{
        get{
            if let userInfo = UserDefaults.standard.value(forKey: "userData") as? [String:Any]{
                let data = APILogin(data: userInfo)
                return data
            }else{
                return nil
            }
        }
        set{
          //  userData = newValue
            if let dataUnwrapped = newValue{
                let diction = ["userid":dataUnwrapped.userid,"otp":dataUnwrapped.otp,"history":dataUnwrapped.history,"status":dataUnwrapped.status, "isCompleted":dataUnwrapped.isCompleted] as [String : Any]
            UserDefaults.standard.set(diction, forKey: "userData")
            }
            if newValue != nil{
                isLoggedIn = true
            }else{
                isLoggedIn = false
            }
        }
    }
    
    static var tempUserData:APILogin?
    
    //MARK: Navigation Reference
    static var navigation:UINavigationController? = nil
    
    static var userLoginData:(String,String)? = nil
    
    static var shouldRegister:Bool = true
    
    static var chosenAnswers:[[String:String]] = []
    
    static var answerForDisplay:[String] = []
    
    static var textReflowTableRef:UITableView = UITableView()
    
    static var registerData:RegisterData{
        get{
            if let regData = UserDefaults.standard.value(forKey: "regData") as? [String:String]{
                let data = RegisterData(data: regData)
                return data
            }else{
                return RegisterData(data: ["name":"","email":""])
            }
        }
        set{
            let diction = ["name":newValue.name,"email":newValue.email]
            UserDefaults.standard.set(diction, forKey: "regData")
        }
        
    }
        //RegisterData(name: "", email: "")
    
    static var allTasksData:[APITaskList] = []
    static var onGoingTasksData:[APITaskList] = []
    static var hiredTasksData:[APITaskList] = []
    
    static var parentIndexForCheckBox:IndexPath?
    static var selectedAnswersForCheckBox:[String] = []
    
    //All Tasks Controller
    static var ongoingDetailShown:Bool = false
    static var dataForInitiatedCell:APITaskList?
    
    //Questionairre Subquestion Check
    static var subquestionIdentifier: [Int:String] = [:]
    
    //Questionairre CheckBox
    static var checkBoxAnswered = [0,0,0,0]
    static var checkBoxLabels:[String] = []
    //Questionairre Radio Button
    static var radioButtonSelected:String = "ssddffgg"
    
    //Questionairre Submit
    static var questionCategory:Int = 0
    static var feedback:String = ""
    
    
    //TaskID
    static var selectedTaskID:Int = 0
    static var tempSelectedID:Int = 0
    
    //Collapse Hired
    static var collapseHired:CollapseHired?
    
    //Change Ongoing
    static var changeBlueLine:Bool = false
    
    //CategoryID
    static var selectedSubCategory:Int = 0
    
    
    //Register
    static var passedNumberField:Bool = false
    
    //MARK: Color Code
    static var blueColor:UIColor = UIColor(red: 1/255, green: 139/255, blue: 255/255, alpha: 1)
    
    
    //Sync TAsks Delegate Reference
    static var globalSync:SyncTasks?
    
    //Partner Occupation for CHat
    static var partnerOccupation:String = ""
    
    //Global Chat Data
    static var chatDataGlobal:APITaskList?
    
    
    
    
}

struct RegisterData{
    var name:String
    var email:String
    
    init(data: [String:String]) {
        self.name =  data["name"]!
        self.email = data["email"]!
    }
}
