//
//  TMSearchController.swift
//  TaskMario
//
//  Created by Appzoc on 07/02/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import Kingfisher
import NotificationBannerSwift

class TMSearchController: UIViewController {
    
    var category:String? = "All"
    var categoryID:Int? = nil
    
    let searchString = "car service car wash car repair"
    
    var displayData:[Subcategories] = []
    
    var tempFilteredArray:[Subcategories] = []{
        didSet{
            if tempFilteredArray.count == 0{
                listEmptyIndicator.isHidden = false
            }else{
                listEmptyIndicator.isHidden = true
            }
        }
    }
    
    @IBOutlet weak var locationLabel: UILabel!
    
    
    @IBOutlet weak var searchTextField: UITextField!
    
    @IBOutlet weak var categoryLabel: UILabel!
    
    @IBOutlet weak var tableRef: UITableView!
    
    @IBOutlet weak var listEmptyIndicator: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpTable()
        tempFilteredArray = displayData
        tableRef.reloadData()
        tableRef.tableFooterView = UIView()
        //categoryLabel.text = category ?? "Main Category"
        // Do any additional setup after loading the view.
        if searchString.contains("car wash") {
            print("True")
        }else{
            print("False")
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        locationLabel.text = AppState.selectedLocation
    }
    
    func setUpTable(){
        switch category!{
        case "All":
            categoryLabel.text = "All Services"
            for item in AppState.subcategoryListMain{
                displayData.append(item)
        }
        case "Instant Services":
            categoryLabel.text = "Instant Services"
            for item in (AppState.homePageData?.CategoryList)!{
                if item.catname == "Instant Services"{
                    for sub in item.Subcategories{
                        displayData.append(sub)
                    }
                    print(displayData)
                }
            }
        case "Home & Repair":
            categoryLabel.text = "Home & Repair"
            for item in (AppState.homePageData?.CategoryList)!{
                if item.catname == "Home & Repair"{
                    for sub in item.Subcategories{
                        displayData.append(sub)
                    }
                }
            }
        default:
            categoryLabel.text = "All Services"
        }
        
        
    }
    
    @IBAction func setLocationBTN(_ sender: UIButton) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMSetLocationController") as! TMSetLocationController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func homeScreenBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadHomeTab(navigationController: self.navigationController!)
    }
    
    @IBAction func newTaskBTN(_ sender: UIButton) {
       // LoadTabBar.shared.loadNewTaskTab(navigationController: self.navigationController!)
    }
    
    @IBAction func taskListBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadAllTasksTab(navigationController: self.navigationController!)
    }
    
    @IBAction func chatListBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadChatTab(navigationController: self.navigationController!)
    }
    
    @IBAction func profileBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadProfileTab(navigationController: self.navigationController!)
    }
    
    
    @IBAction func searchEditingChanged(_ sender: UITextField) {
        if let text = sender.text{
            if text == ""{
                tempFilteredArray = displayData
            }else{
                tempFilteredArray.removeAll()
                for item in displayData{
                    if item.subcat_name.lowercased().hasPrefix(text.lowercased()){
                        tempFilteredArray.append(item)
                    }
                }
            }
            
            tableRef.reloadData()
        }
        /*
        if let text = sender.text{
            if text == ""{
                tempFilteredArray = displayData
            }else{
                tempFilteredArray.removeAll()
                for item in displayData{
                    if item.subcat_name.lowercased().contains(text.lowercased()){
                        tempFilteredArray.append(item)
                    }
                }
            }
            
            tableRef.reloadData()
        }
         
         */
    }
    
    
    

}

extension TMSearchController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tempFilteredArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TMSearchControllerCell") as! TMSearchControllerCell
        cell.cellLabel.text = "\(tempFilteredArray[indexPath.row].subcat_name)"
        //cell.cellImage.kf.setImage(with: URL(string: "\(tempFilteredArray[indexPath.row].subcaticon)")!)
        cell.cellImage.kf.setImage(with: URL(string: "\(tempFilteredArray[indexPath.row].subcaticon)")!, placeholder: UIImage(named: "painting-paint-roller-"), options: [.requestModifier(AppState.modifier)])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 63
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMQuestionsVC") as! TMQuestionsVC
        print("\(tempFilteredArray[indexPath.row].subcaticon)")
        AppState.answerForDisplay.removeAll()
        vc.titleName = tempFilteredArray[indexPath.row].subcat_name
        vc.subQuestionID = tempFilteredArray[indexPath.row].subcatid
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
}
