//
//  TMSearchControllerCell.swift
//  TaskMario
//
//  Created by Appzoc on 07/02/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class TMSearchControllerCell: UITableViewCell {
    
    
    @IBOutlet weak var cellImage: UIImageView!
    
    @IBOutlet weak var cellLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
