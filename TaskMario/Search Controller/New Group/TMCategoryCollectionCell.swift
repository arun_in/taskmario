//
//  TMCategoryCollectionCell.swift
//  TaskMario
//
//  Created by Appzoc on 22/02/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class TMCategoryCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var categoryLabel: UILabel!
    
    
    @IBOutlet weak var selectIndicator: UIView!
    
    
}
