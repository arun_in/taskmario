//
//  TMSubcategoryTableCell.swift
//  TaskMario
//
//  Created by Appzoc on 22/02/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class TMSubcategoryTableCell: UITableViewCell {
    
    @IBOutlet weak var serviceImage: UIImageView!
    
    @IBOutlet weak var serviceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
