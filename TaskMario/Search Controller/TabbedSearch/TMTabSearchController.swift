//
//  TMTabSearchController.swift
//  TaskMario
//
//  Created by Appzoc on 22/02/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class TMTabSearchController: UIViewController {
    
    let mainDisplayData = AppState.homePageData?.CategoryList
    
    //let dataSource = ["Home Services and Repair","Beauty & Health","Business Services","Home Services & Repair","Electronics & Appliance Repair","Beauty & Health","Business Services","Home Services & Repair"]
    //let dataSource2 = ["Electronics & Appliance Repair","Beauty & Health","Car Wash","Home Services & Repair","Electronics & Appliance Repair","Gardening & Cooking","Business Services","Home Services & Repair"]
    var dataSource:[String] {
        var temp:[String] = []
        for item in mainDisplayData!{
            if item.catname != "Instant Services"{
                temp.append(item.catname)
            }
        }
//        for itemOrdering in temp{
//         print(itemOrdering)
//        }
        return temp
    }
    
    var subCategorySource:[String]{
        var temp:[String] = []
//        for item in mainDisplayData![selectedIndex].Subcategories{
//            temp.append(item.subcat_name)
//        }
        for item in mainDisplayData!{
            if item.catid == selectedCategory{
                for object in item.Subcategories{
                    temp.append(object.subcat_name)
                }
            }
        }
        return temp
    }
    
    var subCategoryImage:[String]{
        var temp:[String] = []
//        for item in mainDisplayData![selectedIndex].Subcategories{
//            temp.append(item.subcaticon)
//        }
        for item in mainDisplayData!{
            if item.catid == selectedCategory{
                for object in item.Subcategories{
                    temp.append(object.subcaticon)
                }
            }
        }
        return temp
    }
    
    var subCategoryID:[Int]{
        var temp:[Int] = []
        for item in mainDisplayData!{
            if item.catid == selectedCategory{
                for object in item.Subcategories{
                    temp.append(object.subcatid)
                }
            }
        }
        return temp
    }
    
    var selectedIndex:Int = 0
    
    var selectedCategory:Int = 0
    
    var isSelected:[Bool] = [true,false,false,false,false,false,false,false]
    
    var labelSizeSource:[CGSize] = [CGSize(width: 200, height: 20.5),
                    CGSize(width: 200, height: 20.5),
                    CGSize(width: 200, height: 20.5),
                    CGSize(width: 200, height: 20.5),
                    CGSize(width: 200, height: 20.5),
                    CGSize(width: 200, height: 20.5),
                    CGSize(width: 200, height: 20.5),
                    CGSize(width: 200, height: 20.5)]
    
    //let imageSource = [UIImage(named: "HomeScreenFiller-2"),UIImage(named: "HomeScreenFiller-3"),UIImage(named: "HomeScreenFiller-4"),UIImage(named: "HomeScreenFiller-5"),UIImage(named: "HomeScreenFiller-2"),UIImage(named: "HomeScreenFiller-3"),UIImage(named: "HomeScreenFiller-4"),UIImage(named: "HomeScreenFiller-5")]
    
    let colorSource:[UIColor] = [.red,.blue,.green,.gray,.orange,.yellow,.purple,.red,.blue]
    
    var categorySelected:Int = 0
    
    @IBOutlet weak var collectionRef: UICollectionView!
    
    @IBOutlet weak var tableRef: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // print("Selected Index",selectedIndex)
        tableRef.tableFooterView = UIView()
        for item in mainDisplayData!{
            print(item.catname)
        }
        
        setUpLabelSize()
        setUpSelectedCell()
        collectionRef.scrollToItem(at: IndexPath(item: selectedIndex, section: 0), at: .centeredHorizontally, animated: true)
       // collectionRef.reloadData()
    }
    
    func setUpLabelSize(){
        for item in 0..<dataSource.count{
            let label = UILabel(frame: CGRect.zero)
            label.text = dataSource[dataSource.count - 1 - item]
            let size = label.intrinsicContentSize
            labelSizeSource[item] = size
        }
    }
    
    
    @IBAction func backBTN(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //TabBarButtons
    
    @IBAction func homeScreenBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadHomeTab(navigationController: self.navigationController!)
    }
    @IBAction func newTaskBTN(_ sender: UIButton) {
         LoadTabBar.shared.loadNewTaskTab(navigationController: self.navigationController!)
    }
    
    @IBAction func tasksListBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadAllTasksTab(navigationController: self.navigationController!)
    }
    
    @IBAction func chatListBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadChatTab(navigationController: self.navigationController!)
    }
    @IBAction func profileBTN(_ sender: UIButton) {
         LoadTabBar.shared.loadProfileTab(navigationController: self.navigationController!)
    }
    
}

extension TMTabSearchController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TMCategoryCollectionCell", for: indexPath) as! TMCategoryCollectionCell
        //let orderedData = dataSource.reversed()
        cell.categoryLabel.text = dataSource[dataSource.count - 1 - indexPath.row]
        
        switch isSelected[indexPath.row] {
        case true:
            cell.categoryLabel.textColor = UIColor.black
            cell.selectIndicator.isHidden = false
        case false:
            cell.categoryLabel.textColor = UIColor.lightGray
            cell.selectIndicator.isHidden = true
        }
        //print("Intrinsic Content Size",cell.categoryLabel.intrinsicContentSize)
        //labelSizeSource[indexPath.row] = cell.categoryLabel.intrinsicContentSize
        //let labelSize = cell.categoryLabel.intrinsicContentSize
        //cell.frame = CGRect(x: cell.frame.minX, y: cell.frame.minY, width: labelSize.width, height: cell.frame.height)
        //cell.backgroundColor = colorSource[indexPath.row]
        //cell.categoryLabel.sizeToFit()
        return cell
    }
    

    
//    func collectionView(_ collectionView: UICollectionView,
//                        layout collectionViewLayout: UICollectionViewLayout,
//                        sizeForItemAt indexPath: IndexPath) -> CGSize {
//
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TMCategoryCollectionCell", for: indexPath) as! TMCategoryCollectionCell
//        let size = cell.categoryLabel.intrinsicContentSize
//        let returnSize = CGSize(width: size.width + 20, height: size.height + 20)
//        return returnSize
//    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       // let cell = collectionRef.cellForItem(at: indexPath) as? TMCategoryCollectionCell
        
        let size = labelSizeSource[indexPath.row]
        print("Intrinsic Size",size)
        let returnSize = CGSize(width: size.width + 10, height: size.height + 30.5)
        //let tempReturn = CGSize(width: 150, height: 50)
        return returnSize
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        categorySelected = indexPath.row
        let cell = collectionView.cellForItem(at: indexPath) as? TMCategoryCollectionCell
        //cell?.backgroundColor = UIColor.red
        for var i in 0..<isSelected.count{
            isSelected[i] = false
        }
        isSelected[indexPath.row] = true
        print(isSelected)
        
        for item in mainDisplayData!{
            if cell?.categoryLabel.text == item.catname{
                selectedCategory = item.catid
            }
        }
        
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        
        DispatchQueue.main.async {
            self.selectedIndex = indexPath.row
            //cell?.selectIndicator.isHidden = false
           // self.tableRef.beginUpdates()
            self.tableRef.reloadData()
            //self.tableRef.reloadData()
         //   self.tableRef.endUpdates()
            collectionView.reloadData()
        }
        
    }
    
    func setUpSelectedCell(){
        for var i in 0..<isSelected.count{
            isSelected[i] = false
        }
        isSelected[selectedIndex] = true
    }
    
    
    
}

extension TMTabSearchController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subCategorySource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TMSubcategoryTableCell") as! TMSubcategoryTableCell
        //cell.serviceImage.image = imageSource[indexPath.row]!
        switch indexPath.row {
        default:
            cell.serviceLabel.text = subCategorySource[indexPath.row]
            print(subCategorySource[subCategorySource.count - 1 - indexPath.row])
            cell.serviceImage.kf.setImage(with: URL(string: "\(subCategoryImage[indexPath.row])")!, placeholder: UIImage(named: "painting-paint-roller-"), options: [.requestModifier(AppState.modifier)])
            cell.separatorInset = UIEdgeInsetsMake(0, 22, 1, 22);

        }
        
        //cell.backgroundColor = colorSource[categorySelected]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMQuestionsVC") as! TMQuestionsVC
        AppState.answerForDisplay.removeAll()
        vc.titleName = subCategorySource[indexPath.row]
        vc.subQuestionID = subCategoryID[indexPath.row]
        print("Subcategory ",subCategoryID)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        //UIView.transition(with: tableView,duration: 0.35,options: .transitionCrossDissolve,                    animations: {  })
//        let cell = tableView.cellForRow(at: indexPath) as! TMSubcategoryTableCell
//
//        cell.transform = CGAffineTransform(translationX: 0, y: 100)
//        cell.layer.shadowColor = UIColor.black.cgColor
//        cell.layer.shadowOffset = CGSize(width: 10, height: 10)
//        cell.alpha = 0
//
//        UIView.beginAnimations("rotation", context: nil)
//        UIView.setAnimationDuration(0.5)
//        cell.transform = CGAffineTransform(translationX: 0, y: 0)
//        cell.alpha = 1
//        cell.layer.shadowOffset = CGSize(width: 0, height: 0)
//        UIView.commitAnimations()
    }
}
