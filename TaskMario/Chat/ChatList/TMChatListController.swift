//
//  TMChatListController.swift
//  TaskMario
//
//  Created by Appzoc on 08/02/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class TMChatListController: UIViewController, JSONParserDelegate
{
    
    var mainChatList:[APIChatList] = []
    var chatListSource:[APIChatList] = []
    {
        didSet{
            DispatchQueue.main.async {
                if self.chatListSource.count == 0{
                    self.chatEmptyIndicator.isHidden = false
                }else{
                    self.chatEmptyIndicator.isHidden = true
                }
            }
        }
    }
    @IBOutlet weak var chatEmptyIndicator: UILabel!
    
    @IBOutlet weak var tableRef: UITableView!
    
    var activityView = ActivityIndicatorAlter()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        activityView.start()
        setUpWebCall()
        self.tableRef.tableFooterView = UIView()
    }
    
    func setUpWebCall(){
        //To Test - 92
        let jsonParser = JSONParser(url: "http://taskmario.dev.webcastle.in/api/chatList", httpMethod: .post, parameters: ["userid":AppState.userData!.userid], delegate: self, into: .ChatList)
        jsonParser.callWebService()
    }
    
    func operateOnJSONResult(model: Any) {
        if let dataModel = model as? [APIChatList]{
            mainChatList = dataModel
            chatListSource = dataModel
            tableRef.reloadData()
            activityView.stop()
            //print(dataModel.first!)
        }
    }
    
    func didReturnWithError(error: Error) {
        activityView.stop()
        Banner.showBannerOn(VC: self, title: "Network Error", message: " ", style: .warning)
        print(error)
    }
    
    @IBAction func homeScreenBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadHomeTab(navigationController: self.navigationController!)
    }
    
    @IBAction func newTaskBTN(_ sender: UIButton) {
         LoadTabBar.shared.loadNewTaskTab(navigationController: self.navigationController!)
    }
    @IBAction func taskListBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadAllTasksTab(navigationController: self.navigationController!)
    }
    
    @IBAction func chatListScreen(_ sender: UIButton) {
        //LoadTabBar.shared.loadChatTab(navigationController: self.navigationController!)
    }
    
    @IBAction func profileBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadProfileTab(navigationController: self.navigationController!)
    }
    
    @IBAction func chatSearchField(_ sender: UITextField) {
        if sender.text == nil || sender.text == "" || sender.text == " "{
            chatListSource = mainChatList
            tableRef.reloadData()
        }else{
            chatListSource.removeAll()
            for item in mainChatList{
                if item.partner_name.lowercased().hasPrefix(sender.text!.lowercased()){
                    chatListSource.append(item)
                }
            }
            tableRef.reloadData()
        }
    }
    
    
}

extension TMChatListController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chatListSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TMChatListCellCell") as! TMChatListCellCell
        cell.lastSeenLabel.text = chatListSource[indexPath.row].last_chat_time
        cell.nameLabel.text = chatListSource[indexPath.row].partner_name
        if chatListSource[indexPath.row].unreadmsg{
            cell.isOnline.isHidden = false
        }else{
            cell.isOnline.isHidden = true
        }
        
        cell.profileImage.kf.setImage(with: URL(string: chatListSource[indexPath.row].partner_image)!, placeholder: UIImage(named: "painting-paint-roller-"), options: [.requestModifier(AppState.modifier)])
        cell.workLabel.text = chatListSource[indexPath.row].partner_profession
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 76
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        
        let partnerData = chatListSource[indexPath.row]
        let chatRoomData = ChatRoomData(partnerID: partnerData.partner_id, taskID: partnerData.task_id, partnerName: partnerData.partner_name, partnerImage: partnerData.partner_image, subcatName: partnerData.partner_profession)
        var hired:Bool = false
        if partnerData.hired == 1{
            hired = true
        }
        vc.isHired = hired
        vc.chatRoomData = chatRoomData
        //vc.chatPartnerDetails = chatListSource[indexPath.row]
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
