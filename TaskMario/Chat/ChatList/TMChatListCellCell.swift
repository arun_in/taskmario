//
//  TMChatListCellCell.swift
//  TaskMario
//
//  Created by Appzoc on 08/02/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class TMChatListCellCell: UITableViewCell {
    
    
    @IBOutlet weak var profileImage: StandardImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var workLabel: UILabel!
    @IBOutlet weak var lastSeenLabel: UILabel!
    

    @IBOutlet weak var isOnline: StandardImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
