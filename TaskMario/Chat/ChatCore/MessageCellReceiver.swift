//
//  MessageCellReceiver.swift
//  TaskMario
//
//  Created by Appzoc on 29/03/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class MessageCellReceiver: UITableViewCell {
    
    @IBOutlet weak var messageWrapper: UIView!
    
    @IBOutlet weak var message: UILabel!
    
    @IBOutlet weak var time: UILabel!
    
    @IBOutlet weak var messageWidth: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setUpCell()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUpCell(){
        messageWidth.constant = UIScreen.main.bounds.width * 0.75
       messageWrapper.layer.cornerRadius = 15
        messageWrapper.layer.shadowRadius = 5
        messageWrapper.layer.shadowColor = UIColor.black.cgColor
    }

}
