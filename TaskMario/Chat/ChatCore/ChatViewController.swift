//
//  ChatViewController.swift
//  TaskMario
//
//  Created by Appzoc on 26/03/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//


import UIKit

import TwilioChatClient
import Alamofire
import Kingfisher

class ChatViewController: UIViewController
{
    @IBOutlet weak var partnerImage: StandardImageView!
    @IBOutlet weak var partnerName: UILabel!
    @IBOutlet weak var taskName: UILabel!
    
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var hireBTNRef: StandardButton!
    
    let activityView = ActivityIndicatorAlter()
    
    var chatRoomData:ChatRoomData?
    var isHired:Bool = false
    
    var channelMain:String = ""
    var userImageData = UIImageView()
    var tokenKey:String = ""
    
    // MARK: Chat variables
    var client: TwilioChatClient? = nil
    var generalChannel: TCHChannel? = nil
    var currentChannel:TCHChannel? = nil
    var messages: [TCHMessage] = []
    
    var hiringResponder:Responders?
    //var chatPartnerDetails:APIChatList?
    
    var isReloadRequired = true
    var messageCount = 5
    var fromTask = false
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        getUserImage()
        if isHired{
            hireBTNRef.isHidden = true
        }
        
        if let chatData = chatRoomData{
            partnerName.text = chatData.partnerName
            taskName.text = chatData.subcatName
            partnerImage.kf.setImage(with: URL(string: chatData.partnerImage), placeholder: UIImage(named: "painting-paint-roller-"), options: [.requestModifier(AppState.modifier)])
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        if isReloadRequired
        {
            isReloadRequired = false
            if let chatData = chatRoomData{
                getChannelSID()
            }
        }
    }
    
    
    func getUserImage(){
        Webservices.postMethod(url: "myProfile", parameter: ["userid":AppState.userData!.userid]) {
            (isSuccessful, response) in
            let data = response["Data"] as? [String:Any]
            if let trueData = data?["profile_image"]{
                if let imageProData = trueData as? String{
                    self.userImageData.kf.setImage(with: URL(string: "\(imageProData)")!, placeholder: UIImage(), options: [.requestModifier(AppState.modifier)],completionHandler: { (image, err, cache, url) in
                        self.tableView.reloadData()
                    })
                    
                    
                }
            }
        }
    }
    
    func getChannelSID()
    {
        var parameter = [String:String]()
        parameter["userid"] = String(describing: AppState.userData!.userid)
        parameter["partnerid"] = String(describing: self.chatRoomData!.partnerID)
        parameter["taskid"] = String(describing: self.chatRoomData!.taskID)
        print(parameter)
        
        Webservices.postMethod(url: "chatRoom", parameter: parameter)
        { (isFetched, resultData) in
            if isFetched
            {
                print(resultData)
                let data = resultData["Data"] as? [String:Any] ?? [String:Any]()
                self.channelMain = data["channel"] as? String ?? "" //"channel_sid"
                self.messageCount = data["conversation_count"] as? Int ?? 5
                print("Message Count ",self.messageCount)
                self.getAccessToken()
            }
        }
    }
    
    func getAccessToken()
    {
        var parameter = [String:String]()
        parameter["userid"] = String(describing: AppState.userData!.userid)
        print(parameter)
        
        Webservices.postMethod(url: "getAccessToken", parameter: parameter)
        { (isFetched, resultData) in
            if isFetched
            {
                print(resultData)
                let data = resultData["Data"] as? [String:String] ?? [String:String]()
                let accessToken = data["access_token"]!
                self.createClient(accesToken: accessToken)
            }
        }
    }
    
    
    func createClient(accesToken:String)
    {
        self.activityView.start()
        TwilioChatClient.chatClient(withToken: accesToken, properties: nil, delegate: self)
        {
            (result, chatClient) in
            self.client = chatClient;
            // Update UI on main thread
//            DispatchQueue.main.async()
//            {
//                print("Logged in as \(self.client!.user!.identity!)")
//            }
        }
    }
    
    
    override func viewDidDisappear(_ animated: Bool)
    {
        super.viewDidDisappear(animated)
        logout()
    }
    
    
    @IBAction func hireFromChat(_ sender: StandardButton)
    {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMHirePartnerController") as! TMHirePartnerController
        vc.syncWithWeb = AppState.globalSync
        if let chatData = chatRoomData{
            vc.chatRoomData = chatData
            vc.taskName = chatData.subcatName
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func backBTN(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func airplaneSendBTN(_ sender: UIButton)
    {
        //textField.resignFirstResponder()
        sender.isUserInteractionEnabled = false
        guard BaseValidator.isNotEmpty(string: textField.text) else
        {
            Banner.showBannerOn(VC: self, title: "", message: "Enter Message", style: .warning)
            sender.isUserInteractionEnabled = true
            return
        }

        if let messages = self.currentChannel?.messages
        {
            let messageOptions = TCHMessageOptions().withBody(textField.text!)
            messages.sendMessage(with: messageOptions, completion: { (result, message) in
                self.saveLastChat(message:self.textField.text!)
                self.textField.text = ""
                self.textField.resignFirstResponder()
                sender.isUserInteractionEnabled = true
                
            })
            
        }
    }
    
    
    // MARK: Login / Logout
    
    
    func logout()
    {
        if let client = client
        {
            client.delegate = nil
            client.shutdown()
            self.client = nil
        }
    }
    
    //MARK: Save Last Chat
    func saveLastChat(message:String){
        print("\(channelMain) \(AppState.userData!.userid) \(message)")
        Alamofire.request("http://taskmario.dev.webcastle.in/api/saveLastChat", method: .post, parameters: ["chat_room":"\(self.channelMain)","sender_id":"\(AppState.userData!.userid)","last_chat":"\(message)"], encoding: URLEncoding.default, headers: nil).authenticate(user: "dev", password: "dev").responseJSON { (response) in
            print(response.result.value)
        }
    }
    
    // MARK: UI Logic
    
    // Dismiss keyboard if container view is tapped
    @IBAction func viewTapped(_ sender: Any) {
        self.textField.resignFirstResponder()
    }
    // Scroll to bottom of table view for messages
    func scrollToBottomMessage() {
        if self.messages.count == 0 {
            return
        }
        let bottomMessageIndex = IndexPath(row: messages.count - 1, section: 0)
        tableView.scrollToRow(at: bottomMessageIndex, at: .bottom, animated: true)
    }
    
}

// MARK: Twilio Chat Delegate
extension ChatViewController: TwilioChatClientDelegate
{
    func chatClient(_ client: TwilioChatClient, synchronizationStatusUpdated status: TCHClientSynchronizationStatus)
    {
        if status == .completed
        {
            // Join (or create) the general channel
            print(channelMain)
            if let channelsList = client.channelsList()
            {
                print()
                channelsList.channel(withSidOrUniqueName: channelMain, completion:
                { (result, channel) in
                    if let channel = channel
                    {
                        self.currentChannel = channel
                        channel.join(completion:
                        { result in
                            print("Channel joined with result \(result)")
                            print(result.isSuccessful())
                            self.activityView.stop()
                            self.loadMessages()
                        })
                    }
                })
            }
        }
    }
    
    //MARK: Add-On-Chat
    
    
    func loadMessages()
    {
        messages.removeAll()
        if currentChannel?.synchronizationStatus == .all
        {   //UInt(self.messageCount)
            var retrieveCount = 50
            if self.messageCount > 0 {
                retrieveCount = self.messageCount
            }
            currentChannel?.messages?.getLastWithCount(UInt(retrieveCount))
            { (result, items) in
                
                self.messages = items ?? [TCHMessage]()
                self.tableView.reloadData()
                DispatchQueue.main.async() {
                    if self.messages.count > 0 {
                        self.scrollToBottomMessage()
                    }
                }
            }
        }
    }
    
    // Called whenever a channel we've joined receives a new message
    
    func chatClient(_ client: TwilioChatClient, channel: TCHChannel,
                    messageAdded message: TCHMessage)
    {
        self.messages.append(message)
        self.tableView.reloadData()
        DispatchQueue.main.async() {
            if self.messages.count > 0 {
                self.scrollToBottomMessage()
            }
        }
    }
    
    
    func getCurrentTime() -> String{
        let now = Date()
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "yyyy-MM-dd HH:mm a"
        let dateString = formatter.string(from: now)
        let timeString = "\(dateString.components(separatedBy: " ")[1]) \(dateString.components(separatedBy: " ")[2])"
        return timeString
    }
}

// MARK: UITextField Delegate
extension ChatViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
       // textField.enablesReturnKeyAutomatically = false
        textField.resignFirstResponder()
      // if (textField.text?.trimmingCharacters(in: .whitespaces).isEmpty)!{
        guard BaseValidator.isNotEmpty(string: textField.text) else
        {
            Banner.showBannerOn(VC: self, title: "", message: "Enter Message", style: .warning)
            return true
        }
        
        if let messages = self.currentChannel?.messages {
            let messageOptions = TCHMessageOptions().withBody(textField.text!)
            messages.sendMessage(with: messageOptions, completion: { (result, message) in
                textField.text = ""
                self.saveLastChat(message:textField.text!)
            })
        }
        return true
    }
}


// MARK: UITableViewDataSource Delegate
extension ChatViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    // Return number of rows in the table
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.messages.count
    }
    
    // Create table view rows
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath)
        -> UITableViewCell {
            let cellSend = tableView.dequeueReusableCell(withIdentifier: "MessageCellSender") as! MessageCellSender
            let cellReceive = tableView.dequeueReusableCell(withIdentifier: "MessageCellReceiver") as! MessageCellReceiver
            
            var currentTime = getCurrentTime()
            let message = self.messages[indexPath.row]
            
            if (message.author?.contains("\(AppState.userData!.userid)"))!{
            cellSend.message.text = "\(message.body!)"
            cellSend.time.text = "\(currentTime)"
//            if let imageData = self.userImageData{
//                cellSend.displayImage.kf.setImage(with: URL(string: "\(imageData)")!, placeholder: UIImage(), options: [.requestModifier(AppState.modifier)])
//            }
            cellSend.displayImage.image = self.userImageData.image

            // Set table cell values
            return cellSend
            }else{
                cellReceive.message.text = "\(message.body!)"
                cellReceive.time.text = "\(currentTime)"
                return cellReceive
            }
    
    }
}


/*
Error

 2018-05-15 14:53:57.523190+0530 TaskMario[531:178328] <<<<TwilioChatClient: 0x12fabdfc0>>>> Chat-iOS[0]:        0x16fd6f000 | 05/15/14:53:57.522 | FATAL    | Chat-iOS | Error validating token: 2
 2018-05-15 14:53:57.542861+0530 TaskMario[531:178587] <<<<TwilioChatClient: 0x12fabdfc0>>>> TNWebsocket[1]:        0x1706bb000 | 05/15/14:53:57.542 | CRITICAL | 4      | TNWebsocket | Received error while waiting for message: Exception: Bad socket descriptor
 2018-05-15 14:53:57.545855+0530 TaskMario[531:178570] <<<<TwilioChatClient: 0x12fabdfc0>>>> TNFinalState[2]:        0x16fb3f000 | 05/15/14:53:57.545 | WARNING  | 11     | TNFinalState | Unexpected event: Error
 2018-05-15 14:53:57.546214+0530 TaskMario[531:178570] <<<<TwilioChatClient: 0x12fabdfc0>>>> TNFinalState[2]:        0x16fb3f000 | 05/15/14:53:57.546 | WARNING  | 11     | TNFinalState | Unexpected event: Disconnected
 0x16fd6f000 | 05/15/14:53:57.555 | DEBUG    | 6      | TNHttpTransportClient | ~TNHttpTransportClient - begin
 0x16fd6f000 | 05/15/14:53:57.556 | DEBUG    | 6      | TNHttpTransportClient | ~TNHttpTransportClient - end

 
*/
