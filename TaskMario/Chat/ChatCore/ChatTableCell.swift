//
//  ChatTableCell.swift
//  TaskMario
//
//  Created by Appzoc on 26/03/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class ChatTableCell: UITableViewCell {
    
    @IBOutlet weak var chatCellBackground: StandardView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        setUpChatCell()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setUpChatCell(){
        chatCellBackground.setSpecificRoundCorners([.bottomLeft,.bottomRight,.topRight], radius: 20)
        chatCellBackground.backgroundColor = UIColor.green
    }
    
}
