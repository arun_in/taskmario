////
////  ChatViewController.swift
////  TaskMario
////
////  Created by Appzoc on 26/03/18.
////  Copyright © 2018 Appzoc. All rights reserved.
////
//
//
//import UIKit
//
//import TwilioChatClient
//import Alamofire
//import Kingfisher
//
//class ChatViewControllerTemp: UIViewController, JSONParserDelegate
//{
//    
//    
//    
//    // Important - update this URL with your Twilio Function URL
//    let tokenURL = "https://YOUR_TWILIO_FUNCTION_DOMAIN_HERE.twil.io/chat-token"
//    
//    let activityView = ActivityIndicatorAlter()
//    
//
//    
//    var hiringResponder:Responders?
//    var chatPartnerDetails:APIChatList?
//    
//    var channelMain:String = "CHfd8a86b53f354a7799878be3d677654b" //"Taskmario_680_751_655"
//    var userImageData:String = ""
//    var tokenKey:String = ""
//    
//    @IBOutlet weak var partnerImage: StandardImageView!
//    @IBOutlet weak var partnerName: UILabel!
//    @IBOutlet weak var taskName: UILabel!
//    
//    
//    // Important - this identity would be assigned by your app, for
//    // instance after a user logs in
//    var identity = "qwtry11"
//    
//    // MARK: Chat variables
//    var client: TwilioChatClient? = nil
//    var generalChannel: TCHChannel? = nil
//    var currentChannel:TCHChannel? = nil
//    var messages: [TCHMessage] = []
//    
//    // MARK: UI controls
//    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
//    @IBOutlet weak var textField: UITextField!
//    @IBOutlet weak var tableView: UITableView!
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        activityView.start()
//        alternateLogin()
//        
//        
//        
//        
//        setUpWebCall()
//        if let responder = hiringResponder{
//            //channelMain = responder.channel
//            
//        }
//        if let responder = chatPartnerDetails{
//            //channelMain = responder.channel
//        }
//        
//        // Listen for keyboard events and animate text field as necessary
//        NotificationCenter.default.addObserver(self,
//                                               selector: #selector(keyboardWillShow),
//                                               name:Notification.Name.UIKeyboardWillShow,
//                                               object: nil);
//        
//        NotificationCenter.default.addObserver(self,
//                                               selector: #selector(keyboardDidShow),
//                                               name:Notification.Name.UIKeyboardDidShow,
//                                               object: nil);
//        
//        NotificationCenter.default.addObserver(self,
//                                               selector: #selector(keyboardWillHide),
//                                               name:Notification.Name.UIKeyboardWillHide,
//                                               object: nil);
//        
//        // Set up UI controls
//        if let partnerData = hiringResponder{
//            print("Partner Image one",partnerData.partnerimage)
//            let name: String? = partnerData.partnerName
//            partnerName.text = name ?? ""
//            partnerImage.kf.setImage(with: URL(string: "\(partnerData.partnerimage)")!, placeholder: UIImage(named: "painting-paint-roller-"), options: [.requestModifier(AppState.modifier)])
//            taskName.text = "\(AppState.partnerOccupation)"
//        }else if let partnerDetails = chatPartnerDetails, let name = partnerDetails.partner_name as? String{
//            print("Partner Image Two",partnerDetails.partner_image)
//            partnerName.text = name
//            partnerImage.kf.setImage(with: URL(string: "\(partnerDetails.partner_image)")!, placeholder: UIImage(named: "painting-paint-roller-"), options: [.requestModifier(AppState.modifier)])
//            let profession: String? = partnerDetails.partner_profession
//            taskName.text = profession ?? ""
//            
//        }
//        
//        self.tableView.separatorStyle = .none
//    }
//    
//    
//    override func viewDidAppear(_ animated: Bool) {
//        // Create the general channel (for public use) if it hasn't been created yet
//        //old-Taskmario_216_92_118 new- Taskmario_228_92_118
//        // activityView.start()
//        // ActivityIndicator.setUpActivityIndicator(baseView: self.view)
//        //        let options = [
//        //            TCHChannelOptionFriendlyName: channel,
//        //            TCHChannelOptionType: TCHChannelType.public.rawValue
//        //            ] as [String : Any]
//        //        if let client = client, let channelsList = client.channelsList() {
//        //
//        //            channelsList.createChannel(options: options, completion: { channelResult, channel in
//        //                if (channelResult.isSuccessful()) {
//        //                    print("Channel created.")
//        //                } else {
//        //                    print("Channel NOT created.")
//        //                }
//        //            })
//        //
//        //
//        //        }
//        
//        //        if let client = client, let channelsList = client.channelsList() {
//        //            channelsList.createChannel(options: options, completion: { channelResult, channel in
//        //                if (channelResult.isSuccessful()) {
//        //                    print("Channel created.")
//        //                } else {
//        //                    print("Channel NOT created.")
//        //                }
//        //            })
//        //
//        //
//        //        }
//        
//        super.viewDidAppear(animated)
//        //login()
//        
//    }
//    
//    override func viewDidDisappear(_ animated: Bool) {
//        super.viewDidDisappear(animated)
//        logout()
//    }
//    
//    func setUpWebCall(){
//        let jsonParser = JSONParser(url: "http://taskmario.dev.webcastle.in/api/myProfile", httpMethod: .post, parameters: ["userid":AppState.userData!.userid], delegate: self, into: .MyProfile)
//        jsonParser.callWebService()
//    }
//    
//    func operateOnJSONResult(model: Any) {
//        if let dataObject = model as? APIMyProfile{
//            userImageData = dataObject.profile_image
//        }
//    }
//    
//    func didReturnWithError(error: Error) {
//        print(error)
//    }
//    
//    
//    @IBAction func hireFromChat(_ sender: StandardButton) {
//        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMHirePartnerController") as! TMHirePartnerController
//        vc.syncWithWeb = AppState.globalSync
//        AppState.selectedTaskID = AppState.tempSelectedID
//        if let responder = hiringResponder{
//            vc.hiringResponder = responder
//        }
//        if let responder = self.chatPartnerDetails{
//            vc.chatPartnerDetails = responder
//        }
//        self.present(vc, animated: true, completion: nil)
//    }
//    
//    @IBAction func backBTN(_ sender: UIButton) {
//        self.navigationController?.popViewController(animated: true)
//    }
//    
//    
//    @IBAction func airplaneSendBTN(_ sender: UIButton) {
//        //textField.resignFirstResponder()
//        sender.isUserInteractionEnabled = false
//        guard BaseValidator.isNotEmpty(string: textField.text) else {
//            sender.isUserInteractionEnabled = true
//            return
//        }
//        
//        if let messages = self.currentChannel?.messages {
//            let messageOptions = TCHMessageOptions().withBody(textField.text!)
//            messages.sendMessage(with: messageOptions, completion: { (result, message) in
//                self.textField.text = ""
//                self.textField.resignFirstResponder()
//                sender.isUserInteractionEnabled = true
//            })
//            
//        }
//    }
//    
//    
//    // MARK: Login / Logout
//    
//    
//    
//    func alternateLogin(){
//        Alamofire.request("http://taskmario.dev.webcastle.in/api/getAccessToken", method: HTTPMethod.post, parameters: ["userid":751 ,"unix_time":"1521632102","token_key":"37996a236ae5d8b4107b2ebe1e9aadfa"], encoding: URLEncoding.default, headers: nil).authenticate(user: "dev", password: "dev").responseJSON { (response) in
//            let jsonValue = response.result.value as! [String:Any]
//            //print(jsonValue["Data"])
//            let tempToken = jsonValue["Data"] as! [String:String]
//            let accessToken = tempToken["access_token"]!
//            self.tokenKey = tempToken["access_token"]!
//            
//            print("Access Token : ",self.tokenKey)
//            
//            switch response.result{
//            case .success( _):
//                self.activityView.stop()
//                let clientProperties = TwilioChatClientProperties()
//                
//                TwilioChatClient.chatClient(withToken: accessToken, properties: nil, delegate: self)
//                {
//                    (result, chatClient) in
//                    self.client = chatClient;
//                    // Update UI on main thread
//                    DispatchQueue.main.async()
//                        {
//                            print("Logged in as \(self.client!.user!.identity!)")
//                    }
//                }
//                
//                
//                //                TwilioChatClient.chatClient(withToken: self.tokenKey, properties: clientProperties, delegate: self) {
//                //                    (result, chatClient) in
//                //                    self.client = chatClient
//                //
//                //                    print("Client", self.client?.channelsList() as Any)
//                
//                
//                //                    self.client?.channelsList()?.userChannelDescriptors(completion: { (result, paginator) in
//                //                        if result.isSuccessful(){
//                //                            for channelX in (paginator?.items())!{
//                //                                print("Channel : \(channelX.friendlyName)")
//                //                            }
//                //                        }
//                //                    })
//                
//                
//                
//                
//                //                    if let channelList = chatClient?.channelsList(){
//                //                        channelList.channel(withSidOrUniqueName: self.channelMain, completion: { (statusResult, chatChannel) in
//                //                            if statusResult.isSuccessful(){
//                //                                chatChannel!.join(completion: { (statusInitial) in
//                //                                    if statusInitial.isSuccessful(){
//                //                                        self.currentChannel = chatChannel
//                //                                        print("Joined Channel First time")
//                //                                    }
//                //                                })
//                //                            }else{
//                //                                self.createChannelWithName(name: self.channelMain, completion: { (statusNew, channelNew) in
//                //                                    if statusNew{
//                //                                        channelNew?.join(completion: { (statusFinal) in
//                //                                            if statusFinal.isSuccessful(){
//                //                                                self.currentChannel = channelNew
//                //                                                print("Channel New Joined second time")
//                //                                            }
//                //                                        })
//                //                                    }
//                //                                })
//                //                            }
//                //                        })
//                //                    }
//                
//                
//                // Update UI on main thread
//                //                    self.activityView.stop()
//                //                    DispatchQueue.main.async() {
//                //                        self.navigationItem.prompt = "Logged in as \"\(self.identity)\""
//                //                    }
//            //                }
//            case .failure(let err):
//                self.activityView.stop()
//                Banner.showBannerOn(VC: self, title: "Network Error", message: " ", style: .warning)
//                print(err)
//            }
//        }
//    }
//    
//    
//    func logout() {
//        if let client = client {
//            client.delegate = nil
//            client.shutdown()
//            self.client = nil
//        }
//    }
//    
//    // MARK: Keyboard Dodging Logic
//    
//    @objc func keyboardWillShow(notification: NSNotification) {
//        if let keyboardRect = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
//            UIView.animate(withDuration: 0.1, animations: { () -> Void in
//                // self.bottomConstraint.constant = keyboardRect.height + 10
//                self.view.layoutIfNeeded()
//            })
//        }
//    }
//    
//    @objc func keyboardDidShow(notification: NSNotification) {
//        self.scrollToBottomMessage()
//    }
//    
//    @objc func keyboardWillHide(notification: NSNotification) {
//        UIView.animate(withDuration: 0.1, animations: { () -> Void in
//            // self.bottomConstraint.constant = 20
//            self.view.layoutIfNeeded()
//        })
//    }
//    
//    // MARK: UI Logic
//    
//    // Dismiss keyboard if container view is tapped
//    @IBAction func viewTapped(_ sender: Any) {
//        self.textField.resignFirstResponder()
//    }
//    // Scroll to bottom of table view for messages
//    func scrollToBottomMessage() {
//        if self.messages.count == 0 {
//            return
//        }
//        let bottomMessageIndex = IndexPath(row: messages.count - 1, section: 0)
//        tableView.scrollToRow(at: bottomMessageIndex, at: .bottom, animated: true)
//    }
//    
//}
//
//// MARK: Twilio Chat Delegate
//extension ChatViewControllerTemp: TwilioChatClientDelegate {
//    
//    
//    func chatClient(_ client: TwilioChatClient, synchronizationStatusUpdated status: TCHClientSynchronizationStatus)
//    {
//        if status == .completed
//        {
//            // Join (or create) the general channel
//            let defaultChannel = "Taskmario_563_751_655"
//            if let channelsList = client.channelsList()
//            {
//                channelsList.channel(withSidOrUniqueName: defaultChannel, completion: { (result, channel) in
//                    if let channel = channel
//                    {
//                        self.generalChannel = channel
//                        channel.join(completion: { result in
//                            print("Channel joined with result \(result)")
//                            print(result.isSuccessful())
//                        })
//                    }
//                })
//            }
//        }
//    }
//    
//    
//    //    func chatClient(_ client: TwilioChatClient, synchronizationStatusUpdated status: TCHClientSynchronizationStatus) {
//    //        if status == .completed {
//    //            // Join (or create) the general channel
//    //            _ = channelMain //"Taskmario_228_92_118"
//    //            //"general"
//    //            //let clientProperties = TwilioChatClientProperties()
//    //
//    //
//    //            if let channelList = self.client?.channelsList(){
//    //                channelList.channel(withSidOrUniqueName: channelMain, completion: { (statusResult, chatChannel) in
//    //                    if statusResult.isSuccessful(){
//    //                        chatChannel?.join(completion: { (statusInitial) in
//    //                            if statusInitial.isSuccessful(){
//    //                                self.currentChannel = chatChannel
//    //                                print("Joined Channel First time")
//    //                            }else{
//    //                                print("Channel exists, Join Failed")
//    //                            }
//    //                        })
//    //                    }else{
//    //                        self.createChannelWithName(name: self.channelMain, completion: { (statusNew, channelNew) in
//    //                            if statusNew{
//    //                                channelNew?.join(completion: { (statusFinal) in
//    //                                    if statusFinal.isSuccessful(){
//    //                                        self.currentChannel = channelNew
//    //                                        print("Channel New Joined second time")
//    //                                    }else{
//    //                                        print("Channel created, Join Fail")
//    //                                    }
//    //                                })
//    //                            }
//    //                        })
//    //                    }
//    //                })
//    //            }
//    
//    
//    
//    //            if let channelsList = client.channelsList() {
//    //                channelsList.channel(withSidOrUniqueName: channelMain, completion: { (result, channel) in
//    //                    if let channel = channel {
//    //                        self.generalChannel = channel
//    //                        channel.join(completion: { result in
//    //                            print("Channel joined with result \(result.isSuccessful())")
//    //
//    //                        })
//    //                    } else {
//    //                        //"Taskmario_228_92_118"
//    //                        // Create the general channel (for public use) if it hasn't been created yet
//    //                        channelsList.createChannel(options: [TCHChannelOptionFriendlyName:channel , TCHChannelOptionType: TCHChannelType.public.rawValue], completion: { (result, channel) -> Void in
//    //                            if result.isSuccessful() {
//    //                                self.generalChannel = channel
//    //                                self.generalChannel?.join(completion: { result in
//    //                                    self.generalChannel?.setUniqueName(self.channelMain, completion: { result in
//    //                                        print("channel unique name set")
//    //                                    })
//    //                                })
//    //                            }
//    //                        })
//    //                    }
//    //                })
//    //            }
//    
//    //        }
//    //    }
//    
//    func createChannelWithName(name: String, completion: @escaping (Bool, TCHChannel?) -> Void) {
//        if (name == "general") {
//            completion(false, nil)
//            return
//        }
//        
//        let channelOptions:[NSObject : AnyObject] = [
//            TCHChannelOptionFriendlyName as NSObject: name as AnyObject,
//            TCHChannelOptionType as NSObject: TCHChannelType.public.rawValue as AnyObject
//        ]
//        //UIApplication.shared.isNetworkActivityIndicatorVisible = true;
//        self.client?.channelsList()!.createChannel(options: channelOptions as! [String : Any]) { result, channel in
//            //UIApplication.shared.isNetworkActivityIndicatorVisible = false;
//            completion(result.isSuccessful(), channel)
//        }
//    }
//    
//    //    func loadMessages() {
//    //        messages.removeAll()
//    //        if currentChannel?.synchronizationStatus == .all {
//    //            currentChannel?.messages?.getLastWithCount(100) { (result, items) in
//    //                self.addMessages(newMessages: Set(items!))
//    //            }
//    //        }
//    //    }
//    
//    //    func joinGeneralChatRoomWithCompletion(completion: @escaping (Bool) -> Void) {
//    //        //let uniqueName = ChannelManager.defaultChannelUniqueName
//    //        if let channelsList = client?.channelsList() {
//    //            channelsList.channel(withSidOrUniqueName: channel) { result, channel in
//    //                self.generalChannel = channel
//    //
//    //                if self.generalChannel != nil {
//    //                    self.joinGeneralChatRoomWithUniqueName(name: nil, completion: completion)
//    //                } else {
//    //                    self.createGeneralChatRoomWithCompletion { succeeded in
//    //                        if (succeeded) {
//    //                            self.joinGeneralChatRoomWithUniqueName(name: tempChannelHolder, completion: completion)
//    //                            return
//    //                        }
//    //
//    //                        completion(false)
//    //                    }
//    //                }
//    //            }
//    //        }
//    //    }
//    //
//    //    func joinGeneralChatRoomWithUniqueName(name: String?, completion: @escaping (Bool) -> Void) {
//    //        generalChannel?.join { result in
//    //            if ((result.isSuccessful()) && name != nil) {
//    //                self.setGeneralChatRoomUniqueNameWithCompletion(completion: completion)
//    //                return
//    //            }
//    //            completion((result.isSuccessful()))
//    //        }
//    //    }
//    //
//    //    func createGeneralChatRoomWithCompletion(completion: @escaping (Bool) -> Void) {
//    //        let channelName = ChannelManager.defaultChannelName
//    //        let options:[NSObject : AnyObject] = [
//    //            TCHChannelOptionFriendlyName as NSObject: channelName as AnyObject,
//    //            TCHChannelOptionType as NSObject: TCHChannelType.public.rawValue as AnyObject
//    //        ]
//    //        channelsList!.createChannel(options: options) { result, channel in
//    //            if (result?.isSuccessful())! {
//    //                self.generalChannel = channel
//    //            }
//    //            completion((result?.isSuccessful())!)
//    //        }
//    //    }
//    //
//    //    func setGeneralChatRoomUniqueNameWithCompletion(completion:@escaping (Bool) -> Void) {
//    //        generalChannel.setUniqueName(ChannelManager.defaultChannelUniqueName) { result in
//    //            completion((result?.isSuccessful())!)
//    //        }
//    //    }
//    
//    // Called whenever a channel we've joined receives a new message
//    func chatClient(_ client: TwilioChatClient, channel: TCHChannel,
//                    messageAdded message: TCHMessage) {
//        self.messages.append(message)
//        self.tableView.reloadData()
//        DispatchQueue.main.async() {
//            if self.messages.count > 0 {
//                self.scrollToBottomMessage()
//            }
//        }
//    }
//    
//    
//    func getCurrentTime() -> String{
//        let now = Date()
//        let formatter = DateFormatter()
//        formatter.timeZone = TimeZone.current
//        formatter.dateFormat = "yyyy-MM-dd HH:mm a"
//        let dateString = formatter.string(from: now)
//        let timeString = "\(dateString.components(separatedBy: " ")[1]) \(dateString.components(separatedBy: " ")[2])"
//        return timeString
//    }
//}
//
//// MARK: UITextField Delegate
//extension ChatViewControllerTemp: UITextFieldDelegate {
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        // textField.enablesReturnKeyAutomatically = false
//        textField.resignFirstResponder()
//        // if (textField.text?.trimmingCharacters(in: .whitespaces).isEmpty)!{
//        
//        if let messages = self.currentChannel?.messages {
//            let messageOptions = TCHMessageOptions().withBody(textField.text!)
//            messages.sendMessage(with: messageOptions, completion: { (result, message) in
//                textField.text = ""
//                
//            })
//        }
//        
//        //            if let messages = self.generalChannel?.messages {
//        //                let messageOptions = TCHMessageOptions().withBody(textField.text!)
//        //                messages.sendMessage(with: messageOptions, completion: { (result, message) in
//        //                    textField.text = ""
//        //
//        //                })
//        //            }
//        
//        
//        //  }
//        return true
//    }
//}
//
//
//// MARK: UITableViewDataSource Delegate
//extension ChatViewControllerTemp: UITableViewDataSource, UITableViewDelegate {
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 1
//    }
//    
//    
//    // Return number of rows in the table
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return self.messages.count
//    }
//    
//    // Create table view rows
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath)
//        -> UITableViewCell {
//            let cellSend = tableView.dequeueReusableCell(withIdentifier: "MessageCellSender") as! MessageCellSender
//            let cellReceive = tableView.dequeueReusableCell(withIdentifier: "MessageCellReceiver") as! MessageCellReceiver
//            
//            var currentTime = getCurrentTime()
//            let message = self.messages[indexPath.row]
//            
//            if (message.author?.contains("\(AppState.userData!.userid)"))!{
//                cellSend.message.text = "\(message.body!)"
//                cellSend.time.text = "\(currentTime)"
//                //cellSend.displayImage.kf.setImage(with: URL(string: "\(userImageData)")!, placeholder: UIImage(), options: [.requestModifier(AppState.modifier)])
//                // Set table cell values
//                return cellSend
//            }else{
//                cellReceive.message.text = "\(message.body!)"
//                cellReceive.time.text = "\(currentTime)"
//                return cellReceive
//            }
//            //            let cell = tableView.dequeueReusableCell(withIdentifier: "MessageCell", for: indexPath)
//            //            let message = self.messages[indexPath.row]
//            //
//            //            // Set table cell values
//            //            cell.detailTextLabel?.text = message.author
//            //            cell.textLabel?.text = message.body
//            //            cell.selectionStyle = .none
//            //            return cell
//    }
//    
//    //    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//    //        return 189
//    //    }
//    
//}
//
//
//
//
//
//
//
///*
// 
// func login() {
// 
// // Fetch Access Token from the server and initialize Chat Client - this assumes you are running
// // the PHP starter app on your local machine, as instructed in the quick start guide
// let deviceId = UIDevice.current.identifierForVendor!.uuidString
// let urlString = "\(tokenURL)?identity=\(identity)&device=\(deviceId)"
// 
// TokenUtils.retrieveToken(url: urlString) { (token, identity, error) in
// if let token = token {
// // Set up Twilio Chat client
// TwilioChatClient.chatClient(withToken: token, properties: nil, delegate: self) {
// (result, chatClient) in
// self.client = chatClient;
// // Update UI on main thread
// DispatchQueue.main.async() {
// self.navigationItem.prompt = "Logged in as \"\(self.identity)\""
// }
// }
// } else {
// print("Error retrieving token: \(error.debugDescription)")
// }
// 
// }
// }
// 
// */
//
//
//
//
