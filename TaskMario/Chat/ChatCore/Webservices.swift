//
//  Webservices.swift
//  Exhibiz
//
//  Created by Appzoc on 22/02/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import Foundation
import Alamofire


let baseURL = "http://taskmario.dev.webcastle.in/api/"
//let imageURL = "ExhibizWeb/backend/storage/app/"
//let baseURLImage = "http://app.appzoc.com/ExhibizWeb/backend/storage/app/"
let usernameAuth = "dev"
let passwordAuth = "dev"


final class Webservices
{
    private init(){}
    class func getMethod(url:String, errorBanner:Bool = true, CompletionHandler:@escaping ((Bool,[String:Any])->()))
    {
        let activity = ActivityIndicatorAlter()
        activity.start()
        print("\(baseURL)\(url)")
        let user = usernameAuth
        let password = passwordAuth
        let credentialData = "\(user):\(password)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)"]
        
        Alamofire.request("\(baseURL)\(url)",
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers:headers)
            .validate()
            .responseJSON
            { response in
                activity.stop()
                switch(response.result)
                {
                case .success(_):
                    if response.result.value != nil
                    {
                        let result = response.result.value as! [String:Any]
                        print(result)
                        
                        let Errorcode = result["Error_Code"] as? String ?? "1"
                        let Message = result["Message"] as? String ?? "Error occured while fetching data"
                        
                        if Errorcode == "0"
                        {
                            CompletionHandler(true, result)
                        }
                        else
                        {   CompletionHandler(false, ["":""])
                            if errorBanner == true
                            {
                                print("Error 0")
                            }
                        }
                    }
                    
                    break
                    
                case .failure(_):
                    CompletionHandler(false, ["":""])
                    print("Failed")
                    break
                }
        }
    }

    
    
    //Mark:- POST Methods
    class func postMethod(url:String, parameter:[String:Any], CompletionHandler:@escaping ((Bool,[String:Any])->()))
    {
        let activity = ActivityIndicatorAlter()
        activity.start()
        
        let user = usernameAuth
        let password = passwordAuth
        let credentialData = "\(user):\(password)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)","Content-Type":"application/x-www-form-urlencoded"]
        
        Alamofire.request("\(baseURL)\(url)",
                          method: .post,
                          parameters: parameter,
                          encoding: URLEncoding.default,
                          headers:headers)
            .responseJSON
            { response in
                activity.stop()
                switch(response.result)
                {
                case .success(_):
                    if response.result.value != nil
                    {
                        let result = response.result.value as! [String:Any]
                        print(result)
                        
                        let Errorcode = result["ErrorCode"] as? Int ?? 1
                        let Message = result["Message"] as? String ?? "Error occured while fetching data"
                        
                        if Errorcode == 0
                        {
                            CompletionHandler(true, result)
                        }
                        else
                        {   CompletionHandler(false, ["":""])
                            print("Error 1")
                        }
                    }
                    
                    break
                    
                case .failure(_):
                    CompletionHandler(false, ["":""])
                    print("Failed")
                }
        }
    }
    
    
    // Mark:- Muiltipart data web call
    
    class func postMethodMultiPartImage(url:String, parameter:[String:Any], imageParameter: [String:[UIImage]] , CompletionHandler:@escaping ((Bool,[String:Any])->())) {
       
        let activity = ActivityIndicatorAlter()
        activity.start()
        print("\(baseURL)\(url)")
        
        let user = usernameAuth
        let password = passwordAuth
        let credentialData = "\(user):\(password)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)","Content-Type":"application/json"]
        
        Alamofire.upload(multipartFormData:
            { multipartFormData in
                
                for (key, value) in parameter
                {
                    multipartFormData.append((value as! String).data(using: .utf8)!, withName: key)
                }

                if !imageParameter.isEmpty {
                    let imageKey = imageParameter[imageParameter.startIndex].key
                    let imageArray = imageParameter[imageKey]
                    var index: Int = 1
                    for image in imageArray! {
                        let imageData = UIImageJPEGRepresentation(image, 1)!
                        let imageFileName = imageKey + index.description + ".jpeg"
                        multipartFormData.append(imageData, withName: imageKey, fileName: imageFileName, mimeType: "image/jpeg")
                        index += 1
                    }
                    
                }
                
        },
                         to: "\(baseURL)\(url)",
            headers : headers,
            encodingCompletion:
            { encodingResult in
                
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    upload.responseString(completionHandler: { (response) in
                        print(response)
                    })
                    
                    upload.responseJSON(completionHandler: { response in
                        print(response)
                        if response.result.value != nil
                        {
                            activity.stop()
                            let result = response.result.value as! [String:Any]
                            debugPrint(result)
                            
                            let Errorcode = result["Error_Code"] as? String ?? "1"
                            let Message = result["Message"] as? String ?? "Error occured while fetching data"
                            
                            if Errorcode == "0"
                            {
                                CompletionHandler(true, result)
                            }
                            else
                            {   CompletionHandler(false, ["":""])
                                print("Error 0")
                            }
                        }
                    })
                    
                case .failure(_):
                    activity.stop()
                    CompletionHandler(false, ["":""])
//                    Banner(title: "Oops...!", subtitle: "Something went wrong while connecting", backgroundColor: bannerRed).show(duration: 3)
                    print("Failed")

                }
        }
        )
        
    }
    
    
}

//class ActivityIndicator
//{
//    var activityView:UIActivityIndicatorView!
//    var view = UIView(frame: UIScreen.main.bounds)
//
//    func start()
//    {
//        if activityView == nil
//        { activityView = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
//          activityView.frame = CGRect(x: (view.bounds.maxX/2)-20,
//                                        y: (view.bounds.maxY/2)-20,
//                                        width: 40, height: 40)
//          self.view.addSubview(activityView)
//          self.view.backgroundColor = UIColor(red:0.26, green:0.26, blue:0.26, alpha:0.8)
//          UIApplication.shared.keyWindow?.addSubview(self.view)
//          activityView.startAnimating()
//        }
//        else if activityView.isAnimating == true
//        {
//        }
//        else
//        { UIApplication.shared.keyWindow?.addSubview(self.view)
//          activityView.startAnimating()
//        }
//    }
//
//    func stop()
//    {
//        if activityView == nil || activityView.isAnimating == false
//        {
//        }
//        else
//        { activityView.stopAnimating()
//          self.view.removeFromSuperview()
//        }
//    }
//}



