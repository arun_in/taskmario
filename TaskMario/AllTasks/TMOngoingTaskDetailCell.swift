//
//  TMOngoingTaskDetailCell.swift
//  TaskMario
//
//  Created by Appzoc on 01/03/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import Cosmos
import Alamofire
import Kingfisher

class TMOngoingTaskDetailCell: UITableViewCell {
    
    var accessNav:AccessNavigation?
    
    var dataForCell:APITaskList?
    
    @IBOutlet weak var partnerImage: StandardImageView!
    
    @IBOutlet weak var partnerName: UILabel!
    
    @IBOutlet weak var location: UILabel!
    
    @IBOutlet weak var ratingInStars: CosmosView!
    
    @IBOutlet weak var numberOfTasks: UILabel!
    
    @IBOutlet weak var taskDescription: UILabel!
    
    @IBOutlet weak var bidAmount: UILabel!
    
    var hiringResponder:Responders?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCellWith(data:Responders){
        partnerName.text = data.partnerName
        location.text = "\(data.partnerlocation)  \(data.partnerdistance) Km Away"
        partnerImage.kf.setImage(with: URL(string: "\(data.partnerimage)")!, placeholder: UIImage(named: "painting-paint-roller-"), options: [.requestModifier(AppState.modifier)])
        ratingInStars.rating = data.rating
        numberOfTasks.text = "\(data.numberOfTasks) tasks"
        taskDescription.text = data.description
        bidAmount.text = "Rs.\(data.bidAmount)"
        hiringResponder = data
    }
    
    
    @IBAction func viewPartnerProfileBTN(_ sender: UIButton) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMPartnerProfileController") as! TMPartnerProfileController
        if let data = dataForCell{
            vc.partnerJob = data.subcat_name
            vc.partnerID = data.respondersList[0].partnerID
        }
        accessNav?.accessNavigation(viewCon: vc)
    }
    
    
    @IBAction func toChatController(_ sender: StandardButton) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        //vc.hiringResponder = self.hiringResponder
        if let responder = self.hiringResponder, let cellData = AppState.chatDataGlobal{
            let chatRoomData = ChatRoomData(partnerID: responder.partnerID, taskID: cellData.taskid, partnerName: responder.partnerName, partnerImage: responder.partnerimage, subcatName: cellData.subcat_name)
            var hired = false
            if cellData.task_status == "Converted"{
                hired = true
            }
            vc.isHired = hired
            vc.chatRoomData = chatRoomData
        }else{
            print("Data for CHatroom not found")
        }
        accessNav?.accessNavigation(viewCon: vc)
    }
    
    
    @IBAction func hirePartnerBTN(_ sender: StandardButton) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMHirePartnerController") as! TMHirePartnerController
        vc.syncWithWeb = AppState.globalSync
        vc.accessNav = self.accessNav
        
        if let responder = self.hiringResponder, let cellData = AppState.chatDataGlobal{
            let chatRoomData = ChatRoomData(partnerID: responder.partnerID, taskID: cellData.taskid, partnerName: responder.partnerName, partnerImage: responder.partnerimage, subcatName: cellData.subcat_name)
            vc.chatRoomData = chatRoomData
            vc.taskName = cellData.subcat_name
        }else{
            print("Data for CHatroom not found")
        }
        
        accessNav?.presentThroughController(viewCon: vc)
//        Alamofire.request("http://taskmario.dev.webcastle.in/api/hirePartner", method: .post, parameters: ["userid":92,"partnerid":118,"taskid":260], encoding: URLEncoding.default, headers: nil).authenticate(user: "dev", password: "dev").responseJSON { (response) in
//            print(response.result.value)
//        }
    }
    
}
