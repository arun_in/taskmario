//
//  TMHiredTableCellTwo.swift
//  TaskMario
//
//  Created by Appzoc on 28/02/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import Cosmos
import Kingfisher

class TMHiredTableCellTwo: UITableViewCell {
    
    var navAccess:AccessNavigation!
    
    var collapse:CollapseHired = AppState.collapseHired!
    
    @IBOutlet weak var subcatImage: StandardImageView!
    
    @IBOutlet weak var subcatName: UILabel!
    
    @IBOutlet weak var location: UILabel!
    
    @IBOutlet weak var taskDate: UILabel!
    
    @IBOutlet weak var partnerImage: StandardImageView!
    
    @IBOutlet weak var partnerName: UILabel!
    
    @IBOutlet weak var partnerLastSeen: UILabel!
    
    @IBOutlet weak var ratingInStars: CosmosView!
    
    @IBOutlet weak var numberOfTasks: UILabel!
    
    @IBOutlet weak var partnerDescription: UILabel!
    
    @IBOutlet weak var bidAmount: UILabel!
    
    @IBOutlet weak var chatBTNRef: StandardButton!
    
    @IBOutlet weak var jobCompletedBTNRef: StandardView!
    
    @IBOutlet var completedView: BaseView!
    @IBOutlet var chatAvailableView: BaseView!
    
    var dataForCell:APITaskList?
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @IBAction func collapseDetails(_ sender: UIButton) {
        collapse.collapseHiredDetail()
    }
    
    
    @IBAction func viewProfileBTN(_ sender: IPButton) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMPartnerProfileController") as! TMPartnerProfileController
        if let data = dataForCell{
            vc.partnerJob = data.subcat_name
            vc.partnerID = data.respondersList[0].partnerID
        }
        navAccess.accessNavigation(viewCon: vc)
    }
    

    @IBAction func toChatController(_ sender: StandardButton) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        if let responder = self.dataForCell?.respondersList.first, let cellData = dataForCell{
            let chatRoomData = ChatRoomData(partnerID: responder.partnerID, taskID: cellData.taskid, partnerName: responder.partnerName, partnerImage: responder.partnerimage, subcatName: cellData.subcat_name)
            var hired = false
            if cellData.task_status == "Converted"{
                hired = true
            }
            vc.isHired = hired
            vc.chatRoomData = chatRoomData
        }else{
            print("Data for CHatroom not found")
        }
//        if let responder = dataForCell?.respondersList.first{
//            AppState.partnerOccupation = dataForCell!.subcat_name
//            vc.hiringResponder = responder
//        }
        navAccess.accessNavigation(viewCon: vc)
    }
    
    @IBAction func jobCompletedBTN(_ sender: UIButton) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMJobReviewController") as! TMJobReviewController
        vc.syncWithWeb = AppState.globalSync
        if let taskData = dataForCell{
        vc.partnerDetails = taskData
        vc.provider_id = taskData.respondersList[0].partnerID
            vc.request_id = taskData.taskid
        }
        navAccess.accessNavigation(viewCon: vc)
    }
    
    
    func configureCellWith(data:APITaskList){
        subcatImage.kf.setImage(with: URL(string: "\(data.subcat_image)")!, placeholder: UIImage(named: "painting-paint-roller-"), options: [.requestModifier(AppState.modifier)])
        subcatName.text = data.subcat_name
        location.text = data.location
        
        let date = trimDate(date: data.taskdate.date)
        taskDate.text = "\(date)"
        
        dataForCell = data
        if data.isCompleted{
            chatBTNRef.isHidden = true
            jobCompletedBTNRef.isHidden = false
        }else{
            chatBTNRef.isHidden = false
            jobCompletedBTNRef.isHidden = true
        }
        
        completedView.isHidden = data.isCompleted ? false : true
        
        if let responder = data.respondersList.first{
            partnerName.text = responder.partnerName
            partnerLastSeen.text = "\(responder.partnerlocation) \(responder.partnerdistance) Km Away"
            partnerImage.kf.setImage(with: URL(string: "\(responder.partnerimage)")!, placeholder: UIImage(named: "painting-paint-roller-"), options: [.requestModifier(AppState.modifier)])
            ratingInStars.rating = responder.rating
            numberOfTasks.text = "\(responder.numberOfTasks) tasks"
            partnerDescription.text = responder.description
            bidAmount.text = "Rs.\(responder.bidAmount)"
        }else{
//            partnerName.text = "responder.partnerName"
//            partnerLastSeen.text = "responder.last_chat_time.date"
//            ratingInStars.rating = 5
//            numberOfTasks.text = "responder.numberOfTasks tasks"
//            partnerDescription.text = "responder.description"
//            bidAmount.text = "responder.bidAmount"
        }
    }
    
    func trimDate(date:String)->String{
        //"2018-03-28 11:11:41.000000"
        let dayDate = date.components(separatedBy: " ").first!
        var dateActual = dayDate.components(separatedBy: "-")
        var formattedDate = "\(dateActual[2])-\(dateActual[1])-\(dateActual[0])"
        let timeLong = date.components(separatedBy: " ")[1]
        let time = timeLong.components(separatedBy: ".").first!
        return formattedDate
    }
    
}

protocol CollapseHired{
    func collapseHiredDetail()
}
