//
//  TMAllTasksController.swift
//  TaskMario
//
//  Created by Appzoc on 08/02/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class TMAllTasksController: UIViewController , ReloadForDetail, AccessNavigation, JSONParserDelegate, SyncTasks, CollapseHired{
  
    @IBOutlet weak var allTabRef: UIButton!
    @IBOutlet weak var ongoingTabRef: UIButton!
    @IBOutlet weak var hiredTabRef: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    
    let activityView = ActivityIndicatorAlter()

    var currentTab:String = "All"
    
    var hiredFromChat:Bool = false
    
    var respondersList:[Responders] = []
    var ongoingDetailPrimaryData:APITaskList? = nil
    
    var indexForDetail: Int = 0{
        didSet{
            currentCellArray[indexForDetail] = 1
        }
    }
    
    var tableReference: UITableView = UITableView()
    
    
    @IBOutlet weak var allTaskHighlight: UIView!
    
    @IBOutlet weak var ongoingTaskHighlight: UIView!
    
    @IBOutlet weak var hiredTaskHighlight: UIView!
    
    @IBOutlet weak var tableRef: UITableView!
    
    
    @IBOutlet weak var listEmptyIndicator: UILabel!
    
    
    var cellArrayAll:[Int] = []
    var allTasksData:[APITaskList] = []{
        didSet{
            cellArrayAll.removeAll()
            for item in allTasksData {
                switch item.task_status{
                case "On Going":
                    cellArrayAll.append(1)
                case "Initiated":
                    cellArrayAll.append(2)
                case "Converted":
                    cellArrayAll.append(5)
                default:
                    cellArrayAll.append(0)
                }
            }
        }
    }
    
    var cellArrayOngoing:[Int] = []
    var ongoingTasksData:[APITaskList] = []{
        didSet{
         cellArrayOngoing.removeAll()
            for item in ongoingTasksData{
                cellArrayOngoing.append(2)
            }
        }
    }
    
    var cellArrayHired:[Int] = []
    var hiredTasksData:[APITaskList] = []{
        didSet{
            cellArrayHired.removeAll()
            for item in hiredTasksData{
                cellArrayHired.append(5)
            }
        }
    }
    
    var currentCellArray:[Int] = []{
        didSet{
            if currentCellArray.count == 0{
                listEmptyIndicator.isHidden = false
            }else{
                listEmptyIndicator.isHidden = true
            }
        }
    }
    var currentCellArrayData:[APITaskList] = []
    fileprivate var shouldHideOnGoingLabel: Bool = false
    fileprivate var shouldHideHiredLabel: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        currentCellArray = cellArrayAll
        AppState.globalSync = self
        tableReference = self.tableRef
        setTabColoring(tab: "All")
        // Do any additional setup after loading the view.
        tableRef.allowsSelection = false
        //tableView.registerNib(UINib(nibName: "CustomOneCell", bundle: nil), forCellReuseIdentifier: "CustomCellOne")
        tableRef.register(UINib(nibName: "TMHiredTableCellOne", bundle: nil), forCellReuseIdentifier: "TMHiredTableCellOne")
        tableRef.register(UINib(nibName: "TMHiredTableCellTwo", bundle: nil), forCellReuseIdentifier: "TMHiredTableCellTwo")
        tableRef.register(UINib(nibName: "TMOngoingTaskDetailCell", bundle: nil), forCellReuseIdentifier: "TMOngoingTaskDetailCell")
        allTaskHighlight.isHidden = false
        ongoingTaskHighlight.isHidden = true
        hiredTaskHighlight.isHidden = true
        activityView.start()
        setUpWebCall()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        AppState.globalSync = self
        if hiredFromChat{
            goToHired()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppState.collapseHired = self
       // setUpWebCall()
        AppState.ongoingDetailShown = false
        currentTab = "All"
        titleLabel.text = "All Tasks"
        setTabColoring(tab: "All")
        currentCellArray = cellArrayAll
        currentCellArrayData = allTasksData
        allTaskHighlight.isHidden = false
        ongoingTaskHighlight.isHidden = true
        hiredTaskHighlight.isHidden = true
        shouldHideHiredLabel = false
        shouldHideOnGoingLabel = false
        // setUpWebCall()
        DispatchQueue.main.async {
            self.tableRef.reloadData()
            if self.currentCellArrayData.count > 0 {
                self.tableRef.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
            }
            // self.tableRef.setContentOffset(CGPoint.zero, animated: false)
        }
    }
    
    
    func syncWithWeb() {
        activityView.start()
        //ActivityIndicator.setUpActivityIndicator(baseView: self.view)
        setUpWebCall()
    }
    
    func dataForOngoingDetail(data: APITaskList) {
        
    }
    
    func setUpWebCall(){
        let parser = JSONParser(url: "http://taskmario.dev.webcastle.in/api/alltasks", httpMethod: .post, parameters: ["userid":AppState.userData!.userid], delegate: self, into: .TasksList)
        parser.callWebService()
    }
    
    func operateOnJSONResult(model: Any) {
        if let dataModel = model as? [APITaskList] {
            
            for item in dataModel{
                print("Tasks Status",item.task_status)
            }
            
            allTasksData = dataModel
            ongoingTasksData.removeAll()
            hiredTasksData.removeAll()
            
            var tempOngoingData:[APITaskList] = []
            var tempHiredData:[APITaskList] = []
            
            for item in dataModel{
              // print("\n\n\nTasks Status",item.task_status,"\n")
                if item.task_status == "On Going"{
                  tempOngoingData.append(item)
                  //  print(ongoingTasksData.first!.respondersList)
                }
                if item.task_status == "Converted"{
                    tempHiredData.append(item)
                }
        }
            ongoingTasksData = tempOngoingData
            hiredTasksData = tempHiredData
            
           // print("All Tasks Array",cellArrayAll,"\n\n Ongoing status array",cellArrayOngoing)
            DispatchQueue.main.async {
                if self.currentTab == "OnGoing"{
                 self.currentCellArray = self.cellArrayOngoing
                    self.currentCellArrayData = self.ongoingTasksData
                }else if self.currentTab == "Hired"{
                    self.currentCellArray = self.cellArrayHired
                    self.currentCellArrayData = self.hiredTasksData
                }else{
                self.currentCellArray = self.cellArrayAll
                self.currentCellArrayData = self.allTasksData
                }
                self.tableRef.reloadData()
                self.activityView.stop()
            }
            
           AppState.allTasksData = allTasksData
            AppState.onGoingTasksData = ongoingTasksData
            AppState.hiredTasksData = hiredTasksData
            
        }
    }
    
    func didReturnWithError(error: Error) {
        activityView.stop()
        Banner.showBannerOn(VC: self, title: "Network Error", message: "", style: .warning)
        print(error)
    }
    
    func accessNavigation(viewCon: UIViewController) {
        self.navigationController?.pushViewController(viewCon, animated: true)
    }
    
    func presentThroughController(viewCon: UIViewController) {
        self.present(viewCon, animated: true, completion: nil)
    }
    
    //Delegate Method
    
    func setChatDataGlobal(index: Int) {
        if currentTab == "OnGoing"{
            AppState.chatDataGlobal = ongoingTasksData[index]
        }else if currentTab == "All"{
            AppState.chatDataGlobal = allTasksData[index]
        }
    }
    
    func changeTitle(title: String) {
        DispatchQueue.main.async {
            self.titleLabel.text = title
        }
        
    }
    
    func showDetail(indexArray:[Int],dataArray:[Responders], firstCellData:APITaskList){
        
        print("Index Array",indexArray,"Data Array",dataArray)
        currentTab = "OnGoing"
        currentCellArray = indexArray
        respondersList = dataArray
        ongoingDetailPrimaryData = firstCellData
        tableRef.reloadData()
    }
    
    func showDetailHired(data:APITaskList){
        self.currentCellArrayData = [data]
        //tableRef.reloadData()
    }
    
    func changeTab(to: String) {
        switch to {
        case "OnGoing":
            currentTab = "OnGoing"
            titleLabel.text = "Ongoing Tasks"
            setTabColoring(tab: "OnGoing")
           // currentCellArray = cellArrayOngoing
           // currentCellArrayData = AppState.onGoingTasksData
            allTaskHighlight.isHidden = true
            ongoingTaskHighlight.isHidden = false
            hiredTaskHighlight.isHidden = true
        case "Hired":
            currentTab = "Hired"
            titleLabel.text = "Hired Tasks"
            setTabColoring(tab: "Hired")
           // currentCellArray = cellArrayHired
           // currentCellArrayData = hiredTasksData
            allTaskHighlight.isHidden = true
            ongoingTaskHighlight.isHidden = true
            hiredTaskHighlight.isHidden = false
        case "All":
            currentTab = "All"
            titleLabel.text = "All Tasks"
            setTabColoring(tab: "All")
          //  currentCellArray = cellArrayAll
          //  currentCellArrayData = allTasksData
            allTaskHighlight.isHidden = false
            ongoingTaskHighlight.isHidden = true
            hiredTaskHighlight.isHidden = true
        default:
            currentTab = "OnGoing"
        }
    }
    
    func setTabColoring(tab:String){
        switch tab{
        case "OnGoing":
            ongoingTabRef.setTitleColor(.black, for: [])
            hiredTabRef.setTitleColor(.lightGray, for: [])
            allTabRef.setTitleColor(.lightGray, for: [])
        case "Hired":
            ongoingTabRef.setTitleColor(.lightGray, for: [])
            hiredTabRef.setTitleColor(.black, for: [])
            allTabRef.setTitleColor(.lightGray, for: [])
        case "All":
            ongoingTabRef.setTitleColor(.lightGray, for: [])
            hiredTabRef.setTitleColor(.lightGray, for: [])
            allTabRef.setTitleColor(.black, for: [])
        default:
            ongoingTabRef.setTitleColor(.lightGray, for: [])
            hiredTabRef.setTitleColor(.lightGray, for: [])
            allTabRef.setTitleColor(.black, for: [])
        }
        
    }

    
    
    //SetListType
    
    @IBAction func allTasksBTN(_ sender: UIButton) {
        AppState.ongoingDetailShown = false
        currentTab = "All"
        titleLabel.text = "All Tasks"
        setTabColoring(tab: "All")
        currentCellArray = cellArrayAll
        currentCellArrayData = allTasksData
        allTaskHighlight.isHidden = false
        ongoingTaskHighlight.isHidden = true
        hiredTaskHighlight.isHidden = true
        shouldHideHiredLabel = false
        shouldHideOnGoingLabel = false
       // setUpWebCall()
        DispatchQueue.main.async {
            self.tableRef.reloadData()
            if self.currentCellArrayData.count > 0 {
                self.tableRef.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
            }
           // self.tableRef.setContentOffset(CGPoint.zero, animated: false)
        }
        
    }
    
    @IBAction func ongoingTasksBTN(_ sender: UIButton) {
        AppState.ongoingDetailShown = false
        currentTab = "OnGoing"
        titleLabel.text = "Ongoing Tasks"
        setTabColoring(tab: "OnGoing")
        currentCellArray = cellArrayOngoing
        currentCellArrayData = AppState.onGoingTasksData
        shouldHideOnGoingLabel = true
        //print(ongoingTasksData)
        
        allTaskHighlight.isHidden = true
        ongoingTaskHighlight.isHidden = false
        hiredTaskHighlight.isHidden = true
        //setUpWebCall()
        DispatchQueue.main.async {
            self.tableRef.reloadData()
            if self.currentCellArrayData.count > 0 {
                self.tableRef.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
            }
           // self.tableRef.setContentOffset(CGPoint.zero, animated: false)
        }
    }
    
    @IBAction func hiredTasksList(_ sender: UIButton) {
        AppState.ongoingDetailShown = false
        currentTab = "Hired"
        titleLabel.text = "Hired Tasks"
        setTabColoring(tab: "Hired")                                                                                                                                                                                                              
        currentCellArray = cellArrayHired
        currentCellArrayData = hiredTasksData
        allTaskHighlight.isHidden = true
        ongoingTaskHighlight.isHidden = true
        hiredTaskHighlight.isHidden = false
        shouldHideHiredLabel = true

      //  setUpWebCall()
        DispatchQueue.main.async {
            self.tableRef.reloadData()
            if self.currentCellArrayData.count > 0{
                self.tableRef.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
            }
           // self.tableRef.setContentOffset(CGPoint.zero, animated: false)
        }
    }
    
    func reloadOngoing() {
        AppState.ongoingDetailShown = false
        currentTab = "OnGoing"
        titleLabel.text = "Ongoing Tasks"
        setTabColoring(tab: "OnGoing")
        currentCellArray = cellArrayOngoing
        currentCellArrayData = AppState.onGoingTasksData
        shouldHideOnGoingLabel = true
        //print(ongoingTasksData)
        
        allTaskHighlight.isHidden = true
        ongoingTaskHighlight.isHidden = false
        hiredTaskHighlight.isHidden = true
        //setUpWebCall()
        DispatchQueue.main.async {
            self.tableRef.reloadData()
            if self.currentCellArrayData.count > 0 {
                self.tableRef.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
            }
            // self.tableRef.setContentOffset(CGPoint.zero, animated: false)
        }
    }
    
    func goToHired(){
        AppState.ongoingDetailShown = false
        currentTab = "Hired"
        titleLabel.text = "Hired Tasks"
        setTabColoring(tab: "Hired")
        currentCellArray = cellArrayHired
        currentCellArrayData = hiredTasksData
        allTaskHighlight.isHidden = true
        ongoingTaskHighlight.isHidden = true
        hiredTaskHighlight.isHidden = false
        shouldHideHiredLabel = true
        
        //  setUpWebCall()
        DispatchQueue.main.async {
            self.tableRef.reloadData()
            if self.currentCellArrayData.count > 0{
                self.tableRef.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
            }
            // self.tableRef.setContentOffset(CGPoint.zero, animated: false)
        }
    }
    
    func collapseHiredDetail() {
        AppState.ongoingDetailShown = false
        currentTab = "Hired"
        titleLabel.text = "Hired Tasks"
        currentCellArray = cellArrayHired
        currentCellArrayData = hiredTasksData
        allTaskHighlight.isHidden = true
        ongoingTaskHighlight.isHidden = true
        hiredTaskHighlight.isHidden = false
        //  setUpWebCall()
        DispatchQueue.main.async {
            self.tableRef.reloadData()
            if self.currentCellArrayData.count > 0{
                self.tableRef.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
            }
            // self.tableRef.setContentOffset(CGPoint.zero, animated: false)
        }
    }
    
    //TabBar Controls
    @IBAction func homeScreenBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadHomeTab(navigationController: self.navigationController!)
    }
    
    @IBAction func newTaskBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadNewTaskTab(navigationController: self.navigationController!)
    }
    
    @IBAction func tasksListBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadAllTasksTab(navigationController: self.navigationController!)
    }
    
    @IBAction func chatListBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadChatTab(navigationController: self.navigationController!)
    }
    
    @IBAction func profileBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadProfileTab(navigationController: self.navigationController!)
    }

}

extension TMAllTasksController:UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        switch currentTab {
//        case "Hired":
//            return hiredTasksData.count
//        case "OnGoing":
//            return ongoingTasksData.count
//        case "All":
//            return allTasksData.count
//        default:
//            return currentCellArray.count
//        }
        
        return currentCellArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let responseCell = tableView.dequeueReusableCell(withIdentifier: "TMAllTasksResponsesCell") as! TMAllTasksResponsesCell
        responseCell.seeResponseBTN.tag = indexPath.row
        responseCell.reloadForDetail = self
        responseCell.accessNav = self
        responseCell.shouldHideOnGoingLabel = self.shouldHideOnGoingLabel
        responseCell.seeResponseBTN.tag = indexPath.row
        responseCell.viewDetailsBTNRef.isHidden = false
        
        let cellHiredBasic = tableView.dequeueReusableCell(withIdentifier: "TMHiredTableCellOne") as! TMHiredTableCellOne
        cellHiredBasic.reloadForDetail = self
        cellHiredBasic.viewDetailRef.tag = indexPath.row
        cellHiredBasic.dataForCell = allTasksData[indexPath.row]
        cellHiredBasic.navAccess = self
        cellHiredBasic.shouldHideHiredLabel = self.shouldHideHiredLabel

        
        let cellHiredDetail = tableView.dequeueReusableCell(withIdentifier: "TMHiredTableCellTwo") as! TMHiredTableCellTwo
        let cellOngoingDetail = tableView.dequeueReusableCell(withIdentifier: "TMOngoingTaskDetailCell") as! TMOngoingTaskDetailCell
        cellHiredDetail.navAccess = self
        cellOngoingDetail.accessNav = self
        
        print("Cell All Data Count",allTasksData.count)
        
        switch currentCellArray[indexPath.row] {
        case 1:
            responseCell.viewDetailsBTNRef.isHidden = false
            if currentTab == "All"{
                if AppState.ongoingDetailShown == true && ongoingDetailPrimaryData != nil{
                    AppState.ongoingDetailShown == false
                    responseCell.seeResponseBTN.setTitle("Cancel Request", for: [])
                    responseCell.dataForCell = ongoingDetailPrimaryData
                    responseCell.configureCellWith(data: ongoingDetailPrimaryData!)
                }else{
                    responseCell.dataForCell = allTasksData[indexPath.row]
                    responseCell.configureCellWith(data: allTasksData[indexPath.row])
                }
            }else if currentTab == "OnGoing"{
            //print(ongoingTasksData[indexPath.row])
                if AppState.ongoingDetailShown && ongoingDetailPrimaryData != nil{
                    AppState.ongoingDetailShown == false
                    responseCell.dataForCell = ongoingDetailPrimaryData
                    responseCell.configureCellWith(data: ongoingDetailPrimaryData!)
                }else{
                    responseCell.dataForCell = ongoingTasksData[indexPath.row]
                    responseCell.configureCellWith(data: ongoingTasksData[indexPath.row])
                }
            }
            //responseCell.configureCellWith(data: allTasksData[indexPath.row])
            return responseCell
        case 2:
            if currentTab == "All"{
             responseCell.dataForCell = allTasksData[indexPath.row]
             responseCell.configureCellWith(data: allTasksData[indexPath.row])
            }else if currentTab == "OnGoing"{
             responseCell.dataForCell = ongoingTasksData[indexPath.row]
             responseCell.configureCellWith(data: ongoingTasksData[indexPath.row])
            }
            return responseCell
        case 3:
            //print(respondersList)
            if respondersList.isEmpty == false{
                print(respondersList)
            cellOngoingDetail.dataForCell = ongoingTasksData[indexPath.row-1]
            cellOngoingDetail.configureCellWith(data: respondersList[indexPath.row-1])
            }
            return cellOngoingDetail
        case 4:
            return responseCell
        case 5:
            if currentTab == "All"{
            cellHiredBasic.dataForCell = allTasksData[indexPath.row]
            cellHiredBasic.configureCellWith(data: allTasksData[indexPath.row])
            }else if currentTab == "Hired"{
                cellHiredBasic.dataForCell = hiredTasksData[indexPath.row]
                cellHiredBasic.configureCellWith(data: hiredTasksData[indexPath.row])
            }
            return cellHiredBasic
        case 6:
            cellHiredDetail.configureCellWith(data: currentCellArrayData[indexPath.row])
            return cellHiredDetail
        case 7:
            responseCell.dataForCell = AppState.dataForInitiatedCell
            responseCell.viewDetailsBTNRef.isHidden = true
            responseCell.configureCellWith(data: AppState.dataForInitiatedCell!)
            responseCell.seeResponseBTN.borderColor = UIColor.red
            responseCell.seeResponseBTN.borderWidth = 1
            responseCell.seeResponseBTN.backgroundColor = UIColor.white
            responseCell.seeResponseBTN.setTitle("Cancel Request", for:.normal)
            responseCell.seeResponseBTN.setTitleColor(UIColor.gray, for: [])
            self.tableRef.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
            return responseCell
        default:
            return responseCell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if indexPath.row < 2{
//        return 215
//        }else{
//            return 215
//        }
        switch currentCellArray[indexPath.row]{
        case 0:
            return 224 // RESPONSE CELL - Actual + 40 - 210
        case 1:
            return 224 //
        case 2:
            return 224
        case 3:
            return 250
        case 6:
            return 365
        case 5:
            return 205//190
        default :
            return 224
        }
    }
    
    
    
}

