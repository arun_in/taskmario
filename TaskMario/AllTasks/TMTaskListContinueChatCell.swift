//
//  TMTaskListContinueChatCell.swift
//  TaskMario
//
//  Created by Appzoc on 08/02/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class TMTaskListContinueChatCell: UITableViewCell {
    
    
    @IBOutlet weak var partnerImage: StandardImageView!
    @IBOutlet weak var partnerName: UILabel!
    @IBOutlet weak var partnerLastSeen: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
