//
//  TMHiredTableCellOne.swift
//  TaskMario
//
//  Created by Appzoc on 28/02/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import Kingfisher

class TMHiredTableCellOne: UITableViewCell {
    
    var navAccess:AccessNavigation?
    
    var reloadForDetail:ReloadForDetail? = nil
    
    var dataForCell:APITaskList? = nil
    
    @IBOutlet weak var subcatImage: StandardImageView!
    
    @IBOutlet weak var subcatName: UILabel!
    
    @IBOutlet weak var location: UILabel!
    
    @IBOutlet weak var taskDate: UILabel!
    
    @IBOutlet weak var partnerImage: StandardImageView!
    
    @IBOutlet weak var partnerName: UILabel!
    
    @IBOutlet weak var partnerLastSeen: UILabel!
    
    @IBOutlet weak var viewDetailRef: UIButton!
    @IBOutlet var completedView: BaseView!
    @IBOutlet var isChatAvailableView: BaseView!
    
    @IBOutlet var hiredLBL: UILabel!
    
    @IBOutlet weak var chatBTNRef: StandardButton!
    
    @IBOutlet weak var jobCompletedBTNRef: StandardView!
    
    
    final var shouldHideHiredLabel = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func viewDetailBTN(_ sender: UIButton) {
//        reloadForDetail?.currentCellArray[sender.tag] = 6
//        DispatchQueue.main.async {
//            self.reloadForDetail?.tableReference.reloadData()
//        }
        
        reloadForDetail?.changeTab(to: "Hired")
        reloadForDetail?.currentCellArray = [6]
        reloadForDetail?.tableReference.reloadData()
        reloadForDetail?.changeTitle(title: dataForCell!.subcat_name)
        reloadForDetail?.showDetailHired(data: dataForCell!)
        reloadForDetail?.tableReference.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)

        
        
    }
    
    
    @IBAction func jobCompletedBTN(_ sender: UIButton) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMJobReviewController") as! TMJobReviewController
        vc.syncWithWeb = AppState.globalSync
        if let taskData = dataForCell{
            vc.partnerDetails = taskData
            vc.provider_id = taskData.respondersList[0].partnerID
            vc.request_id = taskData.taskid
        }
        navAccess?.accessNavigation(viewCon: vc)
    }
    
    
    @IBAction func toChatController(_ sender: StandardButton) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        if let responder = self.dataForCell?.respondersList.first, let cellData = dataForCell{
            let chatRoomData = ChatRoomData(partnerID: responder.partnerID, taskID: cellData.taskid, partnerName: responder.partnerName, partnerImage: responder.partnerimage, subcatName: cellData.subcat_name)
            var hired = false
            if cellData.task_status == "Converted"{
                hired = true
            }
            vc.isHired = hired
            vc.chatRoomData = chatRoomData
        }else{
            print("Data for Chatroom not found")
        }
//        if let responder = dataForCell?.respondersList.first{
//            AppState.partnerOccupation = dataForCell!.subcat_name
//            vc.hiringResponder = responder
//        }
        if let nav = navAccess{
        nav.accessNavigation(viewCon: vc)
        }
    }
    
    
    func configureCellWith(data:APITaskList){
        if let responder = data.respondersList.first{
            partnerName.text = responder.partnerName
            let lastSeenTime = convertDate(dateString: responder.last_chat_time.date)
            partnerLastSeen.text = "Last seen \(lastSeenTime)"
                //responder.last_chat_time.date
            partnerImage.kf.setImage(with: URL(string: "\(responder.partnerimage)")!, placeholder: UIImage(named: "dpPlaceHolder"), options: [.requestModifier(AppState.modifier)])
        }
        
        if shouldHideHiredLabel{
            hiredLBL.isHidden = true
        }else{
            hiredLBL.isHidden = false
        }
        
        if data.isCompleted{
            partnerLastSeen.isHidden = true
            chatBTNRef.isHidden = true
            jobCompletedBTNRef.isHidden = false
        }else{
            partnerLastSeen.isHidden = false
            chatBTNRef.isHidden = false
            jobCompletedBTNRef.isHidden = true
    }
    
        subcatImage.kf.setImage(with: URL(string: "\(data.subcat_image)")!, placeholder: UIImage(named: "painting-paint-roller-"), options: [.requestModifier(AppState.modifier)])
        subcatName.text = data.subcat_name
        location.text = data.location
        let date = trimDate(date: data.taskdate.date)
        taskDate.text = "\(date)"
        
        completedView.isHidden = data.isCompleted ? false : true
    }
    
    func trimDate(date:String)->String{
        //"2018-03-28 11:11:41.000000"
        let dayDate = date.components(separatedBy: " ").first!
        var dateActual = dayDate.components(separatedBy: "-")
        var formattedDate = "\(dateActual[2])-\(dateActual[1])-\(dateActual[0])"
        let timeLong = date.components(separatedBy: " ")[1]
        let time = timeLong.components(separatedBy: ".").first!
        return formattedDate
    }
    
    func convertDate(dateString:String) -> String
    {
        print(dateString)
        
        var slicedString = dateString.components(separatedBy: ".").first!
//        if let index = dateString.range(of: ".")?.lowerBound
//        {
//            slicedString = dateString.substring(to: index)
//        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: slicedString)
        dateFormatter.dateFormat = "HH:mm a"
        if let trueDate = date{
            return dateFormatter.string(from: date!)
        }else{
            return ""
        }
    }
    
}
