//
//  TMAllTasksResponsesCell.swift
//  TaskMario
//
//  Created by Appzoc on 08/02/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import Kingfisher

class TMAllTasksResponsesCell: UITableViewCell {
    
    
    @IBOutlet weak var blueLineRef: UIView!
    @IBOutlet weak var taskStatus: UILabel!
    @IBOutlet weak var taskImage: UIImageView!
    @IBOutlet weak var taskName: UILabel!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var taskDate: UILabel!
    @IBOutlet weak var taskMainLabel: UILabel!
    @IBOutlet weak var taskSubLabel: UILabel!
    @IBOutlet weak var viewDetailsBTNRef: StandardButton!
    @IBOutlet weak var seeResponseBTN: StandardButton!
    
    @IBOutlet weak var greySpaceRef: UIView!
    
    
    final var shouldHideOnGoingLabel: Bool = false
    
    var reloadForDetail:ReloadForDetail? = nil
    var accessNav:AccessNavigation?
    
    var dataForCell:APITaskList? = nil
    var detailShown:Bool = false
    var selectedTaskID:Int = 0
    var selectedCategory:Int = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @IBAction func seeResponseAction(_ sender: StandardButton) {

        let grayColor = UIColor(red: 211/255, green: 215/255, blue: 226/255, alpha: 1)
       
        blueLineRef.backgroundColor = grayColor
        taskStatus.isHidden = true

        if dataForCell!.task_status == "Initiated" {
//            if sender.titleLabel?.text != "Cancel Request"{
//                sender.setTitle("Cancel Request", for: [])
//
//            }else if sender.titleLabel?.text == "Cancel Request"{
//                //Call Cancel Task API
//                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMCancelTaskController") as! TMCancelTaskController
//                vc.syncWithWeb = AppState.globalSync
//                vc.dataForCell = self.dataForCell
//                accessNav?.presentThroughController(viewCon: vc)
//               // sender.titleLabel?.text = "0/5 Responses"
//            }
            AppState.dataForInitiatedCell = dataForCell
            switch AppState.ongoingDetailShown {
            case true:
                
                print("Call Cancel Task API")
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMCancelTaskController") as! TMCancelTaskController
                vc.syncWithWeb = AppState.globalSync
                vc.dataForCell = self.dataForCell
                accessNav?.presentThroughController(viewCon: vc)
                AppState.ongoingDetailShown = false
                reloadForDetail?.tableReference.reloadData()
            case false:
                AppState.ongoingDetailShown = true
                AppState.tempSelectedID = self.selectedTaskID
                
                
                
                let responderData:[Responders] = []
                    
                    let indexArray = [7]
                    
                    reloadForDetail?.changeTab(to: "OnGoing")
                    reloadForDetail?.changeTitle(title: dataForCell!.subcat_name)
                
                    reloadForDetail?.showDetail(indexArray: indexArray, dataArray: responderData, firstCellData: dataForCell!)
                    reloadForDetail?.tableReference.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
                // reloadForDetail?.tableReference.reloadData()
            }
            
        }else{
            switch AppState.ongoingDetailShown {
            case true:
                if sender.titleLabel?.text != "Full details"{
                print("Call Cancel Task API")
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMCancelTaskController") as! TMCancelTaskController
                vc.syncWithWeb = AppState.globalSync
                vc.dataForCell = self.dataForCell
                accessNav?.presentThroughController(viewCon: vc)
                AppState.ongoingDetailShown = false
                reloadForDetail?.tableReference.reloadData()
                }else{
                    AppState.ongoingDetailShown = false
                    reloadForDetail?.changeTab(to: "OnGoing")
                    reloadForDetail?.reloadOngoing()
                }
            case false:
                AppState.ongoingDetailShown = true
                AppState.tempSelectedID = self.selectedTaskID
//                sender.borderColor = UIColor.red
//                sender.borderWidth = 1
//                sender.backgroundColor = UIColor.white
                
            //sender.setNeedsDisplay()
            //sender.titleLabel?.text = "Cancel Request"
                
                
                if let responders = dataForCell?.respondersList{
                    let responderData = responders
                    
                    var indexArray = [1]
                    for item in responderData{
                        indexArray.append(3)
                    }
//                    sender.setTitle("Cancel Request", for:.normal)
//                    sender.setTitleColor(UIColor.gray, for: [])
                    
                    reloadForDetail?.setChatDataGlobal(index: sender.tag)
                    reloadForDetail?.changeTab(to: "OnGoing")
                    reloadForDetail?.changeTitle(title: dataForCell!.subcat_name)
                    reloadForDetail?.showDetail(indexArray: indexArray, dataArray: responderData, firstCellData: dataForCell!)
                    reloadForDetail?.tableReference.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
                    
            }
            
            
           // reloadForDetail?.tableReference.reloadData()
            }
        }
    }
    
    func trimDate(date:String)->String{
        //"2018-03-28 11:11:41.000000"
        let dayDate = date.components(separatedBy: " ").first!
        var dateActual = dayDate.components(separatedBy: "-")
        var formattedDate = "\(dateActual[2])-\(dateActual[1])-\(dateActual[0])"
        let timeLong = date.components(separatedBy: " ")[1]
        let time = timeLong.components(separatedBy: ".").first!
        return formattedDate
    }
    
    func configureCellWith(data:APITaskList){
        dataForCell = data
        taskName.text = data.subcat_name
        
        selectedTaskID = data.taskid
        selectedCategory = data.subcat_id
        
        let blueColor = UIColor(red: 1/255, green: 139/255, blue: 255/255, alpha: 1)
        blueLineRef.backgroundColor = blueColor
        
        location.text = data.location
        let date = trimDate(date: data.taskdate.date)
        taskDate.text = "\(date)"
        taskMainLabel.text = data.mainTaskDescription
        taskSubLabel.text = data.subTaskDescription
        taskImage.kf.setImage(with: URL(string: "\(data.subcat_image)")!, placeholder: UIImage(named: "painting-paint-roller-"), options: [.requestModifier(AppState.modifier)])
       
        taskStatus.isHidden = shouldHideOnGoingLabel
       
        print("\(data.receivedResponses)/\(data.maxResponses) Responses")
        
//        if data.task_status == "Initiated" {
//            viewDetailsBTNRef.isHidden = true
//        }
        
        if AppState.ongoingDetailShown{
            viewDetailsBTNRef.setTitle("Full details", for: [])
            seeResponseBTN.borderColor = UIColor.red
            seeResponseBTN.borderWidth = 1
            seeResponseBTN.backgroundColor = UIColor.white
            seeResponseBTN.setTitle("Cancel Request", for:.normal)
            seeResponseBTN.setTitleColor(UIColor.gray, for: [])
            
            //Gray- R: G: B: 237 242 247
            //Blue- R: G: B: 1 139 255
            //GapGray- R: G: B: 201 201 201
            blueLineRef.backgroundColor = UIColor(red: 201/255, green: 201/255, blue: 201/255, alpha: 1)
            blueLineRef.frame = CGRect(x: 0, y: 223, width: 375, height: 1)
            greySpaceRef.backgroundColor = UIColor.white
            
        }else{
            viewDetailsBTNRef.setTitle("View details", for: [])
            seeResponseBTN.setTitle("\(data.receivedResponses)/\(data.maxResponses) Responses", for: [])
            seeResponseBTN.borderColor = UIColor.clear
            seeResponseBTN.borderWidth = 0
            seeResponseBTN.backgroundColor = UIColor(red: 1/255, green: 139/255, blue: 1, alpha: 1)
            seeResponseBTN.setTitleColor(UIColor.white, for: [])
            
            blueLineRef.backgroundColor = UIColor(red: 1/255, green: 139/255, blue: 255/255, alpha: 1)
            blueLineRef.frame = CGRect(x: 0, y: 208, width: 375, height: 2)
            greySpaceRef.backgroundColor = UIColor(red: 237/255, green: 242/255, blue: 247/255, alpha: 1)
        }
        
        if dataForCell?.task_status == "Initiated" {
            taskStatus.text = "Initiated"
        }else{
            taskStatus.text = "Ongoing"
        }
    }

}

protocol ReloadForDetail{
    func changeTitle(title:String)
    var indexForDetail:Int{get set}
    var currentCellArray:[Int]{get set}
    var tableReference:UITableView{get set}
    func showDetail(indexArray:[Int],dataArray:[Responders], firstCellData:APITaskList)
    func showDetailHired(data:APITaskList)
    func changeTab(to:String)
    func reloadOngoing()
    func dataForOngoingDetail(data:APITaskList)
    func setChatDataGlobal(index:Int)
}

//extension TMAllTasksResponsesCell: UICollectionViewDelegate, UICollectionViewDataSource{
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return 5
//    }
//
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TMQuestionsCell", for: indexPath) as! TMQuestionsCell
//        cell.questionNumber.text = "\(indexPath.row + 1)"
//        return cell
//    }
//}

