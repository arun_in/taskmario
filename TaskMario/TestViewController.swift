//
//  TestViewController.swift
//  TaskMario
//
//  Created by Appzoc on 13/02/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class TestViewController: UIViewController {
    
    @IBOutlet weak var viewOneHeight: NSLayoutConstraint!
    
    @IBOutlet weak var viewTwoHeight: NSLayoutConstraint!
    
    var number:Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    @IBAction func switchBTN(_ sender: UIButton) {
        if number{
            number = false
            viewOneHeight.constant = 400
            viewTwoHeight.constant = 100
        }else{
            number = true
            viewOneHeight.constant = 100
            viewTwoHeight.constant = 400
        }
    }
    

}
