//
//  TMSetLocationController.swift
//  TaskMario
//
//  Created by Appzoc on 09/02/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import GooglePlaces
import DropDown
import MapKit
import CoreLocation
import Alamofire

class TMSetLocationController: UIViewController, CLLocationManagerDelegate, UITextFieldDelegate, MKMapViewDelegate{
    
    var isFromQuestionairre:Bool = false
    let activityView = ActivityIndicatorAlter()
    
    var firstUpdate:Bool = true
    var dropDownList:[String] = []
    let dropDown = DropDown()
    var fetcher:GMSAutocompleteFetcher = GMSAutocompleteFetcher()
    
    var placeID:[String] = []
    var latitude:Double =  Double(AppState.latitude)!  //25.785774
    var longitude:Double = Double(AppState.longitude)! //55.989474
    var setPlace:String = AppState.selectedLocation
    
    let locationManager = CLLocationManager()
    
    @IBOutlet weak var mapRef: MKMapView!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var dropDownAnchor: UIView!
    @IBOutlet weak var locationField: UITextField!{
        didSet{
            print("Set")
        }
    }
    
    //TabBarCOntrols
    @IBAction func homeScreenBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadHomeTab(navigationController: self.navigationController!)
    }
    
    @IBAction func newTaskBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadNewTaskTab(navigationController: self.navigationController!)
    }
    
    @IBAction func tasksListBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadAllTasksTab(navigationController: self.navigationController!)
    }
    
    @IBAction func chatListBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadChatTab(navigationController: self.navigationController!)
    }
    
    @IBAction func profileBTN(_ sender: UIButton) {
        LoadTabBar.shared.loadProfileTab(navigationController: self.navigationController!)
    }
    
    @IBAction func editingChanged(_ sender: UITextField) {
        fetcher.sourceTextHasChanged(locationField.text!)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetcher.delegate = self
        let filter = GMSAutocompleteFilter()
        filter.country = "ae"
        fetcher.autocompleteFilter = filter
        setUpDropDown()
        //25.722845, 55.948305
       // latitude = 25.722845
        //longitude = 55.948305
        locationLabel.text = setPlace
        setUpMap()
        setUpLocationManager()
        mapRef.delegate = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        activityView.stop()
    }
    
    
    @IBAction func setLocationGlobalBTN(_ sender: UIButton) {
        
        locationLabel.text = setPlace
        AppState.selectedLocation = setPlace
        AppState.latitude = "\(self.latitude)"
        AppState.longitude = "\(self.longitude)"
        
        
        if isFromQuestionairre{
//            if AppState.selectedLocation == "Choose Location"{
//                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMSetLocationController") as! TMSetLocationController
//                vc.isFromQuestionairre = true
//                self.navigationController?.pushViewController(vc, animated: true)
//            }else if AppState.isLoggedIn == false{
//                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMLoginController") as! TMLoginController
//                vc.isFromQuestionairre = true
//                self.navigationController?.pushViewController(vc, animated: true)
//            }else{
                activityView.start()
                Alamofire.request("http://taskmario.dev.webcastle.in/api/submitTask", method: .post, parameters: ["userid":AppState.userData!.userid, "location":"\(AppState.selectedLocation)", "latitude":"\(AppState.latitude)", "longitude":"\(AppState.longitude)", "category_id":AppState.selectedSubCategory, "feedback": "\(AppState.feedback)"], encoding: URLEncoding.default, headers: nil).authenticate(user: "dev", password: "dev").responseJSON { (response) in
                    print(AppState.chosenAnswers)
                    print(response.result)
                    print(response.result.value)
                    
                   // let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMAllTasksController") as! TMAllTasksController
                    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMGoodJobController") as! TMGoodJobController
                    self.activityView.stop()
                    self.present(vc, animated: true, completion: nil)
                }
                
                
           // }
        }else{
            self.activityView.stop()
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    @IBAction func seCurrenttLocationGlobalBTN(_ sender: UIButton) {
        
        locationLabel.text = setPlace
        AppState.selectedLocation = setPlace
        AppState.latitude = "\(self.latitude)"
        AppState.longitude = "\(self.longitude)"
        
        
        if isFromQuestionairre{
            //            if AppState.selectedLocation == "Choose Location"{
            //                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMSetLocationController") as! TMSetLocationController
            //                vc.isFromQuestionairre = true
            //                self.navigationController?.pushViewController(vc, animated: true)
            //            }else if AppState.isLoggedIn == false{
            //                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMLoginController") as! TMLoginController
            //                vc.isFromQuestionairre = true
            //                self.navigationController?.pushViewController(vc, animated: true)
            //            }else{
            activityView.start()
            Alamofire.request("http://taskmario.dev.webcastle.in/api/submitTask", method: .post, parameters: ["userid":AppState.userData!.userid, "location":"\(AppState.selectedLocation)", "latitude":"\(AppState.latitude)", "longitude":"\(AppState.longitude)", "category_id":AppState.selectedSubCategory, "feedback": "\(AppState.feedback)"], encoding: URLEncoding.default, headers: nil).authenticate(user: "dev", password: "dev").responseJSON { (response) in
                print(AppState.chosenAnswers)
                print(response.result)
                print(response.result.value)
                
                // let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMAllTasksController") as! TMAllTasksController
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TMGoodJobController") as! TMGoodJobController
                self.activityView.stop()
                self.present(vc, animated: true, completion: nil)
            }
            
            
            // }
        }else{
            self.activityView.stop()
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    
    @IBAction func setCurrentLocation(_ sender: UIButton) {
        //setUpLocationManager()
        locationManager.requestLocation()
        let geocoder = CLGeocoder()
            if let currentLocation = locationManager.location{
                geocoder.reverseGeocodeLocation(currentLocation) { (placemark, err) in
                if let country = placemark?.first?.country{
                    if country == "United Arab Emirates"{
                        if let place = placemark?.first?.subLocality{
                            self.setPlace = place
                        }else if let place = placemark?.first?.locality{
                            self.setPlace = place
                        }else if let place = placemark?.first?.name{
                            self.setPlace = place
                        }
                        print("Set Place",self.setPlace)
                        self.locationLabel.text = self.setPlace
                        AppState.selectedLocation = self.setPlace
                        self.locationField.text = ""
                        //locationLabel.text = setPlace
                        //AppState.selectedLocation = setPlace
                    }else{
                        Banner.showBanner(message: "Service not available in this area")
                    }
                }
            }
             
        }else{
            Banner.showBannerOn(VC: self, message: "Unable to access User Location")
        }
        
     //print("Location",locationManager.location?.coordinate.latitude,locationManager.location?.coordinate.longitude)
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        if firstUpdate{
            firstUpdate = false
        }else{
            latitude = mapView.centerCoordinate.latitude
            longitude = mapView.centerCoordinate.longitude
            
            AppState.latitude = "\(latitude)"
            AppState.longitude = "\(longitude)"
            //25.722845, 55.948305
                print("Latitude :",latitude,"Longitude :",longitude)
                //setUpMap()
                let newLocation = CLLocation(latitude: latitude, longitude: longitude)
                let geocoder = CLGeocoder()
                geocoder.reverseGeocodeLocation(newLocation) { (placemark, err) in
                    if let place = placemark?.first?.subLocality{
                        self.setPlace = place
                    }else if let place = placemark?.first?.locality{
                        self.setPlace = place
                    }else if let place = placemark?.first?.name{
                        self.setPlace = place
                    }
                    
                    if let countryTemp = placemark?.first?.country{
                        print(countryTemp)
                    }
                    
                   // print("Set Place",self.setPlace)
                   // self.locationLabel.text = self.setPlace
                    //AppState.selectedLocation = self.setPlace
                    if self.longitude == 0 {
                        self.locationField.text = ""
                    }else{
                        self.locationField.text = self.setPlace
                    }
                }
        }
        
    }
    
    func setUpLocationManager(){
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        //locationManager.allowsBackgroundLocationUpdates = true
        if CLLocationManager.locationServicesEnabled() {
            print("Location Services Enabled")
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }

    }
    
    func setUpDropDown(){
        dropDown.anchorView = dropDownAnchor
        dropDown.dataSource = dropDownList
        dropDown.selectionAction = {
            index,item in
            self.locationField.text = item
            self.dropDown.hide()
            
            GMSPlacesClient.shared().lookUpPlaceID(self.placeID[index]){place, error in
                if let error = error {
                    print("lookup place id query error: \(error.localizedDescription)")
                    return
                }
                
                guard let place = place else {
                    print("No place details for \(self.placeID)")
                    return
                }
                
                self.latitude = place.coordinate.latitude
                self.longitude = place.coordinate.longitude
                self.setPlace = place.name
                self.setUpMap()
                //self.locationLabel.text = place.name
                print("Latitude ",self.latitude)
                print("Longitude ",self.longitude)
                
                print("Place name \(place.name)")
                print("Place address \(place.formattedAddress!)")
                
            }
            
            self.locationField.resignFirstResponder()
        }
    }
    
    func setUpMap(){
        let span = MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
        let center = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        let region = MKCoordinateRegion(center: center, span: span)
        mapRef.setRegion(region, animated: true)
        mapRef.setCenter(center, animated: true)
        //let annotation = MKPointAnnotation()
        //annotation.coordinate = center
       // mapRef.addAnnotation(annotation)
    }
    
//    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
//        print("locations = \(locValue.latitude) \(locValue.longitude)")
//    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("Location updated")
        if let location = locations.first{
            print("Location",location.coordinate.latitude,location.coordinate.longitude)
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Did fail with error")
        print(error)
    }
    
    

}

extension TMSetLocationController:GMSAutocompleteFetcherDelegate{
    func didAutocomplete(with predictions: [GMSAutocompletePrediction]) {
        dropDownList.removeAll()
        
        var tempPlaceID:[String] = []
        for prediction in predictions {
            dropDownList.append(prediction.attributedPrimaryText.string)
            
            tempPlaceID.append(prediction.placeID!)
            //print("Place ID:\(prediction.placeID)")
            //print("\n",prediction.attributedFullText.string)
            //print("\n",prediction.attributedPrimaryText.string)
            //print("\n********")
        }
        placeID = tempPlaceID
        
        dropDown.dataSource = dropDownList
        dropDown.show()
    }
    
    func didFailAutocompleteWithError(_ error: Error) {
        print(error)
    }
    
    
}
