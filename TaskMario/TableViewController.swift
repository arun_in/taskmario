//
//  TableViewController.swift
//  TaskMario
//
//  Created by Appzoc on 20/04/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit



class TableViewController: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    
    @IBOutlet weak var textField: UITextField!
    @IBOutlet var contentTv: UITableView!
    
    var sampleString = "khybgvbg"
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! MessageCellSender
        cell.message.text = self.sampleString
        return cell
    }
    
    @IBAction func BTNTapped(_ sender: UIButton)
    {
        sampleString = textField.text!
        contentTv.reloadData()
        self.view.setNeedsLayout()
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    

}
